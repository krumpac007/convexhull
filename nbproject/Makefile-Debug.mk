#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/GrahamScan.o \
	${OBJECTDIR}/HalfEdge.o \
	${OBJECTDIR}/SolverDivide.o \
	${OBJECTDIR}/SolverIncremental.o \
	${OBJECTDIR}/SolverIncrementalOptimized.o \
	${OBJECTDIR}/SolverJarvis.o \
	${OBJECTDIR}/SolverQuickHull.o \
	${OBJECTDIR}/Tester.o \
	${OBJECTDIR}/functions.o \
	${OBJECTDIR}/geometry.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/structures.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++11 -fopenmp -O3
CXXFLAGS=-std=c++11 -fopenmp -O3

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/convexhull

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/convexhull: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/convexhull ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/GrahamScan.o: GrahamScan.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GrahamScan.o GrahamScan.cpp

${OBJECTDIR}/HalfEdge.o: HalfEdge.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/HalfEdge.o HalfEdge.cpp

${OBJECTDIR}/SolverDivide.o: SolverDivide.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SolverDivide.o SolverDivide.cpp

${OBJECTDIR}/SolverIncremental.o: SolverIncremental.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SolverIncremental.o SolverIncremental.cpp

${OBJECTDIR}/SolverIncrementalOptimized.o: SolverIncrementalOptimized.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SolverIncrementalOptimized.o SolverIncrementalOptimized.cpp

${OBJECTDIR}/SolverJarvis.o: SolverJarvis.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SolverJarvis.o SolverJarvis.cpp

${OBJECTDIR}/SolverQuickHull.o: SolverQuickHull.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SolverQuickHull.o SolverQuickHull.cpp

${OBJECTDIR}/Tester.o: Tester.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Tester.o Tester.cpp

${OBJECTDIR}/functions.o: functions.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/functions.o functions.cpp

${OBJECTDIR}/geometry.o: geometry.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/geometry.o geometry.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/structures.o: structures.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -std=c++11 -fopenmp -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/structures.o structures.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/convexhull

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
