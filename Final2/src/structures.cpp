
#include <vector>
#include <iostream>

#include "geometry.h"
#include "structures.h"
#include "functions.h"


#include <math.h>
#include <algorithm>
#include <omp.h>
#include <set>

void Polyhedron::print() {
   std::cout << m_faces.size() << " faces" << std::endl;

   for (auto& i : m_faces) {
      std::cout << i.size() << " vertices @ " << &i << std::endl;
      //std::cout << omp_get_thread_num() << " - thread number" << std::endl;
      facet_t f = i;
      for (unsigned int j = 0; j < f.size(); j++) {
         std::cout << f[j][0] << " " << f[j][1] << " " << f[j][2] << std::endl;
      }
   }
}

void Polyhedron::printIdx() {
   std::cout << m_faces_idx.size() << " faces" << std::endl;

   for (auto& i : m_faces_idx) {
      std::cout << i.size() << " vertices" << std::endl;
      facet_idx_t f = i;
      for (unsigned int j = 0; j < f.size(); j++) {
         std::cout << f[j] << " ";
      }
      std::cout << std::endl;
   }
}

void Polyhedron::addFacet(facet_t& f) {
   m_faces.push_back(f);
   if (m_vertices.size() != 0)
      for (point_t p : f)
         m_vertices.insert(p);

   if (m_neighbours.size() != 0) {
      std::set<point_t> emptySet;
      for (point_t p : f) {
         m_neighbours.insert({p, emptySet});
      }

      for (unsigned int j = 0; j < f.size(); j++) {
         //insert one before this one and one after
         point_t thisPoint = f[j];
         m_neighbours[thisPoint].insert(f[((j - 1)+f.size()) % f.size()]);
         m_neighbours[thisPoint].insert(f[(j + 1) % f.size()]);
      }
   }
}

std::set<point_t> Polyhedron::getVertices() {
   if (m_vertices.size() == 0) {
      std::set<point_t> vertices;
      for (unsigned int i = 0; i < m_faces.size(); i++) {
         for (unsigned int j = 0; j < m_faces[i].size(); j++) {
            vertices.insert(m_faces[i][j]);
         }
      }
      m_vertices = vertices;
   }
   return m_vertices;
}

void Polyhedron::checkDuplicateFaces() {
   std::vector<std::pair<int, int> > duplicates;
   int numDuplicates = 0;
   for (unsigned int i = 0; i < m_faces.size(); i++) {
      facet_t f1 = m_faces[i];
      for (unsigned int j = i + 1; j < m_faces.size(); j++) {
         facet_t f2 = m_faces[j];
         if (checkFacetsAreTheSame(f1, f2) == 1) {
            numDuplicates++;
            duplicates.push_back({i, j});
         }
      }
   }
   std::cout << "Polyhedron has " << numDuplicates << " duplicates" << std::endl;
   for (auto it : duplicates) {
      std::cout << it.first << "-" << it.second << "; ";
   }
}

void Polyhedron::removeDuplicateFaces() {
   //int numDuplicates = 0;
   std::set<int> duplicateIdxs; //set of duplicate indexes ordered from lowest, every element only once, solves problem when we have
   //3 or more same faces eg. 6-7, 6-8, 7-8
#pragma omp parallel for schedule(dynamic)
   for (unsigned int i = 0; i < m_faces.size(); i++) {
      facet_t f1 = m_faces[i];
      
      for (unsigned int j = i + 1; j < m_faces.size(); j++) {
         facet_t f2 = m_faces[j];
         if (checkFacetsAreTheSame(f1, f2) == 1)
#pragma omp critical
         {
            duplicateIdxs.insert(j);
         }
      }
   }
   //iterate from highest index and delete this element, from highest because if we go from lowest, the set would vector of faces would shrink and I would 
   //delete valid faces
   for (auto it = duplicateIdxs.rbegin(); it != duplicateIdxs.rend(); it++) {
      int idx = *it;
      m_faces.erase(m_faces.begin() + idx);
   }
}

void Polyhedron::removeFacetByPointer(facet_t* f) {
   //int idx;
   m_vertices.clear();
   m_neighbours.clear();
   for (unsigned int i = 0; i < m_faces.size(); i++) {
      facet_t* address = &(m_faces[i]);
      if (address == f) {
         m_faces.erase(m_faces.begin() + i);
         return;
      }
   }
   std::cerr << "Couldnt remove facet from output - wrong adress" << std::endl;

}

void Polyhedron::removeFacetsByPointers(std::vector<facet_t*> facets) {
   std::set<int> toRemoveIdxs;
   m_vertices.clear();
   m_neighbours.clear();
   for (unsigned int i = 0; i < facets.size(); i++) {
      for (unsigned int k = 0; k < m_faces.size(); k++) {
         facet_t* address = &(m_faces[k]);
         if (facets[i] == address) {
            toRemoveIdxs.insert(k);
            break;
         }
      }
   }
   for (auto it = toRemoveIdxs.rbegin(); it != toRemoveIdxs.rend(); it++) {
      m_faces.erase(m_faces.begin()+(*it));
   }
}

std::map<point_t, std::set<point_t> > Polyhedron::getNeighboursMap() { 
   //fill
   std::set<point_t> emptySet;
   std::set<point_t> verts = getVertices();
   for (point_t p : verts) {
      m_neighbours.insert({p, emptySet});
   }

   for (unsigned int i = 0; i < m_faces.size(); i++) {
      facet_t & f = m_faces[i];

      for (unsigned int j = 0; j < f.size(); j++) {
         //insert one before this one and one after
         point_t thisPoint = f[j];
         m_neighbours[thisPoint].insert(f[((j - 1)+f.size()) % f.size()]);
         m_neighbours[thisPoint].insert(f[(j + 1) % f.size()]);
      }
   }
   return m_neighbours;
}

std::map<point_t, std::set<unsigned> > Polyhedron::getFacesMap(){
   std::map<point_t, std::set<unsigned> > facesMap;
   std::set<point_t> verts = getVertices();
   std::set<unsigned> emptySet;
   
   for (point_t p : verts) {
      facesMap.insert({p, emptySet});
   }
   
   for (unsigned int i = 0; i < m_faces.size(); i++) {
      facet_t & f = m_faces[i];
         for (unsigned int j = 0; j < f.size(); j++) {
         //insert one before this one and one after
         point_t thisPoint = f[j];         
         facesMap[thisPoint].insert(i);
      }
   }
   return facesMap;
}


