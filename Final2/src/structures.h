/* 
 * File:   structures.h
 * Author: motykvac
 *
 * Created on February 23, 2017, 5:42 PM
 */

#ifndef STRUCTURES_H
#define	STRUCTURES_H
#pragma once

#include "geometry.h"
#include <vector>
#include <deque>
#include <set>
#include <map>
#include <string>
#include <iostream>


typedef std::vector<double> point_t;
typedef std::vector<point_t> facet_t;
typedef std::vector<int> facet_idx_t;
typedef std::vector<point_t> data_t;
typedef std::pair<unsigned, unsigned> edge_t;
typedef std::pair<point_t, point_t> edgePoints_t;

typedef std::pair<double, double> point2d_t;

//#define D(X) std::cout<<"  "<<#X": "<<X<<std::endl;
//#define R(X) //std::cout<<"  "<<X<<std::endl;


class Polyhedron {
public:
   friend class SolverIncremental;
   friend class SolverIncrementalOptimized;
   friend bool comparePolyhedrons(Polyhedron& p1, Polyhedron& p2, std::string type);
   friend void printOpenScad(Polyhedron& p);
   friend void printOpenScadEachFacet(Polyhedron& p);
   friend edgePoints_t findLowerTangent(Polyhedron&, Polyhedron&);
   friend void discardHiddenFaces(Polyhedron&, std::set<std::vector<double> >&);
   friend void discardHiddenFaces(Polyhedron& polyhedron, std::set<point_t>& horizon, std::set<point_t>& myHorizon, bool left);
   friend void removeCoplanarFaces(Polyhedron&, std::vector<std::vector<std::vector<double> > >&);
   friend void mergeTwoHulls(Polyhedron&, Polyhedron&, edgePoints_t&);
   friend void mergeTwoHullsOptimized(Polyhedron&, Polyhedron&, edgePoints_t&);
   
   void addFacet(facet_t& f);
   void addFacetIdx(facet_idx_t& f) {
      m_faces_idx.push_back(f);
   };

   void popFacet() {
      m_faces.pop_back();
   };

   int getSize() {
      return m_faces.size();
   };
   
   void print();
   void printIdx();
   std::set<point_t> getVertices();
   int getNumFaces() {return m_faces.size();};
   void checkDuplicateFaces();
   void removeDuplicateFaces();
   std::deque<facet_t> getFaces(){
      return m_faces;
   };
   
   facet_t getFace(int idx){
      return m_faces[idx];
   };
   
   facet_t* getFacePointer(int idx){
      return &(m_faces[idx]);
   };
   
   
   void removeFacet(int idx){      
      if(m_faces.erase(m_faces.begin() + idx) == m_faces.end()) 
         std::cout << "Couldnt find face to be erased" << std::endl;
      m_vertices.clear();
      m_neighbours.clear();
   };
   void removeFacetByPointer(facet_t* f);
   void removeFacetsByPointers(std::vector<facet_t*> facets);
   void printFacesAdresses(){
      for(unsigned int i = 0; i < m_faces.size(); i++){
         std::cout<<&(m_faces[i]) << " ";
      }
   }
   std::map<point_t, std::set<point_t> > getNeighboursMap();
   std::map<point_t, std::set<unsigned> > getFacesMap();
private:
   std::deque<facet_t> m_faces;
   std::vector<facet_idx_t> m_faces_idx;
   std::set<point_t> m_vertices;
   std::map<point_t, std::set<point_t> > m_neighbours;
};

class Solver2D {
public:
   facet_t& Solve(data_t& input, facet_t& output);
};


class Solver3D {
public:
   virtual Polyhedron& Solve(data_t& input, Polyhedron& output) = 0;

   std::string getName() {
      return m_name;
   };
protected:
   std::string m_name;
};


#endif	/* STRUCTURES_H */

