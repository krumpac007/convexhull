/* 
 * File:   HalfEdge.h
 * Author: motykvac
 *
 * Created on March 21, 2017, 5:35 PM
 */


#ifndef HALFEDGE_H
#define	HALFEDGE_H


#include "geometry.h"
#include "structures.h"

//#include <string>


class QVertex;
class QFace;
class QHalfEdge;

/**
 QFace is a triangular face, containing a normal vector of the plane and offset to form eq. ax+by+cz+offset=0
 */
class QFace {
public:

   QFace() : he0(NULL), area(0), planeOffset(0), index(0), numVerts(0), next(NULL), mark(VISIBLE), outside(NULL) {
   };
   QHalfEdge* he0; //first halfedge
   double area;
   double planeOffset;
   int index;
   int numVerts;
   QFace* next;


   //enum state
   int mark;
   //outside is the first vertex in the vertexlist that belong to this face... we can itereate through next while ->face is the same for the vertexes
   QVertex* outside;
   void computeCentroid();
   void computeNormal();
   void computeNormalAndCentroid();
   static QFace* createTriangle(QVertex* v0, QVertex* v1, QVertex* v2);
   QHalfEdge* getEdge(int i);

   QHalfEdge* getFirstEdge() {
      return he0;
   };
   //finds edge with this tail and head
   QHalfEdge* findEdge(QVertex* vtail, QVertex* vhead);
   //diff
   double distanceToPlane(QVertex* v);
   double distanceToPlane(point_t& v);
   void getVertexIndexes(int idxs[]);
   int mergeAdjacentFace(QHalfEdge* hedgeAdj, std::vector<QFace*>& discarded);
   //returns discarded face
   QFace* connectHalfEdges(QHalfEdge* hedgePrev, QHalfEdge* hedge);
   enum State {
      VISIBLE = 1, NON_CONVEX = 2, DELETED = 3
   };
   
   std::string getVertexString();

   point_t normal, centroid;


public:
   State state;
};

class QVertex {
public:

   QVertex(point_t p) : pnt(p), index(-1), prev(NULL), next(NULL), face(NULL) {
   };

   QVertex() : QVertex({0, 0, 0}) {
   }

   //point associated with this vtx   
   point_t pnt;
   int index; //point index
   QVertex* prev, *next; //next vertices in the list
   QFace * face; //assigned face
   double operator[](unsigned x) {
      return pnt[x];
   }
};

class QHalfEdge {
public:

   QHalfEdge() : vertex(NULL), face(NULL), next(NULL), prev(NULL), opposite(NULL) {
      ;
   };

   QHalfEdge(QVertex* v, QFace* f) : vertex(v), face(f), next(NULL), prev(NULL), opposite(NULL) {
      ;
   };
   //void SetNext(QHalfEdge * he){ next = he;}; //set next ccw he
   //QHalfEdge* getNext() {return next;};

   //get tail of this he

   QVertex* tail() {
      return prev != NULL ? prev->vertex : NULL;
   };

   QVertex* head() {
      return vertex;
   };

   QFace* oppositeFace() {
      return opposite != NULL ? opposite->face : NULL;
   };
   std::string getVertexString();
   double length();
   double lengthSquared();
   void setOpposite(QHalfEdge* edge){
      opposite = edge;
      edge->opposite = this;
   }


   QVertex * vertex; //vertex associated with this he
   QFace * face; //face ass w this he
   //next is ccw order
   QHalfEdge * next, *prev, *opposite;

};

class QFaceList {
public:
   QFaceList():head(NULL), tail(NULL){;};
   QFace* head;
   QFace* tail;

   void clear() {
      //TODO clear memory
      head = tail = NULL;
   }

   void add(QFace* f) {
      if (head == NULL)
         head = f;
      else
         tail->next = f;

      f->next = NULL;
      tail = f;
   }
   
   QFace* first(){
      return head;
   }
   
   bool isEmpty() {
      return head == NULL;
   }
};

class QVertexList {
public:
   QVertexList(): head(NULL), tail(NULL){;};
   QVertex* head;
   QVertex* tail;

   void clear() {
      //TODO clear memory
      head = tail = NULL;
   }

   void add(QVertex* f) {
      if (head == NULL)
         head = f;
      else
         tail->next = f;

      f->prev = tail;
      f->next = NULL;
      tail = f;
   }
   
   void addAll(QVertex* vtx){
      if(head == NULL){
         head = vtx;
      }
      else{
         tail->next = vtx;
      }
      vtx->prev = tail;
      while(vtx->next != NULL){
         vtx = vtx->next;
      }
      tail=vtx;
   }
   
   void deleteVtx(QVertex* vtx){
      if(vtx->prev == NULL)
         head = vtx->next;
      else
         vtx->prev->next = vtx->next;
      if(vtx->next == NULL)
         tail=vtx->prev;
      else
         vtx->next->prev=vtx->prev;
   }

   //deletes chain of vertices   
   void deleteVtx(QVertex* vtx1, QVertex* vtx2){
      if(vtx1->prev == NULL)
         head=vtx2->next;
      else
         vtx1->prev->next = vtx2->next;
      if(vtx2->next == NULL)
         tail=vtx1->prev;
      else
         vtx2->next->prev = vtx1->prev;         
   }
   
   void insertBefore(QVertex* vtx, QVertex* next){
      vtx->prev = next->prev;
      if(next->prev == NULL)
         head = vtx;
      else
         next->prev->next = vtx;
      vtx->next = next; 
      next->prev = vtx;
   }

   bool isEmpty() {
      return head == NULL;
   }
   
   QVertex* first(){
      return head;
   }
};


#endif	/* HALFEDGE_H */

