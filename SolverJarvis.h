/* 
 * File:   SolverJarvis.h
 * Author: motykvac
 *
 * Created on February 23, 2017, 6:10 PM
 */

#ifndef SOLVERJARVIS_H
#define	SOLVERJARVIS_H

#include "geometry.h"
#include "structures.h"
#include <set>
#include <string>
class SolverJarvis : Solver3D {
public:

   SolverJarvis(int thr = 1) {
      numThreads = thr;
      m_name = "Jarvis";
      //EPS_LOC = 1e-6;
   };
   Polyhedron& Solve(data_t& input, Polyhedron& output);
   Polyhedron& SolveParallel(data_t& input, Polyhedron& output, int numThr);
   

private:
   int numThreads;
   Polyhedron& SolveNaive(data_t& input, Polyhedron& output);
   void WorkParallel(data_t& input, Polyhedron& output, std::pair<unsigned, unsigned> init, std::set<std::pair<unsigned, unsigned> >& opened,
                                                                                                   std::set<std::pair<unsigned, unsigned> >& closed );
   enum Direction {
      DOWN, TOP, LEFT, RIGHT, FRONT, BACK
   };
   std::pair<unsigned, unsigned> findInitial(const data_t& input, const Direction direction = DOWN);
   //double EPS_LOC;

   void printDir(const Direction& dir);
   void printSet(std::set<std::pair<unsigned, unsigned> > & s);
};

#endif	/* SOLVERJARVIS_H */

