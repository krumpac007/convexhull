\select@language {czech}
\select@language {czech}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{viii}{section*.1}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}
\contentsline {section}{C\IeC {\'\i }l pr\IeC {\'a}ce}{1}{section*.6}
\contentsline {section}{Struktura pr\IeC {\'a}ce}{2}{section*.7}
\contentsline {chapter}{\chapternumberline {1}Teorie}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Konvexn\IeC {\'\i } ob\IeC {\'a}lka}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Doln\IeC {\'\i } hranice slo\IeC {\v z}itosti}{8}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Softwarov\IeC {\'e} a hardwarov\IeC {\'e} aspekty}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Pou\IeC {\v z}it\IeC {\'e} technologie}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Programovac\IeC {\'\i } jazyk}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Pou\IeC {\v z}it\IeC {\'e} knihovny}{9}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}OpenMP}{10}{subsection.2.1.3}
\contentsline {paragraph}{Mo\IeC {\v z}nosti OpenMP}{10}{section*.8}
\contentsline {section}{\numberline {2.2}Existuj\IeC {\'\i }c\IeC {\'\i } \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Datov\IeC {\'e} struktury}{11}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Reprezentace mnohost\IeC {\v e}nu}{11}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Half-edge struktura}{12}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}Dal\IeC {\v s}\IeC {\'\i } mo\IeC {\v z}n\IeC {\'e} struktury}{13}{subsubsection.2.3.1.2}
\contentsline {chapter}{\chapternumberline {3}Algoritmy pro nalezen\IeC {\'\i } konvexn\IeC {\'\i } ob\IeC {\'a}lky ve 3D}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Jarvis March}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Popis algoritmu}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Slo\IeC {\v z}itost algoritmu}{17}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Algoritmus Divide and Conquer}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Popis algoritmu}{18}{subsection.3.2.1}
\contentsline {section}{\numberline {3.3}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Popis algoritmu}{20}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Anal\IeC {\'y}za slo\IeC {\v z}itosti}{24}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}QuickHull}{24}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Popis algoritmu}{24}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Anal\IeC {\'y}za slo\IeC {\v z}itosti}{25}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Dal\IeC {\v s}\IeC {\'\i } algoritmy}{26}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Optimalizace}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Obecn\IeC {\'e} optimalizace}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Jarvis March}{28}{section.4.2}
\contentsline {section}{\numberline {4.3}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{29}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Slo\IeC {\v z}itost algoritmu}{31}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}QuickHull}{31}{section.4.4}
\contentsline {section}{\numberline {4.5}Fat Planes}{32}{section.4.5}
\contentsline {section}{\numberline {4.6}Mo\IeC {\v z}nosti paralelizace}{34}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Jarvis march}{34}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{34}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}QuickHull}{35}{subsection.4.6.3}
\contentsline {chapter}{\chapternumberline {5}V\IeC {\'y}sledky}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Testovac\IeC {\'\i } data}{38}{section.5.1}
\contentsline {section}{\numberline {5.2}Jednotliv\IeC {\'e} algoritmy}{38}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Jarvis March}{39}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{39}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}QuichHull}{39}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Celkov\IeC {\'e} porovn\IeC {\'a}n\IeC {\'\i }}{40}{section.5.3}
\contentsline {chapter}{Z{\' a}v{\v e}r}{43}{chapter*.9}
\contentsline {chapter}{Literatura}{45}{section*.11}
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{49}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Instala\IeC {\v c}n\IeC {\'\i } p\IeC {\v r}\IeC {\'\i }ru\IeC {\v c}ka}{51}{appendix.B}
\contentsline {section}{\numberline {B.1}Po\IeC {\v z}adavky}{51}{section.B.1}
\contentsline {section}{\numberline {B.2}Instalace a pou\IeC {\v z}it\IeC {\'\i }}{51}{section.B.2}
\contentsline {section}{\numberline {B.3}Form\IeC {\'a}t vstupu a v\IeC {\'y}stupu}{51}{section.B.3}
\contentsline {appendix}{\chapternumberline {C}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{53}{appendix.C}
