/* 
 * File:   SolverQuickHull.h
 * Author: motykvac
 *
 * Created on March 22, 2017, 6:07 PM
 */

#ifndef SOLVERQUICKHULL_H
#define	SOLVERQUICKHULL_H
#include "structures.h"
#include <vector>
#include "HalfEdge.h"
#include <iostream>
#include "geometry.h"

class SolverQuickHull : Solver3D {
public:

   SolverQuickHull(int numThr = 1) : numThreads(numThr), maxVtxs(3, 0), discardedFaces(3, 0), minVtxs(3, 0) {
      newFaces = new QFaceList();
      unclaimed = new QVertexList();
      claimed = new QVertexList();
      debug = 0;
   };

   Polyhedron& Solve(data_t& data, Polyhedron& p) {
      ;
   }; //quickhull will not store to polyhedron yet
   //for now only void, result will be stored in this object 
   void Solve(data_t& input);
   void Print();
   void PrintVertices();
   bool debug;
   int** getFaces();
   void getFaceIndices(int[], QFace*);
   int getNumFaces();
private:
   int numThreads;
   std::vector<QVertex*> pointBuffer;
   std::vector<int> vertexPointIndices;
   std::vector<QFace*> discardedFaces;

   //vtxs with maximum coordinate, x, y and z
   std::vector<QVertex*> maxVtxs;
   std::vector<QVertex*> minVtxs;

   
   //this will contain ALL the faces created on the way, not only the visible ones, but also deleted etc;
   std::vector<QFace*> faces;
   std::vector<QHalfEdge*> horizon;

   QFaceList* newFaces;
   QVertexList* unclaimed;
   QVertexList* claimed;

   int numVertices;
   int numFaces;
   int numPoints;

   double tolerance;

   double DOUBLE_PREC = 2.2204460492503131e-16;

   static const int NONCONVEX_WRT_LARGER_FACE = 1;
   static const int NONCONVEX = 2;


   void initBuffers(int nump);
   void setPoints(data_t& points, int nump);
   void buildHull();
   void computeMaxAndMin();
   void createInitialSimplex();

   double findMax(double a, double b) {
      return a > b ? a : b;
   };
   void addPointToFace(QVertex* vtx, QFace* face);
   void removePointFromFace(QVertex* vtx, QFace* face);
   void addPointToHull(QVertex* eyeVtx);
   QVertex* nextPointToAdd();
   void reindexFacesAndVertices();
   void calculateHorizon(point_t eyePnt, QHalfEdge* he0, QFace* face, std::vector<QHalfEdge*>& horizon);
   void deleteFacePoints(QFace* face, QFace* absorbingFace);
   QVertex* removeAllPointsFromFace(QFace* face);
   void addNewFaces(QFaceList* newFaces, QVertex* eyeVtx, std::vector<QHalfEdge*>& horizon);
   //returns halfedge of the face we are adding, creates face from point and he
   QHalfEdge* addAdjoiningFace(QVertex* eyeVtx, QHalfEdge* he);
   bool doAdjacentMerge(QFace* face, int mergeType);

   double oppFaceDistance(QHalfEdge* he) {
      return he->face->distanceToPlane(he->opposite->face->centroid);
   }
   void resolveUnclaimedPoints(QFaceList* newFaces);
   void markFaceVertices(QFace* face, int mark);

   void printVector(point_t& p) {
      for (int i = 0; i < p.size(); i++)
         std::cout << p[i] << " ";
      std::cout << std::endl;
   }


};


#endif	/* SOLVERQUICKHULL_H */

