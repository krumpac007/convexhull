NAME = convex

FLAGS = -fopenmp -std=c++11 -O3
CC = g++
SRCDIR = .
BUILDDIR = build
BINDIR = bin

all:$(BINDIR)/$(NAME)
clean:
	rm -f build/*.o
	rm -f bin/*
	rmdir bin
	rmdir build
$(BUILDDIR)/geometry.o: geometry.cpp geometry.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/geometry.o geometry.cpp 

$(BUILDDIR)/HalfEdge.o: HalfEdge.cpp HalfEdge.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/HalfEdge.o HalfEdge.cpp

$(BUILDDIR)/main.o: main.cpp
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/main.o main.cpp

$(BUILDDIR)/SolverDivide.o: SolverDivide.cpp SolverDivide.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/SolverDivide.o SolverDivide.cpp 

$(BUILDDIR)/SolverIncremental.o: SolverIncremental.cpp SolverIncremental.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/SolverIncremental.o SolverIncremental.cpp

$(BUILDDIR)/SolverIncrementalOptimized.o: SolverIncrementalOptimized.cpp SolverIncrementalOptimized.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/SolverIncrementalOptimized.o SolverIncrementalOptimized.cpp

$(BUILDDIR)/SolverJarvis.o: SolverJarvis.cpp SolverJarvis.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/SolverJarvis.o SolverJarvis.cpp

$(BUILDDIR)/SolverQuickHull.o: SolverQuickHull.cpp SolverQuickHull.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/SolverQuickHull.o SolverQuickHull.cpp

$(BUILDDIR)/structures.o: structures.cpp structures.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/structures.o structures.cpp

$(BUILDDIR)/Tester.o: Tester.cpp Tester.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/Tester.o Tester.cpp

$(BUILDDIR)/GrahamScan.o: GrahamScan.cpp GrahamScan.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/GrahamScan.o GrahamScan.cpp

$(BUILDDIR)/functions.o: functions.cpp functions.h
	$(CC) $(FLAGS) -c -o $(BUILDDIR)/functions.o functions.cpp

$(BINDIR)/$(NAME): $(BUILDDIR)/main.o $(BUILDDIR)/geometry.o $(BUILDDIR)/HalfEdge.o $(BUILDDIR)/SolverDivide.o $(BUILDDIR)/SolverIncremental.o $(BUILDDIR)/SolverIncrementalOptimized.o $(BUILDDIR)/SolverJarvis.o $(BUILDDIR)/SolverQuickHull.o $(BUILDDIR)/structures.o $(BUILDDIR)/Tester.o $(BUILDDIR)/functions.o $(BUILDDIR)/GrahamScan.o
	mkdir -p bin
	mkdir -p build
	$(CC) $(FLAGS) -o $(BINDIR)/$(NAME) $(BUILDDIR)/main.o $(BUILDDIR)/geometry.o $(BUILDDIR)/HalfEdge.o $(BUILDDIR)/SolverDivide.o $(BUILDDIR)/SolverIncremental.o $(BUILDDIR)/SolverIncrementalOptimized.o $(BUILDDIR)/SolverJarvis.o $(BUILDDIR)/SolverQuickHull.o $(BUILDDIR)/structures.o $(BUILDDIR)/Tester.o $(BUILDDIR)/functions.o $(BUILDDIR)/GrahamScan.o