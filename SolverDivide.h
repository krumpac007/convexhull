/* 
 * File:   SolverDivide.h
 * Author: motykvac
 *
 * Created on March 8, 2017, 5:00 PM
 */

#ifndef SOLVERDIVIDE_H
#define	SOLVERDIVIDE_H

#include "geometry.h"
#include "structures.h"


class SolverDivide : Solver3D{
   public:
   Polyhedron& Solve(data_t& input, Polyhedron& output);
   Polyhedron& Solve(data_t& input, int begin, int end, Polyhedron& output);
   Polyhedron& SolveParallel(data_t& input, Polyhedron& output, int numThreads);
   Polyhedron& SolveRec(data_t& input, int begin, int end, Polyhedron& output);

};


#endif	/* SOLVERDIVIDE_H */

