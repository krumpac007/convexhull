/* 
 * File:   main.cpp
 * Author: motykvac
 *
 * Created on February 23, 2017, 5:34 PM
 */

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <string>
#include "SolverJarvis.h"
#include "structures.h"
#include "functions.h"
#include "HalfEdge.h"
#include "SolverQuickHull.h"
#include <omp.h>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   int numPoints, dummy;
   enum Type{QUICKHULL, JARVISMARCH, BOTH};
   Type type = BOTH;
   if(argc>1){
      if(*argv[1] == 'j'){
         cout << "Using Jarvis march"<<endl;
         type = JARVISMARCH;
      }
      else if(*argv[1] == 'q'){
         cout << "Using QuickHull"<<endl;
         type=QUICKHULL;
      }
   }   
   cout << "Enter dimension (works only with 3) number of points and points coordinates:" << endl;
   cin>>dummy;
   cin >> numPoints;
   string line;
   double xcoord=0, ycoord=0, zcoord=0;
   //must be here to ger rid of first line
   getline(cin, line);
   data_t data;
   
   double inTimeA = omp_get_wtime();

   /*Data input*/
   for(int i =0; i < numPoints; i++){
      getline(cin, line);      
      istringstream iss(line);
      iss >> xcoord >> ycoord >> zcoord;
      //cout << xcoord << " " << ycoord << " " << zcoord << endl;
      point_t point;
      point.push_back(xcoord);
      point.push_back(ycoord);
      point.push_back(zcoord);
      data.push_back(point);
   }
   double inTimeB = omp_get_wtime();
   //cout<<"intime: " << inTimeB-inTimeA<<endl    ;
   
   //printGeomView(data);
   //printJava(data);
   
   //printAllPoints(data);
   
   if(type==JARVISMARCH || type == BOTH){
   SolverJarvis solverJarvis;
   Polyhedron output;
   double jarvisTimeA = omp_get_wtime();
   solverJarvis.Solve(data, output);
   double jarvisTimeB = omp_get_wtime();
   double jarvisTime = jarvisTimeB-jarvisTimeA;
   
   
   Solver2D solver2d;
   facet_t o;
   std::cout << "JARVIS MARCH:" << std::endl;
   output.print();
   //output.printIdx();
   }
   if(type==QUICKHULL || type == BOTH){
   std::cout << std::endl << "QUICKHULL:" << std::endl;
   SolverQuickHull solverQH;
   //solverQH.debug = 1;
   double qhullTimeA = omp_get_wtime();
   solverQH.Solve(data);
   double qhullTimeB = omp_get_wtime();
   double qhullTime = qhullTimeB-qhullTimeA;
   solverQH.Print(); cout << std::endl;
   //solverQH.PrintVertices();
   }
   
   /*Time tests*/
   /*if(output.getNumFaces() != solverQH.getNumFaces()){
      cout << "Jarvis and quickhull has diff results, jarvis has " << output.getNumFaces() << " faces, quickhull has " << solverQH.getNumFaces() << endl;
   }else
      cout << "Results ok, "<< solverQH.getNumFaces() << " faces" << endl;
   cout<< numPoints <<" points, Jarvis: "<< jarvisTime << ", QuickHull: " << qhullTime << endl;
   
   std::ofstream results;
   results.open("./results", std::fstream::app);
   
   results << numPoints << " " << jarvisTime << " " << qhullTime << endl;
   results.close();
   */
   return 0;
}

