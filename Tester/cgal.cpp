/**THIS FILE WAS CREATED FOR TESTING CGAL SPEED**/


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
//#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/convex_hull_incremental_3.h>
#include <vector>
#include <omp.h>
#include <fstream>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron_3;
typedef K::Point_3 Point_3;
//typedef CGAL::Surface_mesh<Point_3>               Surface_mesh;

void Test(long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests, char type,
        double distribution, unsigned size) {


   int ratio = (maxNumberOfPoints - minNumberOfPoints) / numberOfTests;
   for (int i = 0; i < numberOfTests; i++) {
      int numberOfPoints = minNumberOfPoints + i*ratio;
      if (type == 'c')
         type = ' ';
      std::string cmd = "rbox " + std::to_string(numberOfPoints) + " B" + std::to_string(size) + " " + type + " n W" + std::to_string(distribution)
              + " D3 > testInput";
      const char * input = cmd.c_str();

      system(input);


      //TEST
      std::ifstream in("testInput");
      std::vector<Point_3> points;
      Point_3 p;     
      int dummy;
      in >> dummy;
      in >> dummy;      
      while (in >> p) {
         points.push_back(p);
      }
      Polyhedron_3 poly;
      Polyhedron_3 poly2;

   // compute convex hull of non-collinear points
      double qhTimeA = omp_get_wtime();
   CGAL::convex_hull_3(points.begin(), points.end(), poly);
   double qhTimeB = omp_get_wtime();
   std::cout << "CGAL   " << numberOfPoints << "    "<< poly.size_of_facets()<< "  " << qhTimeB-qhTimeA << "   qh" << std::endl;
   double incTimeA = omp_get_wtime();
   CGAL::convex_hull_incremental_3(points.begin(), points.end(), poly2); 
   double incTimeB = omp_get_wtime();
   //std::cout << "The convex hull contains " << poly.size_of_facets()<< " vertices" << std::endl;
   
   std::cout << "CGAL   " << numberOfPoints << "    "<< poly2.size_of_facets()<< "  " << incTimeB-incTimeA << "   inc" << std::endl;

   }
}

int main(int argc, char* argv[]) {
    std::cout << "My CGAL library is " << CGAL_VERSION_NR << " (1MMmmb1000)" << std::endl; 
   Test(100000, 1100000, 10, 's', 1, 100);
   return 0;
}
