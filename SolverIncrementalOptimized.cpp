#include "SolverIncrementalOptimized.h"
#include "SolverIncremental.h"

#include <iostream>
#include <algorithm>
#include <omp.h>


Polyhedron& SolverIncrementalOptimized::Solve(data_t& input, Polyhedron& output) {
   //find initial tetrahedron
   std::set<unsigned> dummy;
   std::random_shuffle(input.begin(), input.end());
   findInitialTetrahedron(input, output, dummy);
   initConflicts(input, output);

   if (debug) {
      std::cout << "Found initial tetrahedron with vertices: " << std::endl;
      for (auto it = initialTetrahedronIdxs.begin(); it != initialTetrahedronIdxs.end(); it++) {
         printPoint(input[*it]);
      }
   }
   //for each point compute if it lies inside or not
   for (unsigned int i = 0; i < input.size(); i++) {
      //initial tetrahedron point
      if (initialTetrahedronIdxs.find(i) != initialTetrahedronIdxs.end())
         continue;

      point_t p = input[i];

      if (debug) {
         std::cout << "Adding point ";
         printPoint(p);
      }
      //point is inside, can discard it
      if (pointIsInsideHull(i, output)) {
         if (debug)
            std::cout << "Point is inside." << std::endl;
         continue;
      } else {
         //find horizon - horizon edge will be in visible faces only once
         std::vector<edgePoints_t> horizon;

         findHorizon(i, output, horizon);

         if (debug) {
            std::cout << "Point ";
            printPoint(p);
            std::cout << " has horizon: " << std::endl;
            printHorizon(horizon);
         }        

         //for each horizon edge make a new facet
         //only for edges not on coplanar face
         for (unsigned int j = 0; j < horizon.size(); j++) {
            point_t p1 = horizon[j].first;
            point_t p2 = horizon[j].second;
            facet_t newFacet = {p1, p2, p};
            //std::cout << &newFacet << " after creation\n";
            edgePoints_t ee = {p1, p2};
            //output.printFacesAdresses();std::cout<<'\n';
            output.addFacet(newFacet);
            facet_t* addedFacet = &(*output.m_faces.rbegin());

            int prevSize = horizon.size();
            updateConflicts(input, addedFacet, ee, i, horizon, output);
            int newSize = horizon.size();
            //because update conflictst can modify horizon (if i find coplanar face with more than one common edge, i will delelete thsi 
            //edges from horizon to prevent adding the same face multiple times) I need to decrease j if i did so to go through all horizon edges
            if (prevSize != newSize) {
               j--;
            }
         }
         discardVisibleFaces(i, output); //discarding from output         
      }
   }
   output.removeFacetsByPointers(facesToDelete);
   return output;
}

Polyhedron& SolverIncrementalOptimized::SolveParallel(data_t& input, Polyhedron& output, int numThreads){
//same as normal incremental but uses this version for partial hulls  
   if (numThreads == 1) {
      Solve(input, output);
      return output;
   }
   //sort points by x coordinate

   if (input.size() < 100) {
      Solve(input, output);
      return output;
   }

   omp_set_num_threads(numThreads);
   data_t sortedPoints = input;
   std::sort(sortedPoints.begin(), sortedPoints.end(), comparePointsByXCoord);

   //divide into sections   
   std::vector<Polyhedron> partialResults;
   unsigned int newRatio = sortedPoints.size() / numThreads;
   std::vector<data_t> partialInputs;
   Polyhedron p;

   unsigned int oldRatio = 0;
   unsigned int ratio = newRatio;
   while (newRatio < sortedPoints.size()) {
      data_t points;
      partialResults.push_back(p);
      for (unsigned int i = oldRatio; i < newRatio; i++) {
         points.push_back(sortedPoints[i]);
      }
      partialInputs.push_back(points);
      oldRatio = newRatio;
      newRatio += ratio;
   }
   //last section
   //last section -- put the points into last partial input
   data_t points;
   for (unsigned int i = oldRatio; i < sortedPoints.size(); i++) {
      points.push_back(sortedPoints[i]);
   }
   if (points.size() < 4) {
      for (point_t pt : points)
         partialInputs[partialInputs.size() - 1].push_back(pt);
   } else {
      partialInputs.push_back(points);
      partialResults.push_back(p);
   }

   //solve each section by one thread

#pragma omp parallel for schedule(static)
   for (unsigned i = 0; i < partialInputs.size(); i++) {
      SolverIncrementalOptimized s; //because this version operates with pointers, I need to solve each part on separate solver
      s.Solve(partialInputs[i], partialResults[i]);
      //printOpenScadEachFacet(partialResults[i]);
   }
   std::vector<Polyhedron> partialResultsCopy = partialResults;

   //merge linear
   /*for (int i = 0; i < partialResults.size() - 1; i++) {
      edgePoints_t lowerTangent = findLowerTangent(partialResults[0], partialResults[i + 1]);
      //std::cout << "lowerTang: "; printPoint(lowerTangent.first); std::cout << " - ";  printPoint(lowerTangent.second);
      //std::cout << "merging on thr " << omp_get_thread_num();
      mergeTwoHullsOptimized(partialResults[0], partialResults[i + 1], lowerTangent);
     
   }*/
#pragma omp parallel
#pragma omp single
   recursiveMerge(partialResultsCopy, 0, partialResults.size() - 1);

   output = partialResultsCopy[0];
   return output;
}

facet_t SolverIncrementalOptimized::dealWithCoplanarFace(point_t& p, std::vector<edgePoints_t>& horizon, facet_t* coplanarFacet) {

   //determine which horizon edges lie on the coplanar face
   std::vector<edgePoints_t> edgesToRemove;
   for (unsigned int k = 0; k < horizon.size(); k++) {
      edgePoints_t edge = horizon[k];

      if (edgeIsOnFacet(coplanarFacet, edge)) {
         edgesToRemove.push_back(edge);
         //Can i delete the edges from horizon here to not work with them in the next step??         
         horizon.erase(horizon.begin() + k);
         k--;
      }
   }

   //change the orientation for all the edges i am testing on the coplanar facet
   //REASON - I found these edges on the facets neighbouring with the coplanar one, so on the coplanar, they are reversed
   for (unsigned int q = 0; q < edgesToRemove.size(); q++) {
      point_t tmp = edgesToRemove[q].first;
      edgesToRemove[q].first = edgesToRemove[q].second;
      edgesToRemove[q].second = tmp;

   }

   //now find two border points, from these i will make new edges.. 
   point_t borderPoint1;
   int idx1 = 0;
   point_t borderPoint2;
   int idx2 = 0;
   findBorderPoints(borderPoint1, borderPoint2, edgesToRemove);

   for (unsigned int o = 0; o < coplanarFacet->size(); o++) {
      if (coplanarFacet->at(o) == borderPoint1)
         idx1 = o;
      if (coplanarFacet->at(o) == borderPoint2)
         idx2 = o;
   }

   facet_t newFacet;
   //delete all points between border points and put the new point instead
   if (edgesToRemove.size() == 1) {
      //put the point in between
      newFacet = *coplanarFacet;
      if (idx1 < idx2) {
         newFacet.insert(newFacet.begin() + idx2, p); // insert before point on idx2
      } else {
         newFacet.push_back(p);
      }
   } else {
      if (idx1 < idx2) {
         //put the new point and points lying in between idx2 and idx1
         newFacet.push_back(p);
         for (unsigned int o = idx2; o < coplanarFacet->size(); o++) {
            newFacet.push_back(coplanarFacet->at(o));
         }
         for (unsigned int o = 0; o <= idx1; o++) {
            newFacet.push_back(coplanarFacet->at(o));
         }
      } else {
         //need to delete from idx2 to the end end from the beginning to idx1
         newFacet.push_back(p);
         for (int o = idx2; o <= idx1; o++)
            newFacet.push_back(coplanarFacet->at(o));
      }
   }


   for (unsigned int i = 0; i < coplanarFacet->size(); i++) {
      edgePoints_t edge = {coplanarFacet->at(i), coplanarFacet->at((i + 1) % coplanarFacet->size())};
      point_t v1p = {p[0] - edge.first[0], p[1] - edge.first[1], p[2] - edge.first[2]};
      point_t v12 = {edge.second[0] - edge.first[0], edge.second[1] - edge.first[1], edge.second[2] - edge.first[2]};
      point_t v21 = {edge.first[0] - edge.second[0], edge.first[1] - edge.second[1], edge.first[2] - edge.second[2]};
      normalize(v1p);
      normalize(v12);
      normalize(v21);
      if (v1p == v12) {
         //remove point 2
         for (unsigned int j = 0; j < newFacet.size(); j++) {
            if (newFacet[j] == edge.second) {
               newFacet.erase(newFacet.begin() + j);
               break;
            }
         }
      }
      if (v1p == v21) {
         //remove point 1
         for (unsigned int j = 0; j < newFacet.size(); j++) {
            if (newFacet[j] == edge.first) {
               newFacet.erase(newFacet.begin() + j);
               break;
            }
         }
      }
   }
   return newFacet;
}

void SolverIncrementalOptimized::findHorizon(int idx, Polyhedron& output, std::vector<edgePoints_t>& horizon) {

   //if only one face is visible, all its edges will be horizon
   if (pointConflicts.find(idx)->second.size() == 1) {
      facet_t* visibleFacet = pointConflicts.find(idx)->second.at(0);
      for (unsigned int p = 0; p < (*visibleFacet).size(); p++) {
         edgePoints_t e = {(*visibleFacet)[p], (*visibleFacet)[((p + 1) % (*visibleFacet).size())]};
         horizon.push_back(e);
      }
   } else {
      //for each face
      for (facet_t* visibleFacet : pointConflicts.find(idx)->second) {
         //for face's each edge
         for (unsigned int j = 0; j < (*visibleFacet).size(); j++) {
            edgePoints_t edge = {(*visibleFacet)[j], (*visibleFacet)[((j + 1) % (*visibleFacet).size())]};
            bool edgeFoundOnSomeFace = 0;
            for (facet_t* testedFacet : pointConflicts.find(idx)->second) {
               if (visibleFacet == testedFacet) continue; //dont test the facet I am working with               
               //edge is not on some of the other visible faces, meaning this edge is on the horizon
               if (edgeIsOnFacet(testedFacet, edge)) {
                  edgeFoundOnSomeFace = 1;
               }
            }
            if (edgeFoundOnSomeFace == 0)
               horizon.push_back(edge);
         }
      }
   }

}

bool SolverIncrementalOptimized::edgeIsOnFacet(facet_t* f, edgePoints_t& e) {   
   for (unsigned int j = 0; j < (*f).size(); j++) {
      edgePoints_t testedEdge = {(*f)[j], (*f)[(j + 1) % (*f).size()]};
      if (testedEdge.first == e.first && testedEdge.second == e.second)
         return 1;
      if (testedEdge.first == e.second && testedEdge.second == e.first)
         return 1;
   }
   return 0;
}

bool SolverIncrementalOptimized::pointIsInsideHull(int idx, Polyhedron& output) {
   if (pointConflicts.find(idx)->second.size() == 0) {//this point cant see any facet, meaning it is inside
      return 1;
   }
   return 0;
}

void SolverIncrementalOptimized::initConflicts(data_t& input, Polyhedron& output) {
   for (unsigned int i = 0; i < input.size(); i++) {
      std::vector<facet_t*> dummy;
      pointConflicts.insert({i, dummy});
   }

   facet_t* f0 = output.getFacePointer(0);
   facet_t* f1 = output.getFacePointer(1);
   facet_t* f2 = output.getFacePointer(2);
   facet_t* f3 = output.getFacePointer(3);

   std::vector<int> dummy2;
   facetConflicts.insert({f0, dummy2});
   facetConflicts.insert({f1, dummy2});
   facetConflicts.insert({f2, dummy2});
   facetConflicts.insert({f3, dummy2});

   //for each point find facets it can see
   for (unsigned int i = 0; i < input.size(); i++) {
      if (pointCanSeeFacet(input[i], f0)) {
         pointConflicts.find(i)->second.push_back(f0);
         facetConflicts.find(f0)->second.push_back(i);
      }
      if (pointCanSeeFacet(input[i], f1)) {
         pointConflicts.find(i)->second.push_back(f1);
         facetConflicts.find(f1)->second.push_back(i);
      }
      if (pointCanSeeFacet(input[i], f2)) {
         pointConflicts.find(i)->second.push_back(f2);
         facetConflicts.find(f2)->second.push_back(i);
      }
      if (pointCanSeeFacet(input[i], f3)) {
         pointConflicts.find(i)->second.push_back(f3);
         facetConflicts.find(f3)->second.push_back(i);
      }
   }
}

bool SolverIncrementalOptimized::pointCanSeeFacet(point_t& point, facet_t* facet) {

   point_t v12 = {(*facet)[1][0] - (*facet)[0][0], (*facet)[1][1] - (*facet)[0][1], (*facet)[1][2] - (*facet)[0][2]};
   point_t v13 = {(*facet)[2][0] - (*facet)[0][0], (*facet)[2][1] - (*facet)[0][1], (*facet)[2][2] - (*facet)[0][2]};
   point_t vperp = perpendNormal3d(v12, v13);

   point_t v14 = {point[0] - (*facet)[0][0], point[1] - (*facet)[0][1], point[2] - (*facet)[0][2]};
   double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);

   if (angleCos > EPS_LOC) {
      return 1;
   }
   return 0;
}

void SolverIncrementalOptimized::printConflicts() {
   std::cout << "Point conflicts:" << std::endl;
   for (auto it = pointConflicts.begin(); it != pointConflicts.end(); it++) {
      std::cout << (*it).first << ":" << std::endl;
      std::vector<facet_t*> vf = (*it).second;
      for (unsigned int i = 0; i < vf.size(); i++) {
         printFacet(*(vf[i]));
         std::cout << std::endl;
      }
   }

   std::cout << std::endl << "Faces conflicts:" << std::endl;
   for (auto it = facetConflicts.begin(); it != facetConflicts.end(); it++) {
      //if ((*it).second.size() == 0) continue;
      std::cout << "facet ---- @ " << (*it).first << " \n";
      printFacet(*(*it).first);
      std::cout << "      ----";
      std::cout << "\n is visible from ";
      for (unsigned int i = 0; i < (*it).second.size(); i++) {
         int p = (*it).second.at(i);
         std::cout << p << " ";
      }
      std::cout << std::endl;
   }
}

void SolverIncrementalOptimized::discardVisibleFaces(int idx, Polyhedron& output) {
   //for each visible facet
   for (auto ii = pointConflicts.find(idx)->second.begin(); ii != pointConflicts.find(idx)->second.end();) {
      facet_t* toDiscard = *ii;
      if (debug)
         std::cout << "discarding " << toDiscard << std::endl;
      //remove facet from the point conflict list
      //for each point look if it can see the facet to be discarded, if so, delete it from the list
      for (auto it = pointConflicts.begin(); it != pointConflicts.end(); it++) {
         for (unsigned int i = 0; i < (*it).second.size(); i++) {
            if ((*it).second.at(i) == toDiscard) {
               (*it).second.erase((*it).second.begin() + i);
               break;
            }
         }
      }
      //delete it from output      
      //output.removeFacetByPointer(toDiscard);
      facesToDelete.push_back(toDiscard);
      facetConflicts.erase(toDiscard);
   }
   pointConflicts.erase(idx);
}

void SolverIncrementalOptimized::updateConflicts(data_t& input, facet_t* newFacet, edgePoints_t& edge, int idx, std::vector<edgePoints_t>& horizon
        , Polyhedron& output) { //edge is currently added edge and idx is index of point we are adding
   //find two facets incident to the edge
   if (debug) {
      std::cout << "edge ";
      printPoint(edge.first);
      printPoint(edge.second);
      std::cout << "\n";
   }
   //printFacet(*newFacet);

   bool first = 1;

   facet_t* f1;
   facet_t* f2;


   //std::cout<<facetConflicts.size() << "SIZE\n";
   //printConflicts();
   for (auto it = facetConflicts.begin(); it != facetConflicts.end(); ++it) {
      facet_t* testedFacet = it->first;

      if (edgeIsOnFacet(testedFacet, edge)) {
         if (first == 1) {
            f1 = testedFacet;
            first = 0;
         } else {
            f2 = testedFacet;
            break; //both faces found

         }
      }
   }

   //test coplanarity
   if (isCoplanar(input[idx], f1)) {
      //deal with coplanar facet
      //create new face by connecting the point to the coplanar one
      facet_t mergedFacet = dealWithCoplanarFace(input[idx], horizon, f1);
      //remove old facets
      facesToDelete.push_back(f1);
      if(debug)
        std::cout << "Want to delete facet f1@" << f1 << std::endl;
      output.removeFacetByPointer(newFacet); //this one i can HOPEFULLY delete right now because it is the last one in the deque
      //add merged facet to output
      output.addFacet(mergedFacet);
      facet_t* addedFacet = &(*output.m_faces.rbegin());
      //added facet will have same conglicts as f1
      std::vector<int> conflicts = facetConflicts.find(f1)->second;
      facetConflicts.insert({addedFacet, conflicts});
      for (int i : facetConflicts.find(f1)->second) {
         pointConflicts.find(i)->second.push_back(addedFacet);
      }

      //THIS IS TO DELETE THE OLD FACE FFROM CONFLICT LIST
      pointConflicts.find(idx)->second.push_back(f1);
      return;
   }

   if (isCoplanar(input[idx], f2)) {
      //deal with coplanar facet
      //create new face by connecting the point to the coplanar one
      facet_t mergedFacet = dealWithCoplanarFace(input[idx], horizon, f2);
      //remove old facets
      facesToDelete.push_back(f2);
      if(debug)
        std::cout << "Want to delete facet f2 @" << f2 << std::endl;
      output.removeFacetByPointer(newFacet); //this one i can HOPEFULLY delete right now because it is the last one in the deque
      //add merged facet to output
      output.addFacet(mergedFacet);
      facet_t* addedFacet = &(*output.m_faces.rbegin());
      //added facet will have same conglicts as f1
      std::vector<int> conflicts = facetConflicts.find(f2)->second;
      facetConflicts.insert({addedFacet, conflicts});
      for (int i : facetConflicts.find(f2)->second) {
         pointConflicts.find(i)->second.push_back(addedFacet);
      }
      //THIS IS TO DELETE THE OLD FACE FFROM CONFLICT LIST
      pointConflicts.find(idx)->second.push_back(f2);

      return;
   }

   std::vector<int> dummy;
   facetConflicts.insert({newFacet, dummy});

   //for each point that can see f1 or f2 compute if they can see the new facet
   for (int i : facetConflicts.find(f1)->second) {
      if (pointCanSeeFacet(input[i], newFacet)) {
         pointConflicts.find(i)->second.push_back(newFacet);
         facetConflicts.find(newFacet)->second.push_back(i);
         if (debug)
            std::cout << "Adding to conflicts: " << i;
      }
   }
   for (int i : facetConflicts.find(f2)->second) {
      if (pointCanSeeFacet(input[i], newFacet)) {
         if (isInVector(facetConflicts.find(newFacet)->second, i)) {
            if (debug)
               std::cout << "Already added point in prev iteration\n";
         } else {
            pointConflicts.find(i)->second.push_back(newFacet);
            facetConflicts.find(newFacet)->second.push_back(i);
            if (debug)
               std::cout << "Adding to conflicts: " << i;
         }
      }
   }
}

bool SolverIncrementalOptimized::isCoplanar(point_t& point, facet_t* facet) {
   point_t v12 = {facet->at(1)[0] - facet->at(0)[0], facet->at(1)[1] - facet->at(0)[1], facet->at(1)[2] - facet->at(0)[2]};
   point_t v13 = {facet->at(2)[0] - facet->at(0)[0], facet->at(2)[1] - facet->at(0)[1], facet->at(2)[2] - facet->at(0)[2]};
   point_t vperp = perpendNormal3d(v12, v13);

   point_t v14 = {point[0] - facet->at(0)[0], point[1] - facet->at(0)[1], point[2] - facet->at(0)[2]};
   double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);

   if (fabs(angleCos) < EPS_LOC) {
      if (debug) {
         std::cout << "found coplanar point ";
         printPoint(point);
         std::cout << " with face ";
         printFacet(*facet);
         std::cout << " during addition " << std::endl;
         std::cout << "anglecos: " << angleCos << std::endl;
      }
      return 1;
   }
   return 0;
}