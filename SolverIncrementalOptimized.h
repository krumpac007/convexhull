/* 
 * File:   SolverIncrementalOptimized.h
 * Author: motykvac
 *
 * Created on October 23, 2017, 10:54 AM
 */

#ifndef SOLVERINCREMENTALOPTIMIZED_H
#define	SOLVERINCREMENTALOPTIMIZED_H

#include "geometry.h"
#include "functions.h"
#include "structures.h"
#include "HalfEdge.h"
#include "SolverIncremental.h"

#include <set>
#include <iostream>
#include <string>
#include <list>
#include <map>
#include <set>
class SolverIncrementalOptimized : SolverIncremental{
public:
   SolverIncrementalOptimized() {         
      addedPointIsCoplanar = 0;                 
   };
   Polyhedron& Solve(data_t& input, Polyhedron& output);
   Polyhedron& SolveParallel(data_t& input, Polyhedron& output, int numThreads);
private:
   int numThreads;   
   bool addedPointIsCoplanar;   
   std::vector<int> addedPointIsCoplanarWithFaces; //idx of facet added point is coplanar with   
   std::set<unsigned> initialTetrahedronIdxs;
   
   Polyhedron& SolveNaive(data_t& input, Polyhedron& output);
   bool pointIsInsideHull(int idx, Polyhedron& output);
   bool edgeIsOnFacet(facet_t* f, edgePoints_t& e);
   void findHorizon(int idx, Polyhedron& output, std::vector<edgePoints_t>& horizon);
   facet_t dealWithCoplanarFace(point_t& p, std::vector<edgePoints_t>& horizon, facet_t* coplanarFacet);
   void initConflicts(data_t& input, Polyhedron& output);
   bool pointCanSeeFacet(point_t& point, facet_t* facet);
   void discardVisibleFaces(int idx, Polyhedron& output);
   void updateConflicts(data_t& input, facet_t* newFacet, edgePoints_t& edge, int idx, std::vector<edgePoints_t>& horizon, Polyhedron& output);
   bool isCoplanar(point_t& p, facet_t* f);
   void printConflicts();
  
    bool isInVector(std::vector<int>& v, int i){
      for(unsigned int k = 0; k < v.size(); k++){
         if(v[k] == i)
            return 1;
      }
      return 0;
   };
   
   //conflict graph for points
   std::map<int, std::vector<facet_t*> > pointConflicts; //for each point remember faces it can see
   //conflict graph for facets
   std::map<facet_t*, std::vector<int> > facetConflicts; //for each facet remember points that can see it
   std::vector<facet_t*> facesToDelete;   
};

#endif	/* SOLVERINCREMENTALOPTIMIZED_H */

