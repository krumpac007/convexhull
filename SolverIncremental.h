/* 
 * File:   SolverIncremental.h
 * Author: motykvac
 *
 * Created on October 11, 2017, 8:31 AM
 */

#ifndef SOLVERINCREMENTAL_H
#define	SOLVERINCREMENTAL_H

#include "geometry.h"
#include "functions.h"
#include "structures.h"
#include <set>
#include <iostream>
#include <string>
#include <set>

class SolverIncremental : Solver3D {
public:
   SolverIncremental() {
      m_name = "Incremental";     
   };
   virtual Polyhedron& Solve(data_t& input, Polyhedron& output);
   virtual Polyhedron& SolveParallel(data_t& input, Polyhedron& output, int numThr);

protected:
   enum Direction {
      DOWN, TOP, LEFT, RIGHT, FRONT, BACK
   };

   Polyhedron& makeTetrahedron(const data_t& input, Polyhedron& output, const std::set<unsigned>& pointIdxs);
   unsigned findInitialPoint(const data_t& input, const Direction direction = DOWN);
   Polyhedron& findInitialTetrahedron(const data_t& input, Polyhedron& output, std::set<unsigned>& initialTetrahedronIdxs);
   Polyhedron& findBasicTetrahedron(const data_t& input, Polyhedron& output, std::set<unsigned>& initialTetrahedronIdxs);
   void findBorderPoints(point_t& borderPoint1, point_t& borderPoint2, const std::vector<edgePoints_t>& edges);
   void printHorizon(std::vector<edgePoints_t>& horizon);
private:
   int numThreads;
   bool pointIsInsideHull(point_t& point, Polyhedron& output, std::set<unsigned>& visibleFacesIdxs, bool& addedPointIsCoplanar,
           std::vector<unsigned>& addedPointIsCoplanarWithFaces);
   bool edgeIsOnFacet(facet_t& f, edgePoints_t& e);
   void findHorizon(std::set<unsigned>& visibleFacesIdxs, Polyhedron& output, std::vector<edgePoints_t>& horizon);
   void dealWithCoplanarFaces(Polyhedron& output, point_t& currentPoint, std::set<unsigned>& visibleFacesIdxs,
           std::vector<edgePoints_t>& horizon, std::vector<unsigned>& addedPointIsCoplanarWithFaces);
   void dealWithOneCoplanarFace(Polyhedron& output, point_t& p, std::set<unsigned>& visibleFacesIdxs,
           std::vector<edgePoints_t>& horizon, std::vector<unsigned>& addedPointIsCoplanarWithFaces);
   void printInput(data_t&);
};

#endif	/* SOLVERINCREMENTAL_H */

