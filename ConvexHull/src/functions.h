/* 
 * File:   functions.h
 * Author: motykvac
 *
 * Created on February 24, 2017, 4:17 PM
 */



#ifndef FUNCTIONS_H
#define	FUNCTIONS_H

#include <vector>
#include <iostream>
#include <fstream>
#include "structures.h"

void printPoint(point_t& p){
   for(int i = 0; i < p.size(); i++)
      std::cout << p[i] << " ";
   std::cout << std::endl;
}

void printAllPoints(std::vector<point_t>& v){
   for(int i = 0; i<v.size(); i++){
      printPoint(v[i]);
      //std::cout << std::endl;
   }
}

//prints input points in geomview
void printGeomView(data_t data){
   std::ofstream file;
   file.open("./in/geom");
   file << "LIST" <<std::endl;
   for(int i = 0; i < data.size(); i++){
      point_t point = data[i];
      file << "SPHERE 0.1 " << point[0] << " " << point[1] << " "  << point[2] << std::endl;
   }
   
   file.close();
}
void printJava(data_t data){
   std::ofstream file;
   file.open("./in/java");
   
   for(int i = 0; i < data.size(); i++){
      point_t point = data[i];
      file << "new Point3d (" << point[0] << "," << point[1] << ","  << point[2] <<")," << std::endl;
   }
   
   file.close();
}

#endif	/* FUNCTIONS_H */

