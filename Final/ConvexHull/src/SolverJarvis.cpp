#include <vector>

#include "SolverJarvis.h"
#include "geometry.h"
#include <climits>
#include <set>
#include <cfloat>
#include <ctime>

Polyhedron& SolverJarvis::Solve(data_t& input, Polyhedron& output){
       // edge cases
    if (input.size() == 0) {
        return output;
    } else if (input.size() < 3) {
        facet_t plane;
        const data_t& inputData = input;
        for (auto& i : inputData) {
            plane.push_back(i);
        }
        output.addFacet(plane);
        return output;
    }

    SolveNaive(input, output);

    return output;
}

Polyhedron& SolverJarvis::SolveNaive(data_t& input, Polyhedron& output){
   const data_t& idata = input;
    typedef std::pair<unsigned, unsigned> edge_t;
    edge_t init = findInitial(idata);
    // discovered and processed edges
    std::set<edge_t> fresh, closed;

    do {
        // points a, b
        edge_t curr = {init.first, init.second};
        if (fresh.size() > 0) {
            curr = *(fresh.begin());
        }
        point_t vab = {idata[curr.first][0] - idata[curr.second][0],
                       idata[curr.first][1] - idata[curr.second][1],
                       idata[curr.first][2] - idata[curr.second][2]};

        point_t vcb, vperp;
        unsigned c = UINT_MAX;
        // find some non-collinear c
        for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b
            if (i == curr.first || i == curr.second) {
                continue;
            }
            vcb = {idata[i][0] - idata[curr.second][0],
                   idata[i][1] - idata[curr.second][1],
                   idata[i][2] - idata[curr.second][2]};
            // find vector perpendicular to abc
            vperp = perpend3d(vab, vcb);
            // if c not collinear, use it
            if (!zeroVect(vperp)) {
                c = i;
                break;
            }
        }

        // all input points on one line
        if (c == UINT_MAX) {
            R("collinear set");
            return output;
        }

        unsigned currC = c;
        std::vector<unsigned> onPlane;
        onPlane.push_back(c);
        for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b and initial c
            if (i == curr.first || i == curr.second || i == c) {
                continue;
            }

            // compute direction, if > 0 its on one side of the normal vector... we dont care which?
            double dd = dot(idata[i][0] - idata[curr.second][0],
                            idata[i][1] - idata[curr.second][1],
                            idata[i][2] - idata[curr.second][2],
                            vperp[0], vperp[1], vperp[2]);

            //c is on the same side normal vector goes
            if (dd > EPS_LOC) {
                // new c
                currC = i; 
                vcb = {idata[currC][0] - idata[curr.second][0],
                       idata[currC][1] - idata[curr.second][1],
                       idata[currC][2] - idata[curr.second][2]};
                vperp = perpendNormal3d(vab, vcb);
                onPlane.clear();
                onPlane.push_back(i);
            } else if (fabs(dd) < EPS_LOC) {
                // on the same plane as abc
                onPlane.push_back(i);
            }
        }

        c = currC;

        // PROBLEM
        // this returns all points on faces, but I only need their convex hull
        // we need a way to find convex hull of points on plane, but in 3d
        
        // POSSIBLE SOLUTION
        // eliminate one non-zero coordinate, otherwise the points would be on line after elimination of a zero coord
        // normal vector has at least one non zero
        
        onPlane.push_back(curr.first);
        onPlane.push_back(curr.second);

        facet_t planar; // 3d points on plane converted to 2d
        std::pair<int, int> coords; // id of coordinates to use
        if (fabs(vperp[0]) > EPS_LOC) {
            coords = {1, 2};
        } else if (fabs(vperp[1]) > EPS_LOC) {
            coords = {0, 2};
        } else {
            coords = {0, 1};
        }

        for (auto& i : onPlane) {
            planar.push_back({idata[i][coords.first], idata[i][coords.second]});
        }

        // find points on diameter of face in ccw order
        GrahamScan2D solver;
        std::vector<unsigned> faceID;
        solver.solveID(planar, faceID);
        unsigned fs = faceID.size();



        //
        bool flipped = 0;
        for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                     ey = onPlane[faceID[(i+1) % fs]];
            if (ex == curr.second && ey == curr.first) {
                flipped = 1;
            }
        }

        // add new found edges to list, remove already found ones
        for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                     ey = onPlane[faceID[(i+1) % fs]];
            edge_t oe = {ex, ey};
            if (ex >= ey) {
                oe = {ey, ex};
            }
            if (flipped) {
                std::swap(ex, ey);
            }
            if (closed.find(oe) != closed.end()) {

                fresh.erase({ey, ex});
                fresh.erase({ex, ey});
            } else {

                fresh.insert({ey, ex});
                closed.insert(oe);
            }
        }

        facet_t face;
        facet_idx_t face_idx; //facet made of indexes, not points
        for (auto i : faceID) {
            face.push_back(idata[onPlane[i]]);
            face_idx.push_back(onPlane[i]);
        }
        output.addFacet(face);
        output.addFacetIdx(face_idx);
    } while (!fresh.empty());

    // polyhedron with two faces means a plane => remove one of the faces
    if (output.getSize() == 2) {
        output.popFacet();
    }

    return output;
}

Polyhedron& SolverJarvis::SolveParallel(data_t& input, Polyhedron& output, int numThr){
    const data_t& idata = input;
    typedef std::pair<unsigned, unsigned> edge_t;
    edge_t init = findInitial(idata);
    // discovered and processed edges
    std::set<edge_t> fresh, closed;

    do {
        // points a, b
        edge_t curr = {init.first, init.second};
        if (fresh.size() > 0) {
            curr = *(fresh.begin());
        }
        point_t vab = {idata[curr.first][0] - idata[curr.second][0],
                       idata[curr.first][1] - idata[curr.second][1],
                       idata[curr.first][2] - idata[curr.second][2]};

        //point_t can be a point or vector
        point_t vcb, vperp;
        unsigned c = UINT_MAX;
        // find some non-collinear c
        for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b
            if (i == curr.first || i == curr.second) {
                continue;
            }
            vcb = {idata[i][0] - idata[curr.second][0],
                   idata[i][1] - idata[curr.second][1],
                   idata[i][2] - idata[curr.second][2]};
            // find vector perpendicular to abc
            vperp = perpend3d(vab, vcb);
            // if c not collinear, use it
            if (!zeroVect(vperp)) {
                c = i;
                break;
            }
        }

        // all input points on one line
        if (c == UINT_MAX) {
            return output;
        }

        unsigned currC = c;
        std::vector<unsigned> onPlane;
        onPlane.push_back(c);
        for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b and initial c
            if (i == curr.first || i == curr.second || i == c) {
                continue;
            }

            // compute direction, if > 0 its on one side of the normal vector... we dont care which?
            double dd = dot(idata[i][0] - idata[curr.second][0],
                            idata[i][1] - idata[curr.second][1],
                            idata[i][2] - idata[curr.second][2],
                            vperp[0], vperp[1], vperp[2]);

            //c is on the same side normal vector goes
            if (dd > EPS_LOC) {
                // new c
                currC = i; 
                vcb = {idata[currC][0] - idata[curr.second][0],
                       idata[currC][1] - idata[curr.second][1],
                       idata[currC][2] - idata[curr.second][2]};
                vperp = perpendNormal3d(vab, vcb);
                onPlane.clear();
                onPlane.push_back(i);
            } else if (fabs(dd) < EPS_LOC) {
                // on the same plane as abc
                onPlane.push_back(i);
            }
        }

        c = currC;

        // PROBLEM
        // this returns all points on faces, but I only need their convex hull
        // we need a way to find convex hull of points on plane, but in 3d
        
        // POSSIBLE SOLUTION
        // eliminate one non-zero coordinate, otherwise the points would be on line after elimination of a zero coord
        // normal vector has at least one non zero
        
        onPlane.push_back(curr.first);
        onPlane.push_back(curr.second);

        facet_t planar; // 3d points on plane converted to 2d
        std::pair<int, int> coords; // id of coordinates to use
        if (fabs(vperp[0]) > EPS_LOC) {
            coords = {1, 2};
        } else if (fabs(vperp[1]) > EPS_LOC) {
            coords = {0, 2};
        } else {
            coords = {0, 1};
        }
   
        for (auto& i : onPlane) {
            planar.push_back({idata[i][coords.first], idata[i][coords.second]});
        }

        // find points on diameter of face in ccw order
        GrahamScan2D solver;
        std::vector<unsigned> faceID;
        solver.solveID(planar, faceID);
        unsigned fs = faceID.size();
        //
        bool flipped = 0;
        for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                     ey = onPlane[faceID[(i+1) % fs]];
            if (ex == curr.second && ey == curr.first) {

                flipped = 1;
            }
        }

        // add new found edges to list, remove already found ones
        for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                     ey = onPlane[faceID[(i+1) % fs]];
            edge_t oe = {ex, ey};
            if (ex >= ey) {
                oe = {ey, ex};
            }
            if (flipped) {
                std::swap(ex, ey);
            }
            if (closed.find(oe) != closed.end()) {
                fresh.erase({ey, ex});
                fresh.erase({ex, ey});
            } else {

                fresh.insert({ey, ex});
                closed.insert(oe);
            }
        }

        facet_t face;
        facet_idx_t face_idx; //facet made of indexes, not points
        for (auto i : faceID) {
            face.push_back(idata[onPlane[i]]);
            face_idx.push_back(onPlane[i]);
        }
        output.addFacet(face);
        output.addFacetIdx(face_idx);
    } while (!fresh.empty());

    // polyhedron with two faces means a plane => remove one of the faces
    if (output.getSize() == 2) {
        output.popFacet();
    }

    return output;
}



std::pair<unsigned, unsigned> SolverJarvis::findInitial(const data_t& input)
{
    srand(time(NULL));
    int rndx, rndy, rndz;
    double maxd;
    unsigned far; //the furthest point    

    // find farthest point in downwards direction
    far = 0;
    maxd = DBL_MIN;
    rndx = 0; rndy = -1; rndz = 0;
    //najde minimum tim ze znuluje druhe dve 
    for (unsigned i = 0; i < input.size(); i++) {
        double d = dot(input[i][0], input[i][1], input[i][2], 
                rndx,        rndy,        rndz);
        double dif = d - maxd;
        if (dif > EPS_LOC) {
            maxd = d;
            far = i;
        }
    }

    unsigned paired = 0;
    maxd = DBL_MAX;
    double minLen = DBL_MAX;
    // find second point on edge = point with smallest angle to plane
    // perpendicular to rng vector from previous step
    for (unsigned i = 0; i < input.size(); i++) {
        if (i == far) {
            continue;
        }
        //vector s bodem
        point_t vFarI = {input[far][0] - input[i][0],
                         input[far][1] - input[i][1],
                         input[far][2] - input[i][2]};
        double iLen = sqrt(vFarI[0]*vFarI[0] + vFarI[1]*vFarI[1]
                           + vFarI[2]*vFarI[2]);
        // directional distance
        double d = dot(vFarI[0], vFarI[1], vFarI[2], 
                       rndx,     rndy,     rndz);
        d /= iLen;
        //d je cosinus uhlu protoze vector normalovy ma vel. 1
        
        double dif = d - maxd;
        //kdyz je cosinus mensi nez aktualni nejmensi, tzn ze uhel s normalem je vetsi, takze uhel s rovinou mensi, uloz tenhle bod
        if (dif < -EPS_LOC) {
            paired = i;
            maxd = d;
            minLen = iLen;
            //pokud tento bod lezi na rovine s zatim najitym nejmensim (cosinus je v mezi epsilonu), zkontroluj, ktery je k uplne prvnimu nal. bodu nejbliz
        } else if (fabs(dif) < EPS_LOC) {
            if (iLen < minLen - EPS_LOC) {
                paired = i;
                minLen = iLen;
            }
        }
    }
    //vracim bod nejniz a ten co s nim svira nejmensi uhel a ej nejbliz
    return {far, paired};
}

