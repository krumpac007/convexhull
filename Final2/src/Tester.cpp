#include "Tester.h"
#include "SolverJarvis.h"
#include "SolverIncremental.h"
#include "SolverIncrementalOptimized.h"
#include "SolverQuickHull.h"
#include "SolverDivide.h"
#include <sstream>
#include <iostream>
#include <string>
#include <fstream>
#include <time.h>

void Tester::RandomTest(std::vector<std::string> algorithms, int maxNumberOfPoints, int numberOfTests) {
   srand(time(NULL));

   for (int i = 0; i < numberOfTests; i++) {
      int numberOfPoints = std::rand() % (maxNumberOfPoints - 20 + 1) + 20;
      RandomTest(algorithms, numberOfPoints, numberOfPoints, 1);
   }

}

void Tester::RandomTest(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests) {
   int ratio = (maxNumberOfPoints - minNumberOfPoints) / numberOfTests;
   srand(time(NULL));
   for (std::string algorithm : algorithms) {
      for (int i = 0; i < numberOfTests; i++) {
         int numberOfPoints = minNumberOfPoints + i*ratio;
         char types [2] = {'c', 's'};
         char type = types[std::rand() % 2];
         double distribution = (double) std::rand() / (double) RAND_MAX;
         int size = std::rand() % (maxNumberOfPoints - (minNumberOfPoints / 10) + 1) + (minNumberOfPoints / 10); //size between minnumberofpoints/10 - 100000
         int numThreads = std::rand() % (10) + 1;
         std::cout << "Testing: " << algorithm << ", " << numberOfPoints << " points, type " << type << ", distrubution " << distribution << ", size " <<
                 size << " numThreads " << numThreads << ".\n";
         Test({algorithm}, numberOfPoints, numberOfPoints, 1, type, distribution, size, numThreads);
      }
   }
}

void Tester::Test(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests, char type,
        double distribution, unsigned size, unsigned numThreads) {

   //test input
   if (minNumberOfPoints <= 0) {
      std::cout << "Tester error: Negative number of points.";
      return;
   }
   if (maxNumberOfPoints < minNumberOfPoints) {
      std::cout << "Tester error: Max number of points is lower then min.";
      return;
   }
   if (numberOfTests <= 0) {
      std::cout << "Tester error: Not positive number of tests.";
      return;
   }
   if (type != 's' && type != 'c') {
      std::cout << "Tester error: Wrong type of input.";
      return;
   }
   if (distribution < 0 || distribution > 1) {
      std::cout << "Tester error: Wrong distribution type, must be between 0 and 1.";
      return;
   }
   if (size <= 0) {
      std::cout << "Tester error: Negative size.";
      return;
   }
   if (size < 1) {
      std::cout << "Tester error: Wrong number of threads, must be > 0.";
      return;
   }



   //for number of tests make input, test the input in qhull, test the input in selected algorithm and compare
   int ratio = (maxNumberOfPoints - minNumberOfPoints) / numberOfTests;
   //sometimes for cube distribution, rbox makes input with multiple same points or other kind of degenerated input, so rather make points very close to the surface
   if(type == 'c' && distribution < 0.05) distribution = 0.05;                                                                
   for (int i = 0; i < numberOfTests; i++) {
      int numberOfPoints = minNumberOfPoints + i*ratio;
      if (type == 'c')
         type = ' ';
      std::string cmd = "rbox " + std::to_string(numberOfPoints) + " B" + std::to_string(size) + " " + type + " n W" + std::to_string(distribution)
              + " D3 > testInput";
      const char * input = cmd.c_str();
      int sys = 0;
      
      sys = system(input);
      m_points.clear();
      parseInput();
      const char * qhull = "qhull -i < testInput > testOutput";
      sys = system(qhull);
      Polyhedron qHullOutput = parseQHullOutput();

      for (std::string algorithm : algorithms) {
         if (algorithm == "jarvis" || algorithm == "all") {
            SolverJarvis solver;
            Polyhedron solverOutput;
            if (numThreads == 1)
               solver.Solve(m_points, solverOutput);
            else
               solver.SolveParallel(m_points, solverOutput, numThreads);
            if (comparePolyhedronsFast(qHullOutput, solverOutput) == 0) {
               std::cout << "Tester: Jarvis - outputs differ.\n";
            } else {
               std::cout << "Tester: Jarvis - outputs OK.\n";
            }
         }
         if (algorithm == "incremental" || algorithm == "all") {
            SolverIncremental solver;
            Polyhedron solverOutput;
            if (numThreads == 1)
               solver.Solve(m_points, solverOutput);
            else
               solver.SolveParallel(m_points, solverOutput, numThreads);
            if (comparePolyhedronsFast(qHullOutput, solverOutput) == 0) {
               std::cout << "Tester: Incremental - outputs differ.\n";
            } else {
               std::cout << "Tester: Incremental - outputs OK.\n";
            }
         }
         if (algorithm == "incrementalOptimized" || algorithm == "all") {
            SolverIncrementalOptimized solver;
            Polyhedron solverOutput;
            if (numThreads == 1)
               solver.Solve(m_points, solverOutput);
            else {
               //std::cout << "Optimized incremental only works with one thread\n";
               solver.SolveParallel(m_points, solverOutput, numThreads);
            }
            if (comparePolyhedronsFast(qHullOutput, solverOutput) == 0) {
               std::cout << "Tester: Incremental optimized - outputs differ.\n";
            } else {
               std::cout << "Tester: Incremental optimized - outputs OK.\n";
            }
         }
         if (algorithm == "quickhull" || algorithm == "all") {
            SolverQuickHull solver;
            Polyhedron solverOutput;
            if (numThreads == 1)
               solver.Solve(m_points, solverOutput);
            else {
               solver.SolveParallel(m_points, solverOutput, numThreads);
            }
            if (comparePolyhedronsFast(qHullOutput, solverOutput) == 0) {
               std::cout << "Tester: Quickhull - outputs differ.\n";
            } else {
               std::cout << "Tester: Quickhull - outputs OK.\n";
            }
         }
         if (algorithm == "divide" || algorithm == "all") {
            SolverDivide solver;
            Polyhedron solverOutput;
            if (numThreads == 1)
               solver.Solve(m_points, solverOutput);
            else {
               solver.SolveParallel(m_points, solverOutput, numThreads);
            }
            if (comparePolyhedronsFast(qHullOutput, solverOutput) == 0) {
               std::cout << "Tester: Divide & Conquer - outputs differ.\n";
            } else {
               std::cout << "Tester: Divide & Conquer - outputs OK.\n";
            }
         }
      }

   }
}

void Tester::TimeTest(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests, char type,
        double distribution, unsigned size, unsigned numThreads) {

   //test input
   if (minNumberOfPoints <= 0) {
      std::cout << "Tester error: Negative number of points.";
      return;
   }
   if (maxNumberOfPoints < minNumberOfPoints) {
      std::cout << "Tester error: Max number of points is lower then min.";
      return;
   }
   if (numberOfTests <= 0) {
      std::cout << "Tester error: Not positive number of tests.";
      return;
   }
   if (type != 's' && type != 'c') {
      std::cout << "Tester error: Wrong type of input.";
      return;
   }
   if (distribution < 0 || distribution > 1) {
      std::cout << "Tester error: Wrong distribution type, must be between 0 and 1.";
      return;
   }
   if (size <= 0) {
      std::cout << "Tester error: Negative size.";
      return;
   }
   if (size < 1) {
      std::cout << "Tester error: Wrong number of threads, must be > 0.";
      return;
   }



   //for number of tests make input, test the input in qhull, test the input in selected algorithm and compare
   int ratio = (maxNumberOfPoints - minNumberOfPoints) / numberOfTests;
   if(type == 'c' && distribution < 0.05) distribution = 0.05;
   for (int i = 0; i < numberOfTests; i++) {
      int numberOfPoints = minNumberOfPoints + i*ratio;
      if (type == 'c')
         type = ' ';
      std::string cmd = "rbox " + std::to_string(numberOfPoints) + " B" + std::to_string(size) + " " + type + " n W" + std::to_string(distribution)
              + " D3 > testInput";
      const char * input = cmd.c_str();
      int sys = 0;
      sys = system(input);
      m_points.clear();
      parseInput();

      double qhullTimeA = omp_get_wtime();
      sys = system("qhull < testInput > /dev/null");

      double qhullTimeB = omp_get_wtime();
      std::cout << "QHull   " << m_points.size() << "    " << -1 << "   " << qhullTimeB - qhullTimeA << "  " << distribution << std::endl;

      for (std::string algorithm : algorithms) {
         if (algorithm == "jarvis" || algorithm == "all") {
            SolverJarvis solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Jarvis   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << std::endl;
         }
         if (algorithm == "incremental" || algorithm == "all") {
            SolverIncremental solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Incremental   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << std::endl;
         }
         if (algorithm == "incrementalOptimized" || algorithm == "all") {
            SolverIncrementalOptimized solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "IncrementalOptimized   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << std::endl;

         }
         if (algorithm == "quickhull" || algorithm == "all") {
            SolverQuickHull solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "QuickHull   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << std::endl;

         }
         if (algorithm == "divide" || algorithm == "all") {
            SolverDivide solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Divide   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << std::endl;

         }
      }

   }
}

void Tester::ThreadTest(std::vector<std::string> algorithms, int minThr, int maxThr,
        int numberOfPoints, char type, double distribution, unsigned size) {

   if (type == 'c')
      type = ' ';
   if(type == ' ' && distribution < 0.05) distribution = 0.05;
   std::string cmd = "rbox " + std::to_string(numberOfPoints) + " B" + std::to_string(size) + " " + type + " n W" + std::to_string(distribution)
           + " D3 > testInput";
   const char * input = cmd.c_str();
   int sys = 0;
   sys = system(input);
   m_points.clear();
   parseInput();
   for (int numThreads = minThr; numThreads <= maxThr; numThreads++) {
      for (std::string algorithm : algorithms) {
         if (algorithm == "jarvis" || algorithm == "all") {
            SolverJarvis solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Jarvis   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << "  " << numThreads << std::endl;
         }
         if (algorithm == "incremental" || algorithm == "all") {
            SolverIncremental solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Incremental   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << "  " << numThreads << std::endl;
         }
         if (algorithm == "incrementalOptimized" || algorithm == "all") {
            SolverIncrementalOptimized solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "IncrementalOptimized   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << "  " << numThreads << std::endl;

         }
         if (algorithm == "quickhull" || algorithm == "all") {
            SolverQuickHull solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "QuickHull   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << "  " << numThreads << std::endl;

         }
         if (algorithm == "divide" || algorithm == "all") {
            SolverDivide solver;
            Polyhedron solverOutput;
            double timeA = omp_get_wtime();
            solver.SolveParallel(m_points, solverOutput, numThreads);
            double timeB = omp_get_wtime();
            std::cout << "Divide   " << m_points.size() << "    " << solverOutput.getNumFaces() << "   " << timeB - timeA << "  " << distribution << "  " << numThreads << std::endl;
         }
      }

   }
}

Polyhedron Tester::parseQHullOutput() {
   std::ifstream inFile("testOutput");

   std::string line;
   std::getline(inFile, line);
   //int numVerts = atoi(line.c_str());

   Polyhedron output;
   while (std::getline(inFile, line)) {
      //each line consist of facet by vert numbers
      std::istringstream iss(line);
      facet_t newFacet;
      int pointIdx;
      while (iss >> pointIdx) {
         point_t point = m_points[pointIdx];
         newFacet.push_back(point);

      }
      output.addFacet(newFacet);
   }
   return output;
}

void Tester::TestGraham() {
   int dummy, np;
   int xcoord = 0, ycoord = 0;
   std::cin >> dummy;
   std::cin >> np;
   std::string line;
   getline(std::cin, line);
   data_t data;
   for (int i = 0; i < np; i++) {
      getline(std::cin, line);
      std::istringstream iss(line);
      iss >> xcoord >> ycoord;
      //cout << xcoord << " " << ycoord << " " << zcoord << endl;
      point_t point;
      point.push_back(xcoord);
      point.push_back(ycoord);
      data.push_back(point);
   }
   GrahamScan2D gs;
   std::vector<unsigned> ids;
   gs.solveID(data,ids);
   
}

void Tester::parseInput() {
   std::ifstream inFile("testInput");
   std::string line;
   std::getline(inFile, line); //first line is dimension
   std::getline(inFile, line); //num of points
   int numPoints = atoi(line.c_str());
   double xcoord, ycoord, zcoord;
   for (int i = 0; i < numPoints; i++) {
      std::getline(inFile, line);
      std::istringstream iss(line);

      iss >> xcoord >> ycoord >> zcoord;

      point_t point;
      point.push_back(xcoord);
      point.push_back(ycoord);
      point.push_back(zcoord);
      m_points.push_back(point);
   }
}
