\select@language {czech}
\select@language {czech}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{viii}{section*.1}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.5}
\contentsline {section}{C\IeC {\'\i }le pr\IeC {\'a}ce}{1}{section*.6}
\contentsline {section}{Struktura pr\IeC {\'a}ce}{2}{section*.7}
\contentsline {chapter}{\chapternumberline {1}Teorie}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Z\IeC {\'a}kladn\IeC {\'\i } pojmy}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Konvexn\IeC {\'\i } ob\IeC {\'a}lka}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Doln\IeC {\'\i } hranice slo\IeC {\v z}itosti}{9}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Softwarov\IeC {\'e} aspekty}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Pou\IeC {\v z}it\IeC {\'e} technologie}{11}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Programovac\IeC {\'\i } jazyk}{11}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Pou\IeC {\v z}it\IeC {\'e} knihovny}{11}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}OpenMP}{12}{subsection.2.1.3}
\contentsline {paragraph}{Mo\IeC {\v z}nosti OpenMP}{12}{section*.13}
\contentsline {section}{\numberline {2.2}Existuj\IeC {\'\i }c\IeC {\'\i } \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}Datov\IeC {\'e} struktury}{13}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Reprezentace mnohost\IeC {\v e}nu}{13}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Half-edge struktura}{13}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}Dal\IeC {\v s}\IeC {\'\i } mo\IeC {\v z}n\IeC {\'e} struktury}{14}{subsubsection.2.3.1.2}
\contentsline {chapter}{\chapternumberline {3}Algoritmy pro nalezen\IeC {\'\i } konvexn\IeC {\'\i } ob\IeC {\'a}lky ve 3D}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Jarvis March}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Popis algoritmu}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Slo\IeC {\v z}itost algoritmu}{17}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Algoritmus Divide and Conquer}{17}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Popis algoritmu}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Detekce viditeln\IeC {\'y}ch st\IeC {\v e}n}{19}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{20}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Popis algoritmu}{20}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Anal\IeC {\'y}za slo\IeC {\v z}itosti}{23}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}QuickHull}{23}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Popis algoritmu}{23}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Anal\IeC {\'y}za slo\IeC {\v z}itosti}{27}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Brute force}{27}{section.3.5}
\contentsline {section}{\numberline {3.6}Dal\IeC {\v s}\IeC {\'\i } algoritmy}{27}{section.3.6}
\contentsline {chapter}{\chapternumberline {4}Optimalizace}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Obecn\IeC {\'e} optimalizace}{29}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Kompil\IeC {\'a}torov\IeC {\'e} optimalizace}{29}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Jarvis March}{30}{section.4.2}
\contentsline {section}{\numberline {4.3}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{31}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Slo\IeC {\v z}itost algoritmu}{33}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}Fat Planes}{33}{section.4.4}
\contentsline {section}{\numberline {4.5}Paralelizace}{34}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Jarvis march}{34}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Algoritmus Divide and Conquer}{35}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Inkrement\IeC {\'a}ln\IeC {\'\i } algoritmus}{36}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}QuickHull}{36}{subsection.4.5.4}
\contentsline {chapter}{\chapternumberline {5}V\IeC {\'y}sledky}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Testovac\IeC {\'\i } data}{37}{section.5.1}
\contentsline {section}{\numberline {5.2}Celkov\IeC {\'e} srovn\IeC {\'a}n\IeC {\'\i }}{38}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Testovan\IeC {\'e} algoritmy}{38}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Testovac\IeC {\'\i } data \IeC {\v c}.1}{39}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Testovac\IeC {\'\i } data \IeC {\v c}.2}{39}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Testovac\IeC {\'\i } data \IeC {\v c}.3}{39}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}Testovac\IeC {\'\i } data \IeC {\v c}.4}{40}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}Vyhodnocen\IeC {\'\i }}{41}{subsection.5.2.6}
\contentsline {section}{\numberline {5.3}Paralelizace}{43}{section.5.3}
\contentsline {chapter}{Z{\' a}v{\v e}r}{49}{chapter*.47}
\contentsline {chapter}{Literatura}{51}{section*.49}
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{55}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Instala\IeC {\v c}n\IeC {\'\i } p\IeC {\v r}\IeC {\'\i }ru\IeC {\v c}ka}{57}{appendix.B}
\contentsline {section}{\numberline {B.1}Po\IeC {\v z}adavky}{57}{section.B.1}
\contentsline {section}{\numberline {B.2}Instalace a pou\IeC {\v z}it\IeC {\'\i }}{57}{section.B.2}
\contentsline {section}{\numberline {B.3}Form\IeC {\'a}t vstupu a v\IeC {\'y}stupu}{58}{section.B.3}
\contentsline {appendix}{\chapternumberline {C}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{61}{appendix.C}
