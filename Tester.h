#include <vector>
#include <string>
#include <iostream>
#include "geometry.h"
#include "structures.h"

#ifndef TESTER_H
#define	TESTER_H
class Tester{
public:
   void RandomTest(std::vector<std::string> algorithms, int maxNumberOfPoints, int numberOfTests);
   void RandomTest(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests);
   void Test(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests, char type, double distribution,
             unsigned size, unsigned numThreads);
   void TimeTest(std::vector<std::string> algorithms, long minNumberOfPoints, long maxNumberOfPoints, int numberOfTests, char type, double distribution,
             unsigned size, unsigned numThreads);
   void ThreadTest(std::vector<std::string> algorithms, int minThr, int maxThr, int numberOfPoints, char type, double distribution,
             unsigned size);
   void TestGraham();
private:
   Polyhedron parseQHullOutput();
   void parseInput();
   data_t m_points;

};


#endif	/* TESTER_H */

