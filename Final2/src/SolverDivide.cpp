#include "SolverDivide.h"
#include "SolverIncremental.h"
#include "SolverQuickHull.h"
#include <algorithm>

Polyhedron& SolverDivide::Solve(data_t& input, Polyhedron& output) {
   if (input.size() < 500) {
      SolverQuickHull s;
      data_t points;
      for (int i = 0; i < input.size(); i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }
   data_t sortedPoints = input;
   //sort points
   //make hull of left side and right side
   //merge
   std::sort(sortedPoints.begin(), sortedPoints.end(), comparePointsByXCoord);
   int half = sortedPoints.size() / 2;
   Polyhedron left;
   Polyhedron right;
   //If one half is all coplanar, I cannot split, so make hull of all the points
   if (sortedPoints[0][0] == sortedPoints[half][0] || sortedPoints[half][0] == sortedPoints[sortedPoints.size() - 1][0]) {
      SolverQuickHull s;
      s.Solve(sortedPoints, output);
      return output;
   }
   
   Solve(sortedPoints, 0, half, left);
   Solve(sortedPoints, half, input.size(), right);
   edgePoints_t lt = findLowerTangent(left, right);
   mergeTwoHullsOptimized(left, right, lt);
   output = left;
   return output;
}

Polyhedron& SolverDivide::Solve(data_t& input, int begin, int end, Polyhedron& output) {
      if (end - begin < 500) {
      SolverQuickHull s;
      data_t points;
      for (int i = begin; i < end; i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }
   
   int half = (end + begin) / 2;
   Polyhedron left;
   Polyhedron right;
   if (input[begin][0] == input[half][0] || input[half][0] == input[end - 1][0]) {
      SolverQuickHull s;
      data_t points;
      for (int i = begin; i < end; i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }
   Solve(input, begin, half, left);
   Solve(input, half, end, right);
   edgePoints_t lt = findLowerTangent(left, right);
   mergeTwoHullsOptimized(left, right, lt);
   output = left;
   return output;
}

Polyhedron& SolverDivide::SolveParallel(data_t& input, Polyhedron& output, int numThreads) {
   if (numThreads == 1) {
      Solve(input, output);
      return output;
   }
   if (input.size() < 500) {
      SolverQuickHull s;
      data_t points;
      for (int i = 0; i < input.size(); i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }
   omp_set_num_threads(numThreads);
   data_t sortedPoints = input;
   
   std::sort(sortedPoints.begin(), sortedPoints.end(), comparePointsByXCoord);
   int half = sortedPoints.size() / 2;
   Polyhedron left;
   Polyhedron right;

   if (sortedPoints[0][0] == sortedPoints[half][0] || sortedPoints[half][0] == sortedPoints[sortedPoints.size() - 1][0]) {

      SolverQuickHull s;
      s.Solve(sortedPoints, output);
      return output;
   }

#pragma omp parallel
   {
#pragma omp single
      SolveRec(sortedPoints, 0, half, left);
#pragma omp single
      SolveRec(sortedPoints, half, input.size(), right);
   }
   
   edgePoints_t lt = findLowerTangent(left, right);
   mergeTwoHullsOptimized(left, right, lt);
   output = left;
   return output;
}

Polyhedron& SolverDivide::SolveRec(data_t& input, int begin, int end, Polyhedron& output) {
   if (end - begin < 500) {
      SolverQuickHull s;
      data_t points;
      for (int i = begin; i < end; i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }
   int half = (end + begin) / 2;
   Polyhedron left;
   Polyhedron right;

   if (input[begin][0] == input[half][0] || input[half][0] == input[end - 1][0]) {
      SolverQuickHull s;
      data_t points;
      for (int i = begin; i < end; i++) {
         points.push_back(input[i]);
      }
      s.Solve(points, output);
      return output;
   }

#pragma omp task shared (input, left)
   SolveRec(input, begin, half, left);
#pragma omp task shared (input, right)
   SolveRec(input, half, end, right);
#pragma omp taskwait

   edgePoints_t lt = findLowerTangent(left, right);
   mergeTwoHullsOptimized(left, right, lt);
   output = left;
   return output;
}