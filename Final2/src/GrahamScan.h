/* 
 * File:   GrahamScan.h
 * Author: motykvac
 *
 * Created on January 5, 2018, 12:22 PM
 */

#ifndef GRAHAMSCAN_H
#define	GRAHAMSCAN_H
#include "geometry.h"
#include "structures.h"

class GrahamScan2D {
public:

   void solveID(const facet_t& input, std::vector<unsigned>& ids);
private:
   int findMinY(const data_t& points);

   /** Sorts points by polar angle */
   void sortPoints(const data_t& inputData);

   /** Does linear pass through sorted points and finds hull */
   unsigned scan(const data_t& inputData, unsigned * ptStack);

   /** Precomputes polar angles of all points, with respect to minY */
   void computeAngles(const data_t& points);

   struct AngleCmp {

      AngleCmp(const GrahamScan2D& p, const data_t& d)
      : part_(p), data_(d) {
      }
      bool operator()(const unsigned& a, const unsigned& b);
      const GrahamScan2D& part_;
      const data_t& data_;
   };
   /** point indexes sorted by polar angle */
   std::vector<unsigned> order_;
   /** precomputed polar angles */
   std::vector<double> polar_;
   /** pivot point index */
   int pivot_;

};


#endif	/* GRAHAMSCAN_H */

