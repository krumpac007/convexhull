/* 
 * File:   functions.h
 * Author: motykvac
 *
 * Created on February 24, 2017, 4:17 PM
 */

extern bool debug;
extern double EPS_LOC;

#ifndef FUNCTIONS_H
#define	FUNCTIONS_H
//#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "geometry.h"
#include "structures.h"
#include "omp.h"
#include "GrahamScan.h"


inline void printPoint(point_t& p, bool newline = 1) {
   for (unsigned int i = 0; i < p.size(); i++)
      std::cout << p[i] << " ";
   if (newline)
      std::cout << std::endl;
}

inline void printFacet(facet_t& f) {
   for (unsigned int i = 0; i < f.size(); i++) {
      printPoint(f[i]);
   }
}
inline bool comparePointsByXCoord(const point_t& p1, const point_t& p2) {
   if (p1[0] < p2[0]) return 1;
   return 0;
}

bool checkPointsAreTheSame(point_t& first, point_t& second);

bool checkFacetsAreTheSame(facet_t& first, facet_t& second, std::string type = "CCW");

void printAllPoints(std::vector<point_t>& v);

//prints input points in geomview
void printGeomView(data_t data);

void printOpenScad(Polyhedron& p);

void printOpenScadEachFacet(Polyhedron& p);

void printOpenScadEachFacetVect(std::vector<facet_t>& fs);

void printJava(data_t data);

bool comparePolyhedronsFast(Polyhedron& p1, Polyhedron& p2);

bool comparePolyhedrons(Polyhedron& p1, Polyhedron& p2, std::string type = "CCW");

point_t getAveragePoint(std::vector<point_t>& allVertices);

point_t getAveragePoint(Polyhedron& p);

bool isCoplanar(facet_t& f, point_t & point);

bool areCoplanar(facet_t& f1, facet_t& f2);
bool pointCanSeeFacet(point_t& point, facet_t & f);

bool isCollinear(point_t& p, edgePoints_t & e);

void makeFaceCCW(facet_t& facet, point_t & outside);

bool isLowerTangent(facet_t& facet, edgePoints_t& edge, int edgePointIdx);

edgePoints_t findLowerTangent(Polyhedron& p1, Polyhedron& p2);

void discardHiddenFaces(Polyhedron& polyhedron, std::set<point_t>& horizon);

void discardHiddenFaces(Polyhedron& polyhedron, std::set<point_t>& horizon, std::set<point_t>& myHorizon, bool left) ;

void removeCoplanarFaces(Polyhedron& p, std::vector<facet_t>& newFaces);

void removeDuplicatePoints(data_t & data);

void mergeTwoHullsOptimized(Polyhedron& p1, Polyhedron& p2, edgePoints_t& lowerTangent);

Polyhedron& recursiveMerge(std::vector<Polyhedron>& partialResults, int start, int end);

#endif	/* FUNCTIONS_H */

