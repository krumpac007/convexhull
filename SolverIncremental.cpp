#include "SolverIncremental.h"
#include "functions.h"
#include <iostream>
#include <algorithm>
#include <omp.h>

Polyhedron& SolverIncremental::Solve(data_t& input, Polyhedron& output) {
   //find initial tetrahedron
   std::set<unsigned> initialTetrahedronIdxs;
   findInitialTetrahedron(input, output, initialTetrahedronIdxs);

   if (debug) {
      std::cout << "Found initial tetrahedron with vertices: " << std::endl;
      for (auto it = initialTetrahedronIdxs.begin(); it != initialTetrahedronIdxs.end(); it++) {
         printPoint(input[*it]);
      }
   }

   bool addedPointIsCoplanar = 0;
   std::vector<unsigned> addedPointIsCoplanarWithFaces; //idx of facet added point is coplanar with
   //for each point compute if it lies inside or not
   for (unsigned int i = 0; i < input.size(); i++) {
      //initial tetrahedron point
      if (initialTetrahedronIdxs.find(i) != initialTetrahedronIdxs.end())
         continue;

      point_t p = input[i];
      addedPointIsCoplanar = 0;
      addedPointIsCoplanarWithFaces.clear();

      std::set<unsigned> visibleFacesIdxs;
      visibleFacesIdxs.clear(); //not necessary probably

      if (debug) {
         std::cout << "Adding point ";
         printPoint(p);
      }
      //point is inside, can discard it
      if (pointIsInsideHull(p, output, visibleFacesIdxs, addedPointIsCoplanar, addedPointIsCoplanarWithFaces)) {
         if (debug)
            std::cout << "Point is inside." << std::endl;
         continue;
      } else {
         //find horizon - horizon edge will be in visible faces only once
         std::vector<edgePoints_t> horizon;

         findHorizon(visibleFacesIdxs, output, horizon);

         if (debug) {
            std::cout << "Point ";
            printPoint(p);
            std::cout << " has horizon: " << std::endl;
            printHorizon(horizon);
         }
         /***DEAL WITH COPLANAR FACES***/
         if (addedPointIsCoplanar) {
            dealWithCoplanarFaces(output, p, visibleFacesIdxs, horizon, addedPointIsCoplanarWithFaces);
         }


         for (auto it = visibleFacesIdxs.rbegin(); it != visibleFacesIdxs.rend(); it++) {
            output.removeFacet(*it);
         }

         //for each horizon edge make a new facet
         //only for edges not on coplanar face
         for (unsigned int j = 0; j < horizon.size(); j++) {
            //horizon points should be ordered CCW, if not, makeFaceCCW procedure would be necesassary                       
            point_t p1 = horizon[j].first;
            point_t p2 = horizon[j].second;
            facet_t newFacet = {p1, p2, p};
            output.addFacet(newFacet);
         }
      }
   }
   return output;
}

void SolverIncremental::dealWithCoplanarFaces(Polyhedron& output, point_t& p, std::set<unsigned>& visibleFacesIdxs,
        std::vector<edgePoints_t>& horizon, std::vector<unsigned>& addedPointIsCoplanarWithFaces) {
   int size = addedPointIsCoplanarWithFaces.size();
   for (int i = 0; i < size; i++)
      dealWithOneCoplanarFace(output, p, visibleFacesIdxs, horizon, addedPointIsCoplanarWithFaces);
}

void SolverIncremental::dealWithOneCoplanarFace(Polyhedron& output, point_t& p, std::set<unsigned>& visibleFacesIdxs,
        std::vector<edgePoints_t>& horizon, std::vector<unsigned>& addedPointIsCoplanarWithFaces) {

   facet_t coplanarFacet;
   coplanarFacet = output.getFace(addedPointIsCoplanarWithFaces[addedPointIsCoplanarWithFaces.size() - 1]);
   visibleFacesIdxs.insert(addedPointIsCoplanarWithFaces[addedPointIsCoplanarWithFaces.size() - 1]); //I want the coplanar face to be removed too, but I cant add it to visible, because
   addedPointIsCoplanarWithFaces.pop_back(); //it would affect finding horizon

   //determine which horizon edges lie on the coplanar face
   std::vector<edgePoints_t> edgesToRemove;
   for (unsigned int k = 0; k < horizon.size(); k++) {
      edgePoints_t edge = horizon[k];

      if (edgeIsOnFacet(coplanarFacet, edge)) {
         edgesToRemove.push_back(edge);
         horizon.erase(horizon.begin() + k);
         k--;
      }
   }

   //change the orientation for all the edges i am testing on the coplanar facet
   //REASON - I found these edges on the facets neighbouring with the coplanar one, so on the coplanar, they are reversed
   for (unsigned int q = 0; q < edgesToRemove.size(); q++) {
      point_t tmp = edgesToRemove[q].first;
      edgesToRemove[q].first = edgesToRemove[q].second;
      edgesToRemove[q].second = tmp;

   }

   //now find two border points, from these i will make new edges.. 
   point_t borderPoint1;
   unsigned int idx1 = 0;
   point_t borderPoint2;
   unsigned int idx2 = 0;
   findBorderPoints(borderPoint1, borderPoint2, edgesToRemove);

   for (unsigned int o = 0; o < coplanarFacet.size(); o++) {
      if (coplanarFacet[o] == borderPoint1)
         idx1 = o;
      if (coplanarFacet[o] == borderPoint2)
         idx2 = o;
   }

   facet_t newFacet;
   //delete all points between border points and put the new point instead
   if (edgesToRemove.size() == 1) {
      //put the point in between
      newFacet = coplanarFacet;
      if (idx1 < idx2) {
         newFacet.insert(newFacet.begin() + idx2, p); // insert before point on idx2
      } else {
         newFacet.push_back(p);
      }
   } else {
      if (idx1 < idx2) {
         //put the new point and points lying in between idx2 and idx1
         newFacet.push_back(p);
         for (unsigned int o = idx2; o < coplanarFacet.size(); o++) {
            newFacet.push_back(coplanarFacet[o]);
         }
         for (unsigned int o = 0; o <= idx1; o++) {
            newFacet.push_back(coplanarFacet[o]);
         }
      } else {
         //need to delete from idx2 to the end end from the beginning to idx1
         newFacet.push_back(p);
         for (unsigned int o = idx2; o <= idx1; o++)
            newFacet.push_back(coplanarFacet[o]);
      }
   }
   output.addFacet(newFacet);
}

void SolverIncremental::findBorderPoints(point_t& borderPoint1, point_t& borderPoint2, const std::vector<edgePoints_t>& edges) {
   if (edges.size() == 1) {
      borderPoint1 = edges[0].first;
      borderPoint2 = edges[0].second;
      return;
   }
   //bool first = 1; //found first or second point

   for (unsigned int i = 0; i < edges.size(); i++) {
      point_t point1 = edges[i].first;
      bool point1Found = 0;
      point_t point2 = edges[i].second;
      bool point2Found = 0;
      //search for point1
      for (unsigned int j = 0; j < edges.size(); j++) {
         if (j == i) continue;
         edgePoints_t tmpEdge = edges[j];
         if (tmpEdge.first == point1 || tmpEdge.second == point1) {
            point1Found = 1;
         }
         if (tmpEdge.first == point2 || tmpEdge.second == point2) {
            point2Found = 1;
         }
      }
      //point1 is starting point in CCW ordering
      if (point1Found == 0) {
         borderPoint1 = point1;
      }

      //point2 is ending point in CCW ordering
      if (point2Found == 0) {
         borderPoint2 = point2;

      }
   }
}

void SolverIncremental::findHorizon(std::set<unsigned>& visibleFacesIdxs, Polyhedron& output, std::vector<edgePoints_t>& horizon) {
   //if only one face is visible, all its edges will be horizon
   if (visibleFacesIdxs.size() == 1) {
      facet_t visibleFacet = output.getFace(*visibleFacesIdxs.begin());
      for (unsigned int p = 0; p < visibleFacet.size(); p++) {
         edgePoints_t e = {visibleFacet[p], visibleFacet[((p + 1) % visibleFacet.size())]};
         horizon.push_back(e);
      }
   } else {
      //for each face
      for (unsigned int f : visibleFacesIdxs) {
         facet_t visibleFacet = output.getFace(f);
         //for face's each edge
         for (unsigned int j = 0; j < visibleFacet.size(); j++) {
            edgePoints_t edge = {visibleFacet[j], visibleFacet[((j + 1) % visibleFacet.size())]};
            bool edgeFoundOnSomeFace = 0;
            for (unsigned int o : visibleFacesIdxs) {
               if (o == f) continue; //dont test the facet I am working with
               facet_t testedFacet = output.getFace(o);
               //edge is not on some of the other visible faces, meaning this edge is on the horizon
               if (edgeIsOnFacet(testedFacet, edge)) {
                  edgeFoundOnSomeFace = 1;
               }
            }
            if (edgeFoundOnSomeFace == 0)
               horizon.push_back(edge);
         }
      }
   }

}

void SolverIncremental::printHorizon(std::vector<edgePoints_t>& horizon) {

   for (unsigned int i = 0; i < horizon.size(); i++) {
      edgePoints_t e = horizon[i];
      std::cout << "[" << e.first[0] << ", " << e.first[1] << ", " << e.first[2] << "] - [" << e.second[0] << "," << e.second[1] << ","
              << e.second[2] << "]; " << std::endl;
   }
   std::cout << std::endl;

}

bool SolverIncremental::edgeIsOnFacet(facet_t& f, edgePoints_t & e) {
   for (unsigned int j = 0; j < f.size(); j++) {
      edgePoints_t testedEdge = {f[j], f[(j + 1) % f.size()]};
      if (testedEdge.first == e.first && testedEdge.second == e.second)
         return 1;
      if (testedEdge.first == e.second && testedEdge.second == e.first)
         return 1;
   }
   return 0;
}

bool SolverIncremental::pointIsInsideHull(point_t& point, Polyhedron& output, std::set<unsigned>& visibleFacesIdxs, bool& addedPointIsCoplanar,
        std::vector<unsigned>& addedPointIsCoplanarWithFaces) {
   for (unsigned int i = 0; i < output.m_faces.size(); i++) {
      facet_t facet = output.m_faces[i];
      point_t v12 = {facet[1][0] - facet[0][0], facet[1][1] - facet[0][1], facet[1][2] - facet[0][2]};
      point_t v13 = {facet[2][0] - facet[0][0], facet[2][1] - facet[0][1], facet[2][2] - facet[0][2]};
      point_t vperp = perpend3d(v12, v13);

      point_t v14 = {point[0] - facet[0][0], point[1] - facet[0][1], point[2] - facet[0][2]};
      double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);
      if (angleCos < -EPS_LOC) {//point is inside
         continue;
      } else if (fabs(angleCos) < EPS_LOC) {
         if (debug) {
            std::cout << "found coplanar point ";
            printPoint(point);
            std::cout << " with face ";
            printFacet(facet);
            std::cout << " during addition " << std::endl;
            std::cout << " angleCos " << angleCos << std::endl;
         }
         addedPointIsCoplanar = 1;
         addedPointIsCoplanarWithFaces.push_back(i);
         //if all other faces are found as invisible, the point lies inside one of the facets and i can discard it ?
      } else {
         visibleFacesIdxs.insert(i);
      }

   }
   //if point is coplanar with some face and lies OUTSIDE of the hull, we will delete the coplanar face also and later replace it with a new bigger one

   if (visibleFacesIdxs.size() != 0)
      return 0;

   if (debug) {
      std::cout << "Point ";
      printPoint(point);
      std::cout << "is inside hull" << std::endl;
   }
   return 1;
}


//find 4 non coplanar points and make a tetrahedron

Polyhedron & SolverIncremental::findInitialTetrahedron(const data_t& input, Polyhedron & output, std::set<unsigned>& initialTetrahedronIdxs) {
   //it would be possible to use the method from QuickHull here
   std::set<unsigned> initial;
   unsigned a = findInitialPoint(input, DOWN);
   unsigned b = findInitialPoint(input, TOP);
   initial.insert(a);
   initial.insert(b);

   if (a == b) {
      std::cout << "All points on a plane" << std::endl;
      throw ("coplanar");
   }
   unsigned c = findInitialPoint(input, LEFT);
   unsigned d = findInitialPoint(input, RIGHT);
   //cannot insert point collinear with the first 2!
   initial.insert(c);

   if (initial.size() == 2) {
      //find some non collinear point
      point_t v1 = {input[*(++initial.begin())][0] - input[*initial.begin()][0], input[*(++initial.begin())][1] - input[*initial.begin()][1], input[*(++initial.begin())][2] - input[*initial.begin()][2]};
      for (unsigned int i = 0; i < input.size(); i++) {
         point_t v2 = {input[i][0] - input[*initial.begin()][0], input[i][1] - input[*initial.begin()][1], input[i][2] - input[*initial.begin()][2]};
         point_t vperp = perpend3d(v1, v2);
         if (!zeroVect(vperp)) {
            //thord point found
            initial.insert(i);
            break;
         }
      }
   } else {//check if not collinear
      point_t p1 = input[*initial.begin()];
      point_t p2 = input[*(++initial.begin())];
      point_t p3 = input[*(++(++initial.begin()))];

      point_t v12 = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
      point_t v13 = {p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2]};
      point_t vperp = perpend3d(v12, v13);
      if (zeroVect(vperp)) {
         initial.erase(c);
         for (unsigned int i = 0; i < input.size(); i++) {
            v13 = {input[i][0] - p1[0], input[i][1] - p1[1], input[i][2] - p1[2]};
            vperp = perpend3d(v12, v13);
            if (!zeroVect(vperp)) {
               initial.insert(i);
               break;
            }
         }
      }
   }

   //cannot insert in all 4 points on a plane
   initial.insert(d);

   //if 4 points in set i must check if they are not coplanar
   if (initial.size() == 4) {
      point_t p1 = input[*initial.begin()];
      point_t p2 = input[*(++initial.begin())];
      point_t p3 = input[*(++(++initial.begin()))];
      point_t p4 = input[d];
      point_t v12 = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
      point_t v13 = {p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2]};
      point_t v14 = {p4[0] - p1[0], p4[1] - p1[1], p4[2] - p1[2]};
      point_t vperp = perpend3d(v12, v13);
      double angleCos = dot(v14, vperp);
      if (fabs(angleCos) < EPS_LOC) {
         //all 4 points coplanar
         initial.erase(d);
         for (unsigned int i = 0; i < input.size(); i++) {
            p4 = input[i];
            v14 = {p4[0] - p1[0], p4[1] - p1[1], p4[2] - p1[2]};
            angleCos = dot(v14, vperp);
            if (fabs(angleCos) > EPS_LOC) {
               initial.insert(i);
               break;
            }
         }
      }
   }

   if (initial.size() == 4) {
      makeTetrahedron(input, output, initial);
   } else {
      //try adding next point
      unsigned e = findInitialPoint(input, FRONT);
      initial.insert(e);

      if (initial.size() == 4) {
         //make a tetrah
         makeTetrahedron(input, output, initial);
      } else {
         unsigned f = findInitialPoint(input, BACK);
         initial.insert(f);
         if (initial.size() == 4) {
            //make tetrah.           
            makeTetrahedron(input, output, initial);
         } else {
            //find some noncoplanar points and make tetrah.
            //2 more points
            if (initial.size() == 2) {
               //find some non collinear point
               point_t v1 = {input[*(++initial.begin())][0] - input[*initial.begin()][0], input[*(++initial.begin())][1] - input[*initial.begin()][1], input[*(++initial.begin())][2] - input[*initial.begin()][2]};
               for (unsigned int i = 0; i < input.size(); i++) {
                  point_t v2 = {input[i][0] - input[*initial.begin()][0], input[i][1] - input[*initial.begin()][1], input[i][2] - input[*initial.begin()][2]};
                  point_t vperp = perpend3d(v1, v2);
                  if (!zeroVect(vperp)) {
                     //thord point found
                     initial.insert(i);
                     break;
                  }
               }
            }
            //find 4th point
            point_t p1 = input[*initial.begin()];
            point_t p2 = input[*(++initial.begin())];
            point_t p3 = input[*(++(++initial.begin()))];
            point_t v12 = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
            point_t v13 = {p3[0] - p1[0], p3[1] - p1[1], p3[2] - p1[2]};
            point_t vperp = perpend3d(v12, v13);
            for (unsigned int i = 0; i < input.size(); i++) {
               point_t p4 = input[i];
               point_t v14 = {p4[0] - p1[0], p4[1] - p1[1], p4[2] - p1[2]};
               double angleCos = dot(v14, vperp);
               if (fabs(angleCos) > EPS_LOC) {
                  initial.insert(i);
                  break;
               }
            }
            makeTetrahedron(input, output, initial);
         }
      }
   }
   initialTetrahedronIdxs = initial;
   return output;
}

Polyhedron & SolverIncremental::makeTetrahedron(const data_t& input, Polyhedron& output, const std::set<unsigned>& pointIdxs) {
   if (pointIdxs.size() != 4) throw ("wrongsize");
   facet_t f1, f2, f3, f4;
   point_t p1, p2, p3, p4;

   //I cannot access set by index
   auto it = pointIdxs.begin();
   p1 = input[*it];
   it++;
   p2 = input[*it];
   it++;
   p3 = input[*it];
   it++;
   p4 = input[*it];
   f1 = {p1, p2, p3};
   f2 = {p1, p3, p4};
   f3 = {p2, p3, p4};
   f4 = {p1, p2, p4};


   makeFaceCCW(f1, p4);
   makeFaceCCW(f2, p2);
   makeFaceCCW(f3, p1);
   makeFaceCCW(f4, p3);
   output.addFacet(f1);
   output.addFacet(f2);
   output.addFacet(f3);
   output.addFacet(f4);
   return output;
}

//finds index of a point with maximum coordinate in one direction
unsigned SolverIncremental::findInitialPoint(const data_t& input, const Direction direction) {
   unsigned far; //the furthest point    
   double maxd;

   // find farthest point in direction
   far = 0;
   switch (direction) {
      case DOWN:
         maxd = input[0][1];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][1] < maxd) {
               maxd = input[i][1];
               far = i;
            }
         };
         break;
      case TOP:
         maxd = input[0][1];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][1] > maxd) {
               maxd = input[i][1];
               far = i;
            }
         };
         break;
      case LEFT:
         maxd = input[0][0];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][0] < maxd) {
               maxd = input[i][0];
               far = i;
            }
         };
         break;
      case RIGHT:
         maxd = input[0][0];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][0] > maxd) {
               maxd = input[i][0];
               far = i;
            }
         };
         break;
      case FRONT:
         maxd = input[0][2];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][2] < maxd) {
               maxd = input[i][2];
               far = i;
            }
         };
         break;
      case BACK:
         maxd = input[0][2];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][2] > maxd) {
               maxd = input[i][2];
               far = i;
            }
         };
         break;
   }
   return far;
}

//builds a tetrahedron from first 4 points on input - just for testing purpose

Polyhedron & SolverIncremental::findBasicTetrahedron(const data_t& input, Polyhedron & output, std::set<unsigned>& initialTetrahedronIdxs) {
   point_t a = input[0];
   point_t b = input[1];
   point_t c = input[2];
   point_t d = input[3];

   facet_t f1 = {a, b, c};
   facet_t f2 = {a, b, d};
   facet_t f3 = {d, b, c};
   facet_t f4 = {a, c, d};

   makeFaceCCW(f1, d);
   makeFaceCCW(f2, c);
   makeFaceCCW(f3, a);
   makeFaceCCW(f4, b);

   output.addFacet(f1);
   output.addFacet(f2);
   output.addFacet(f3);
   output.addFacet(f4);
   initialTetrahedronIdxs.insert(0);
   initialTetrahedronIdxs.insert(1);
   initialTetrahedronIdxs.insert(2);
   initialTetrahedronIdxs.insert(3);
   return output;
}

Polyhedron & SolverIncremental::SolveParallel(data_t& input, Polyhedron& output, int numThreads) {
   if (numThreads == 1) {
      Solve(input, output);
      return output;
   }
   //sort points by x coordinate

   if (input.size() < 100) {
      Solve(input, output);
      return output;
   }

   omp_set_num_threads(numThreads);
   data_t sortedPoints = input;
   std::sort(sortedPoints.begin(), sortedPoints.end(), comparePointsByXCoord);

   //divide into sections   
   std::vector<Polyhedron> partialResults;
   unsigned int newRatio = sortedPoints.size() / numThreads;
   std::vector<data_t> partialInputs;
   Polyhedron p;

   unsigned int oldRatio = 0;
   unsigned int ratio = newRatio;
   while (newRatio < sortedPoints.size()) {
      data_t points;
      partialResults.push_back(p);
      for (unsigned int i = oldRatio; i < newRatio; i++) {
         points.push_back(sortedPoints[i]);
      }
      partialInputs.push_back(points);
      oldRatio = newRatio;
      newRatio += ratio;
   }
   //last section
   //last section -- put the points into last partial input
   data_t points;
   for (unsigned int i = oldRatio; i < sortedPoints.size(); i++) {
      points.push_back(sortedPoints[i]);
   }
   if (points.size() < 4) {
      for (point_t pt : points)
         partialInputs[partialInputs.size() - 1].push_back(pt);
   } else {
      partialInputs.push_back(points);
      partialResults.push_back(p);
   }

   //solve each section by one thread

#pragma omp parallel for schedule(static)
   for (unsigned i = 0; i < partialInputs.size(); i++) {
      Solve(partialInputs[i], partialResults[i]);
      //printOpenScadEachFacet(partialResults[i]);
   }
   std::vector<Polyhedron> partialResultsCopy = partialResults;

   //merge
   /*for (int i = 0; i < partialResults.size() - 1; i++) {
      edgePoints_t lowerTangent = findLowerTangent(partialResults[0], partialResults[i + 1]);
      //std::cout << "lowerTang: "; printPoint(lowerTangent.first); std::cout << " - ";  printPoint(lowerTangent.second);
      //std::cout << "merging on thr " << omp_get_thread_num();
      mergeTwoHullsOptimized(partialResults[0], partialResults[i + 1], lowerTangent);
     
   }*/
#pragma omp parallel
#pragma omp single
   recursiveMerge(partialResultsCopy, 0, partialResults.size() - 1);

   output = partialResultsCopy[0];
   return output;
}