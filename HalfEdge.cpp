
#include <stdlib.h>
#include <iostream>
#include <string>
#include "HalfEdge.h"

void QFace::computeCentroid() {
   centroid = {0, 0, 0};
   QHalfEdge* he = he0;
   do {
      centroid[0] += he->vertex->pnt[0];
      centroid[1] += he->vertex->pnt[1];
      centroid[2] += he->vertex->pnt[2];
      he = he->next;
   } while (he != he0);
   centroid[0] /= (double) numVerts;
   centroid[1] /= (double) numVerts;
   centroid[2] /= (double) numVerts;
}

void QFace::computeNormal() {
   QHalfEdge* he1 = he0->next;
   QHalfEdge* he2 = he1->next;

   point_t p0 = he0->vertex->pnt;
   point_t p2 = he1->vertex->pnt;

   point_t d2 = {p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2]};

   normal = {0, 0, 0};
   numVerts = 2;

   while (he2 != he0) {
      point_t d1 = d2;
      p2 = he2->vertex->pnt;
      d2 = {p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2]};
      normal[0] += d1[1] * d2[2] - d1[2] * d2[1];
      normal[1] += d1[2] * d2[0] - d1[0] * d2[2];
      normal[2] += d1[0] * d2[1] - d1[1] * d2[0];

      he1 = he2;
      he2 = he2->next;
      numVerts++;
   }
   //normalize the vector to have len 1
   area = vectLen3d(normal);
   normal[0] /= area;
   normal[1] /= area;
   normal[2] /= area;
}

void QFace::computeNormalAndCentroid() {
   computeNormal();
   computeCentroid();
   planeOffset = dot(normal, centroid);
   //test
   int numv = 0;
   QHalfEdge* he = he0;
   do {
      numv++;
      he = he->next;
   } while (he != he0);
   if (numv != numVerts) {
      std::cout << "Face numverts = " << numVerts << " should be " << numv;
      exit(1);

   }
}

QFace* QFace::createTriangle(QVertex* v0, QVertex* v1, QVertex* v2) {
   QFace* face = new QFace();
   //he only needs head vertex
   QHalfEdge* he0 = new QHalfEdge(v0, face);
   QHalfEdge* he1 = new QHalfEdge(v1, face);
   QHalfEdge* he2 = new QHalfEdge(v2, face);

   he0->prev = he2;
   he0->next = he1;

   he1->prev = he0;
   he1->next = he2;

   he2->prev = he1;
   he2->next = he0;

   face->he0 = he0;
   face->computeNormalAndCentroid();
   return face;

}

QHalfEdge* QFace::getEdge(int i) {
   QHalfEdge* he = he0;
   while (i > 0) {
      he = he->next;
      i--;
   }
   while (i < 0) {
      he = he->prev;
      i++;
   }
   return he;
}

QHalfEdge* QFace::findEdge(QVertex* vtail, QVertex* vhead) {
   QHalfEdge* he = he0;
   do {
      if (he->head() == vhead && he->tail() == vtail)
         return he;
      he = he->next;
   } while (he != he0);
   return NULL;
}

//possible segfault 

void QFace::getVertexIndexes(int idxs[]) {
   QHalfEdge* he = he0;
   int i = 0;
   do {
      idxs[i++] = he->head()->index;
      he = he->next;
   } while (he != he0);
}

double QFace::distanceToPlane(QVertex* v) {
   return dot(normal, v->pnt) - planeOffset;
}


//diff
double QFace::distanceToPlane(point_t& p) {
   return dot(normal, p) - planeOffset;
}
//possible segfault on array ? has only 3 members?
//should be array of pointers?

int QFace::mergeAdjacentFace(QHalfEdge* hedgeAdj, std::vector<QFace*>& discarded) {
   QFace* oppFace = hedgeAdj->oppositeFace();
   int numDiscarded = 0;

   discarded[numDiscarded++] = oppFace;
   oppFace->mark = DELETED;

   QHalfEdge* hedgeOpp = hedgeAdj->opposite;

   QHalfEdge* hedgeAdjPrev = hedgeAdj->prev;
   QHalfEdge* hedgeAdjNext = hedgeAdj->next;
   QHalfEdge* hedgeOppPrev = hedgeOpp->prev;
   QHalfEdge* hedgeOppNext = hedgeOpp->next;

   //first type merge, the faces are next to each other, have the same opposite face
   while (hedgeAdjPrev->oppositeFace() == oppFace) {
      hedgeAdjPrev = hedgeAdjPrev->prev;
      hedgeOppNext = hedgeOppNext->next;
   }
   while (hedgeAdjNext->oppositeFace() == oppFace) {
      hedgeOppPrev = hedgeOppPrev->prev;
      hedgeAdjNext = hedgeAdjNext->next;
   }

   QHalfEdge* hedge;

   //absorbs the opposite face to itself, faces are not only triangles, so for all edges that are added to new(this, bigger) face, upadte its face they belong to         
   for (hedge = hedgeOppNext; hedge != hedgeOppPrev->next; hedge = hedge->next) {
      hedge->face = this;
   }

   //if the initial edge is the first in the face list, we need to choose another one, bcs this hedge will no longer exist
   if (hedgeAdj == he0) {
      he0 = hedgeAdjNext;
   }

   //after this hedgeoppprev will have as its next hedgeAdjNext and it will form a circle
   QFace* discardedFace;

   //handle halfesges at head
   discardedFace = connectHalfEdges(hedgeOppPrev, hedgeAdjNext);
   if (discardedFace != NULL) {
      discarded[numDiscarded++] = discardedFace;
   }

   //at tail
   // handle the half edges at the tail
   discardedFace = connectHalfEdges(hedgeAdjPrev, hedgeOppNext);
   if (discardedFace != NULL) {
      discarded[numDiscarded++] = discardedFace;
   }

   computeNormalAndCentroid();
   //checkConsistency();

   return numDiscarded;
}

QFace* QFace::connectHalfEdges(QHalfEdge* hedgePrev, QHalfEdge* hedge) {
   QFace* discardedFace = NULL;
   if (hedgePrev->oppositeFace() == hedge->oppositeFace()) {
      //there is redundant edge we can get rid of
      QFace* oppFace = hedge->oppositeFace();
      QHalfEdge* hedgeOpp;

      if (hedgePrev == he0) {
         he0 = hedge;
      }

      if (oppFace->numVerts == 3) { // then we can get rid of the opposite face altogether
         hedgeOpp = hedge->opposite->prev->opposite;

         oppFace->mark = DELETED;
         discardedFace = oppFace;
      } else {
         hedgeOpp = hedge->opposite->next;

         if (oppFace->he0 == hedgeOpp->prev) {
            oppFace->he0 = hedgeOpp;
         }
         hedgeOpp->prev = hedgeOpp->prev->prev;
         hedgeOpp->prev->next = hedgeOpp;
      }
      hedge->prev = hedgePrev->prev;
      hedge->prev->next = hedge;
      
      hedge->opposite = hedgeOpp;
      hedgeOpp->opposite = hedge;
      
      oppFace->computeNormalAndCentroid();
   }
   else{
      hedgePrev->next = hedge;
      hedge->prev = hedgePrev;
   }
   return discardedFace;
}

std::string QHalfEdge::getVertexString() {
   std::string s = "";
   if (tail() != NULL) {
      s += std::to_string(tail()->index);
      s += " - ";
      s += std::to_string(head()->index);
      return s;
   } else {
      s += "?-";
      s += std::to_string(head()->index);
      return s;
   }
}

std::string QFace::getVertexString(){
   std::string s="";
   QHalfEdge* he = he0;
   do{
     if(s.empty()){
        s = std::to_string(he->head()->index);
     } 
     else{
        s+= " ";
        s+= std::to_string(he->head()->index);
     }
     he = he->next;
   }
   while(he != he0);
   return s;
}

double QHalfEdge::length() {
   if (tail() != NULL) {
      return dist3d(head()->pnt, tail()->pnt);
   } else
      return -1;   
}

double QHalfEdge::lengthSquared() {
   if (tail() != NULL) {
      return dist3dSquared(head()->pnt, tail()->pnt);
   } else
      return -1;
}
