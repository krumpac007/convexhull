#include <vector>
#include "SolverQuickHull.h"
#include <algorithm>
#include <math.h>

void SolverQuickHull::Solve(data_t& input) {
   int nump = input.size();
   if (nump < 4) {
      std::cerr << "Less than 4 points on input" << std::endl;
      exit(1);
   }
   //std::cout << "Solving on thread " << omp_get_thread_num(); 
   initBuffers(nump);
   setPoints(input, nump);

   buildHull();
}

Polyhedron& SolverQuickHull::Solve(data_t& input, Polyhedron& p) {
   Solve(input);
   storeToPolyhedron(p);
   return p;
}

Polyhedron& SolverQuickHull::SolveParallel(data_t& input, Polyhedron& output, int numThreads) {
   if (numThreads == 1) {
      Solve(input, output);
      return output;
   }
   if (input.size() < 100) {
      Solve(input, output);
      return output;
   }
   omp_set_num_threads(numThreads);
   m_numThreads = numThreads;
   //sort points by x coordinate
   data_t sortedPoints = input;
   std::sort(sortedPoints.begin(), sortedPoints.end(), comparePointsByXCoord);

   //divide into sections
   std::vector<Polyhedron> partialResults;
   unsigned int newRatio = sortedPoints.size() / numThreads;
   std::vector<data_t> partialInputs;
   Polyhedron p;
   unsigned int oldRatio = 0;
   unsigned int ratio = newRatio;

   while (newRatio < sortedPoints.size()) {
      data_t points;
      for (unsigned int i = oldRatio; i < newRatio; i++) {
         points.push_back(sortedPoints[i]);
      }
      partialResults.push_back(p);
      partialInputs.push_back(points);
      oldRatio = newRatio;
      newRatio += ratio;
   }
   //last section -- put the points into last partial input
   data_t points;
   for (unsigned int i = oldRatio; i < sortedPoints.size(); i++) {
      points.push_back(sortedPoints[i]);
   }
   if (points.size() < 4) {
      for (point_t pt : points)
         partialInputs[partialInputs.size() - 1].push_back(pt);
   } else {
      partialInputs.push_back(points);
      partialResults.push_back(p);
   }


#pragma omp parallel for schedule(static)
   for (unsigned int i = 0; i < partialInputs.size(); i++) {
      //std::cout << "input has " << partialInputs[i].size() << '\n';
      SolverQuickHull sq(numThreads);
      sq.Solve(partialInputs[i], partialResults[i]);
   }
   //merge
   std::vector<Polyhedron> partialResultsCopy = partialResults;
   /*double mergeTime = 0;
    ******THIS IS LINEAR MERGE, SLOWER THEN RECURSIVE******
   for (int i = 0; i < partialResults.size() - 1; i++) {
      edgePoints_t lowerTangent = findLowerTangent(partialResults[0], partialResults[i + 1]);
      //std::cout << "lowerTang: "; printPoint(lowerTangent.first); std::cout << " - ";  printPoint(lowerTangent.second);
      
      double mergeTimeA = omp_get_wtime();
      mergeTwoHullsOptimized(partialResults[0], partialResults[i + 1], lowerTangent);
      double mergeTimeB = omp_get_wtime();
      mergeTime += (mergeTimeB - mergeTimeA);
   }
   std::cout <<" merge took " << mergeTime << "\n";
    */
   //double recMergeTimeA = omp_get_wtime();
#pragma omp parallel
#pragma omp single
   recursiveMerge(partialResultsCopy, 0, partialResults.size() - 1);
   //double recMergeTimeB = omp_get_wtime();
   //std::cout <<" Recursive merge took " << recMergeTimeB - recMergeTimeA << "\n";
   //output = partialResults[0];
   output = partialResultsCopy[0];
   return output;
}

void SolverQuickHull::storeToPolyhedron(Polyhedron& p) {
   for (QFace* fp : faces) {
      if (fp->mark == 1) {
         facet_t newFacet;
         QHalfEdge* he = fp->he0;
         do {
            newFacet.push_back(he->head()->pnt);
            he = he->next;
         } while (he != fp->he0);
         p.addFacet(newFacet);
      }
   }
}

void SolverQuickHull::initBuffers(unsigned int nump) {
   ///this is probably useless becasue vector will be copied at the end no matter what.. i could add to the exist. vector right at the beginning

   if (pointBuffer.size() < nump) {
      std::vector<QVertex*> newBuffer(nump);
      vertexPointIndices = std::vector<int>(nump);
      for (unsigned int i = 0; i < pointBuffer.size(); i++) {
         newBuffer[i] = pointBuffer[i];
      }
      for (unsigned int i = pointBuffer.size(); i < nump; i++) {
         newBuffer[i] = new QVertex();
      }
      pointBuffer = newBuffer;
   }

   faces.clear();
   claimed->clear();
   numFaces = 0;
   numPoints = nump;
}

//this will set the points in the pointbuffer, pointers

void SolverQuickHull::setPoints(data_t& points, int nump) {
   for (int i = 0; i < nump; i++) {
      QVertex* vtx = pointBuffer[i];
      vtx->pnt = points[i];
      vtx->index = i;
   }
}

void SolverQuickHull::buildHull() {
   int cnt = 0;
   QVertex* eyeVtx;

   computeMaxAndMin();
   createInitialSimplex();

   while ((eyeVtx = nextPointToAdd()) != NULL) {
      addPointToHull(eyeVtx);
      cnt++;
      if (debug) {
         std::cout << "iteration " << cnt << " done" << std::endl;
      }
   }
   
   for (int i = 0; i < numPoints; i++) {
      pointBuffer[i]->index = -1;
   }
   numFaces = 0;

   //remove inactive faces
   for (unsigned int i = 0; i < faces.size(); i++) {
      QFace * face = faces[i];
      if (face->mark != QFace::VISIBLE) {
         //diff
         faces.erase(faces.begin() + i);
      } else {
         markFaceVertices(face, 0);
         numFaces++;
      }
   }

   //reindex vertices
   numVertices = 0;
   for (int i = 0; i < numPoints; i++) {
      QVertex* vtx = pointBuffer[i];
      if (vtx->index == 0) {
         vertexPointIndices[numVertices] = i;
         vtx->index = numVertices++;
      }
   }
   if (debug) {
      std::cout << "hull done" << std::endl;
   }
}

void SolverQuickHull::computeMaxAndMin() {
   point_t max = std::vector<double>(3), min = std::vector<double>(3);
   for (int i = 0; i < 3; i++) {
      maxVtxs[i] = minVtxs[i] = pointBuffer[0];
   }

   max = pointBuffer[0]->pnt;
   min = pointBuffer[0]->pnt;

   for (int i = 1; i < numPoints; i++) {
      point_t pnt = pointBuffer[i]->pnt;
      if (pnt[0] > max[0]) {
         max[0] = pnt[0];
         maxVtxs[0] = pointBuffer[i];
      } else if (pnt[0] < min[0]) {
         min[0] = pnt[0];
         minVtxs[0] = pointBuffer[i];
      }

      if (pnt[1] > max[1]) {
         max[1] = pnt[1];
         maxVtxs[1] = pointBuffer[i];
      } else if (pnt[1] < min[1]) {
         min[1] = pnt[1];
         minVtxs[1] = pointBuffer[i];
      }

      if (pnt[2] > max[2]) {
         max[2] = pnt[2];
         maxVtxs[2] = pointBuffer[i];
      } else if (pnt[2] < min[2]) {
         min[2] = pnt[2];
         minVtxs[2] = pointBuffer[i];
      }
   }

   //set epsilon tolerance, coming from quickhull paper
   tolerance = 3 * DOUBLE_PREC * (findMax(fabs(max[0]), fabs(min[0])) +
           findMax(fabs(max[1]), fabs(min[1])) +
           findMax(fabs(max[2]), fabs(min[2]))
           );
}

void SolverQuickHull::addPointToFace(QVertex* vtx, QFace* face) {
   vtx->face = face;
   if (face->outside == NULL) {
      claimed->add(vtx);
   } else {
      claimed->insertBefore(vtx, face->outside);
   }
   face->outside = vtx;
}

void SolverQuickHull::createInitialSimplex() {
   double max = 0;
   int imax = 0;

   for (int i = 0; i < 3; i++) {
      double diff = maxVtxs[i]->pnt[i] - minVtxs[i]->pnt[i];
      if (diff > max) {
         max = diff;
         imax = i;
      }
   }
   if (max <= tolerance) {
      std::cerr << "Points are maybe coincident" << std::endl;
      exit(1);
   }

   QVertex * vtx[4];
   //first 2 will be the two with greatest separation
   vtx[0] = maxVtxs[imax];
   vtx[1] = minVtxs[imax];

   //third will be the one furthest from line between the two
   point_t u01 = std::vector<double>(3);
   point_t diff02 = std::vector<double>(3);
   point_t nrml = std::vector<double>(3);
   point_t xprod = std::vector<double>(3);


   double maxSqr = 0;
   u01[0] = vtx[1]->pnt[0] - vtx[0]->pnt[0];
   u01[1] = vtx[1]->pnt[1] - vtx[0]->pnt[1];
   u01[2] = vtx[1]->pnt[2] - vtx[0]->pnt[2];
   normalize(u01);

   for (int i = 0; i < numPoints; i++) {
      diff02[0] = pointBuffer[i]->pnt[0] - vtx[0]->pnt[0];
      diff02[1] = pointBuffer[i]->pnt[1] - vtx[0]->pnt[1];
      diff02[2] = pointBuffer[i]->pnt[2] - vtx[0]->pnt[2];

      xprod = perpend3d(u01, diff02);
      double lenSqr = vectSqr3d(xprod);
      if (lenSqr > maxSqr &&
              pointBuffer[i] != vtx[0] && pointBuffer[i] != vtx[1]) {
         maxSqr = lenSqr;
         vtx[2] = pointBuffer[i];
         nrml[0] = xprod[0];
         nrml[1] = xprod[1];
         nrml[2] = xprod[2];
      }
   }
   if (sqrt(maxSqr) <= 100 * tolerance) {
      std::cerr << "Points may be collinear" << std::endl;
      exit(1);
   }
   normalize(nrml);

   double maxDist = 0;
   double d0 = dot(vtx[2]->pnt, nrml);
   for (int i = 0; i < numPoints; i++) {
      double dist = fabs(dot(pointBuffer[i]->pnt, nrml) - d0);
      if (dist > maxDist && pointBuffer[i] != vtx[0] && pointBuffer[i] != vtx[1] && pointBuffer[i] != vtx[2]) {
         maxDist = dist;
         vtx[3] = pointBuffer[i];
      }
   }
   if (fabs(maxDist <= 100 * tolerance)) {
      std::cerr << "Points may be coplanar" << std::endl;
      exit(1);
   }

   if (debug) {
      std::cout << "init. vertices:" << std::endl;
      std::cout << vtx[0]->index << ": ";
      printVector(vtx[0]->pnt);
      std::cout << vtx[1]->index << ": ";
      printVector(vtx[1]->pnt);
      std::cout << vtx[2]->index << ": ";
      printVector(vtx[2]->pnt);
      std::cout << vtx[3]->index << ": ";
      printVector(vtx[3]->pnt);
   }

   QFace * tris[4];
   //orientation
   if (dot(vtx[3]->pnt, nrml) - d0 < 0) {
      tris[0] = QFace::createTriangle(vtx[0], vtx[1], vtx[2]);
      tris[1] = QFace::createTriangle(vtx[3], vtx[1], vtx[0]);
      tris[2] = QFace::createTriangle(vtx[3], vtx[2], vtx[1]);
      tris[3] = QFace::createTriangle(vtx[3], vtx[0], vtx[2]);

      for (int i = 0; i < 3; i++) {
         int k = (i + 1) % 3;
         tris[i + 1]->getEdge(1)->setOpposite(tris[k + 1]->getEdge(0));
         tris[i + 1]->getEdge(2)->setOpposite(tris[0]->getEdge(k));
      }
   } else {
      tris[0] = QFace::createTriangle(vtx[0], vtx[2], vtx[1]);
      tris[1] = QFace::createTriangle(vtx[3], vtx[0], vtx[1]);
      tris[2] = QFace::createTriangle(vtx[3], vtx[1], vtx[2]);
      tris[3] = QFace::createTriangle(vtx[3], vtx[2], vtx[0]);

      for (int i = 0; i < 3; i++) {
         int k = (i + 1) % 3;
         tris[i + 1]->getEdge(0)->setOpposite(tris[k + 1]->getEdge(1));
         tris[i + 1]->getEdge(2)->setOpposite(tris[0]->getEdge((3 - i) % 3));
      }
   }

   for (int i = 0; i < 4; i++) {
      faces.push_back(tris[i]);
   }

   for (int i = 0; i < numPoints; i++) {
      QVertex* v = pointBuffer[i];

      if (v == vtx[0] || v == vtx[1] || v == vtx[2] || v == vtx[3]) {
         continue;
      }
      maxDist = tolerance;
      QFace* maxFace = NULL;
      for (int k = 0; k < 4; k++) {
         //diff
         double dist = tris[k]->distanceToPlane(v);
         if (dist > maxDist) {
            maxFace = tris[k];
            maxDist = dist;
         }
      }
      if (maxFace != NULL) {
         addPointToFace(v, maxFace);
      }
   }
}

void SolverQuickHull::deleteFacePoints(QFace* face, QFace* absorbingFace) {
   //QVertex* faceVtxs = removeAllPointsFromFace(face);
   QVertex* faceVtxs = new QVertex();
   if (face->outside != NULL) {
      QVertex* end = face->outside;
      while (end->next != NULL && end->next->face == face) {
         end = end->next;
      }
      claimed->deleteVtx(face->outside, end);
      end->next = NULL;
      faceVtxs = face->outside;
   } else
      faceVtxs = NULL;


   if (faceVtxs != NULL) {
      if (absorbingFace == NULL) {
         unclaimed->addAll(faceVtxs);
      } else {
         QVertex* vtxNext = faceVtxs;
         for (QVertex* vtx = vtxNext; vtx != NULL; vtx = vtxNext) {
            vtxNext = vtx->next;
            double dist = absorbingFace->distanceToPlane(vtx);
            if (dist > tolerance) {
               addPointToFace(vtx, absorbingFace);
            } else {
               unclaimed->add(vtx);
            }
         }
      }
   }

}

void SolverQuickHull::resolveUnclaimedPoints(QFaceList* newFaces) {
   QVertex* vtxNext = unclaimed->first();
   for (QVertex* vtx = vtxNext; vtx != NULL; vtx = vtxNext) {
      vtxNext = vtx->next;

      double maxDist = tolerance;
      QFace* maxFace = NULL;
      for (QFace* newFace = newFaces->first(); newFace != NULL; newFace = newFace->next) {
         if (newFace->mark == QFace::VISIBLE) {
            double dist = newFace->distanceToPlane(vtx);
            if (dist > maxDist) {
               maxDist = dist;
               maxFace = newFace;
            }
            if (maxDist > 1000 * tolerance) {
               break;
            }
         }
      }

      if (maxFace != NULL) {
         addPointToFace(vtx, maxFace);
      }
   }
}

void SolverQuickHull::calculateHorizon(point_t eyePnt, QHalfEdge* edge0, QFace* face, std::vector<QHalfEdge*>& horizon) {
   deleteFacePoints(face, NULL);
   face->mark = QFace::DELETED;

   if (debug) {
      std::cout << " visiting face " << face->getVertexString() << std::endl;
   }

   QHalfEdge* edge;
   if (edge0 == NULL) {
      edge0 = face->getEdge(0);
      edge = edge0;
   } else {
      edge = edge0->next;
   }
   do {
      QFace* oppFace = edge->oppositeFace();

      if (oppFace != NULL && oppFace->mark == QFace::VISIBLE) {
         if (oppFace->distanceToPlane(eyePnt) > tolerance) {
            calculateHorizon(eyePnt, edge->opposite, oppFace, horizon);
         } else {
            horizon.push_back(edge);
            if (debug) {
               std::cout << " adding horizon edge " << edge->getVertexString() << std::endl;
            }
         }
      }
      edge = edge->next;
   } while (edge != edge0);
}

bool SolverQuickHull::doAdjacentMerge(QFace* face, int mergeType) {
   QHalfEdge* hedge = face->he0;

   bool convex = true;
   do {
      QFace* oppFace = hedge->oppositeFace();
      bool merge = false;
      double dist1; //dist2;

      if (mergeType == NONCONVEX) { //merge definitely nonconvex
         if (oppFaceDistance(hedge) > -tolerance || oppFaceDistance(hedge->opposite) > -tolerance)
            merge = true;
      } else {//merge if the faces are parallel or nonconv., write to larger face
         if (face->area > oppFace->area) {
            if ((dist1 = oppFaceDistance(hedge)) > -tolerance)
               merge = true;
            else if (oppFaceDistance(hedge->opposite) > -tolerance)
               convex = false;
         } else {
            if (oppFaceDistance(hedge->opposite) > -tolerance)
               merge = true;
            else if (oppFaceDistance(hedge) > -tolerance)
               convex = false;
         }
      }

      if (merge) {
         if (debug) {
            std::cout << " merging " << face->getVertexString() << " and " << oppFace->getVertexString() << std::endl;
         }
         int numd = face->mergeAdjacentFace(hedge, discardedFaces);
         for (int i = 0; i < numd; i++) {
            deleteFacePoints(discardedFaces[i], face);
         }
         if (debug) {
            std::cout << " result: " << face->getVertexString() << std::endl;
         }
         return true;
      }
      hedge = hedge->next;
   } while (hedge != face->he0);
   if (!convex)
      face->mark = QFace::NON_CONVEX;
   return false;
}

void SolverQuickHull::addPointToHull(QVertex* eyeVtx) {
   horizon.clear();
   unclaimed->clear();

   if (debug) {
      std::cout << "Adding point:" << eyeVtx->index << '\n';
      std::cout << "which is " << eyeVtx->face->distanceToPlane(eyeVtx->pnt) << " above face " << eyeVtx->face->getVertexString() << std::endl;
   }

   if (eyeVtx == eyeVtx->face->outside) {
      if (eyeVtx->next != NULL && eyeVtx->next->face == eyeVtx->face) {
         eyeVtx->face->outside = eyeVtx->next;
      } else {
         eyeVtx->face->outside = NULL;
      }
   }
   claimed->deleteVtx(eyeVtx);
   calculateHorizon(eyeVtx->pnt, NULL, eyeVtx->face, horizon);

   newFaces->clear();
   addNewFaces(newFaces, eyeVtx, horizon);

   //merge faces that are non convex determined by larger face (smaller face in bigger one)
   for (QFace* face = newFaces->first(); face != NULL; face = face->next) {
      if (face->mark == QFace::VISIBLE) {
         while (doAdjacentMerge(face, NONCONVEX_WRT_LARGER_FACE));
      }
   }

   for (QFace* face = newFaces->first(); face != NULL; face = face->next) {
      if (face->mark == QFace::NON_CONVEX) {
         face->mark = QFace::VISIBLE;
         while (doAdjacentMerge(face, NONCONVEX));
      }
   }
   resolveUnclaimedPoints(newFaces);
}

//horizon is in ccw order

QHalfEdge* SolverQuickHull::addAdjoiningFace(QVertex* eyeVtx, QHalfEdge* he) {
   QFace* face = QFace::createTriangle(eyeVtx, he->tail(), he->head());
   faces.push_back(face);

   //-1 is the opposite he at the horizon 
   face->getEdge(-1)->setOpposite(he->opposite);
   return face->getEdge(0);
}

void SolverQuickHull::addNewFaces(QFaceList* newFaces, QVertex* eyeVtx, std::vector<QHalfEdge*>& horizon) {
   newFaces->clear();
   QHalfEdge* hedgeSidePrev = NULL;
   QHalfEdge* hedgeSideBegin = NULL;

   //DIFF
   //add face for each horizon edge
   for (unsigned int i = 0; i < horizon.size(); i++) {
      QHalfEdge* horizonHe = horizon[i];
      QHalfEdge* hedgeSide = addAdjoiningFace(eyeVtx, horizonHe); //not the he on horizon, but the next in ccw orded

      if (debug) {
         std::cout << "new face : " << hedgeSide->face->getVertexString() << std::endl;
      }

      if (hedgeSidePrev != NULL) {
         hedgeSide->next->setOpposite(hedgeSidePrev); //set the next face to be next to each other
      } else {
         hedgeSideBegin = hedgeSide;
      }
      newFaces->add(hedgeSide->face);
      hedgeSidePrev = hedgeSide;
   }
   hedgeSideBegin->next->setOpposite(hedgeSidePrev); //last one

}

QVertex* SolverQuickHull::nextPointToAdd() {
   if (!claimed->isEmpty()) {
      QFace* eyeFace = claimed->first()->face;
      QVertex* eyeVtx = NULL;
      double maxDist = 0;
      for (QVertex* vtx = eyeFace->outside; vtx != NULL && vtx->face == eyeFace; vtx = vtx->next) {
         double dist = eyeFace->distanceToPlane(vtx);
         if (dist > maxDist) {
            maxDist = dist;
            eyeVtx = vtx;
         }
      }
      return eyeVtx;
   } else return NULL;
}

void SolverQuickHull::markFaceVertices(QFace* face, int mark) {
   QHalfEdge* he0 = face->getFirstEdge();
   QHalfEdge* he = he0;

   do {
      he->head()->index = mark;
      he = he->next;
   } while (he != he0);
}

void SolverQuickHull::reindexFacesAndVertices() {

}

int** SolverQuickHull::getFaces() {
   int** allFaces = new int*[faces.size()];
   int k = 0;
   for (unsigned int i = 0; i < faces.size(); i++) {
      QFace* face = faces[i];
      allFaces[k] = new int[face->numVerts];
      getFaceIndices(allFaces[k], face);
      k++;
   }

   return allFaces;
}

void SolverQuickHull::getFaceIndices(int* indices, QFace* face) {
   QHalfEdge* hedge = face->he0;
   int k = 0;
   do {
      int idx = hedge->head()->index;
      indices[k++] = idx;
      hedge = hedge->next;
   } while (hedge != face->he0);
}

void SolverQuickHull::Print() {
   int numf = getNumFaces();

   std::cout << numf << " faces" << std::endl;
   for (unsigned int i = 0; i < faces.size(); i++) {
      QFace* f = faces[i];
      if (f->mark == 1) {
         std::cout << f->numVerts << " vertices" << std::endl;
         QHalfEdge* h = f->he0;
         do {
            std::cout << h->vertex->pnt[0] << " " << h->vertex->pnt[1] << " " << h->vertex->pnt[2] << std::endl;
            h = h->next;
         } while (h != f->he0);
      }
   }
}

void SolverQuickHull::PrintVertices() {
   for (int i = 0; i < numVertices; i++) {
      std::cout << pointBuffer[vertexPointIndices[i]]->pnt[0] << " " << pointBuffer[vertexPointIndices[i]]->pnt[1] << " " << pointBuffer[vertexPointIndices[i]]->pnt[2] << std::endl;
   }
}

int SolverQuickHull::getNumFaces() {
   int numf = 0;
   for (unsigned int i = 0; i < faces.size(); i++) {
      QFace* f = faces[i];
      if (f->mark == 1) {
         numf++;
      }
   }
   return numf;
}
