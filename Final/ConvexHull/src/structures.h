/* 
 * File:   structures.h
 * Author: motykvac
 *
 * Created on February 23, 2017, 5:42 PM
 */
#include <vector>
#include <string>

#ifndef STRUCTURES_H
#define	STRUCTURES_H


typedef std::vector<double> point_t;
typedef std::vector<point_t> facet_t;
typedef std::vector<int> facet_idx_t;
typedef std::vector<point_t> data_t;
typedef std::pair<double, double> point2d_t;

//enum algoVariant{JARVIS, JARVISPARA, DC, DCPARA, INCR, INCRPARA};
#define D(X) //std::cout<<"  "<<#X": "<<X<<std::endl;
#define R(X) //std::cout<<"  "<<X<<std::endl;

class Polyhedron {
public:

   void addFacet(facet_t& f) {
      m_faces.push_back(f);
   };
   void addFacetIdx(facet_idx_t& f) {
      m_faces_idx.push_back(f);
   };

   void popFacet() {
      m_faces.pop_back();
   };

   int getSize() {
      return m_faces.size();
   };
   
   void print();
   void printIdx();
   int getNumFaces() {return m_faces.size();};
private:
   std::vector<facet_t> m_faces;
   std::vector<facet_idx_t> m_faces_idx;
   
   
};

class Solver2D {
public:
   facet_t& Solve(data_t& input, facet_t& output);
};

class GrahamScan2D {
public:

   void solveID(const facet_t& input, std::vector<unsigned>& ids);
private:
   int findMinY(const data_t& points);

   /** Sorts points by polar angle */
   void sortPoints(const data_t& inputData);

   /** Does linear pass through sorted points and finds hull */
   unsigned scan(const data_t& inputData, unsigned * ptStack);

   /** Precomputes polar angles of all points, with respect to minY */
   void computeAngles(const data_t& points);

   struct AngleCmp {

      AngleCmp(const GrahamScan2D& p, const data_t& d)
      : part_(p), data_(d) {
      }
      bool operator()(const unsigned& a, const unsigned& b);
      const GrahamScan2D& part_;
      const data_t& data_;
   };
   /** point indexes sorted by polar angle */
   std::vector<unsigned> order_;
   /** precomputed polar angles */
   std::vector<double> polar_;
   /** pivot point index */
   int pivot_;

};

class Solver3D {
public:
   virtual Polyhedron& Solve(data_t& input, Polyhedron& output) = 0;

   std::string getName() {
      return m_name;
   };
protected:
   std::string m_name;
};


#endif	/* STRUCTURES_H */

