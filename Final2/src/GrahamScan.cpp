#include "GrahamScan.h"
#include <algorithm>

/*Solves convex hull for 2d using graham scan, (c) Peter Mitura*/
facet_t& Solver2D::Solve(data_t& inputData, facet_t& output) {
   /*check if every point is 2d*/
   //std::cout<<"graham start"<<std::endl;
   for (unsigned int i = 0; i < inputData.size(); i++) {
      if (inputData[i].size() > 2) {
         std::cerr << "Points are not in 2d" << std::endl;
      }

      if (inputData.size() <= 2) {
         output = inputData;
         return output;
      }

      point_t currPoint;

      unsigned maxIndex = 0, currIndex, nextIndex;

      // find point with max Y (min X in case of tie)
      for (unsigned i = 1; i < inputData.size(); i++) {
         double dif = inputData[i][1] - inputData[maxIndex][1];
         if (fabs(dif) <= EPS) {
            if (inputData[i][0] > inputData[maxIndex][0]) {
               maxIndex = i;
            }
         } else if (dif > EPS) {
            maxIndex = i;
         }
      }


      // find the rest of points
      currIndex = maxIndex;
      do {
         output.push_back(inputData[currIndex]);
         // avoid setting same point as next
         nextIndex = !currIndex;

         // check orientation for all remaining n - 1 points
         for (unsigned i = 0; i < inputData.size(); i++) {
            if (i == currIndex) {
               continue;
            }

            int o = orientation(inputData[currIndex][0],
                    inputData[currIndex][1],
                    inputData[nextIndex][0],
                    inputData[nextIndex][1],
                    inputData[i ][0],
                    inputData[i ][1]);

            if (o == 0) {
               // exclude collinear points
               if (dist(inputData[currIndex][0],
                       inputData[currIndex][1],
                       inputData[i][0],
                       inputData[i][1])
                       >
                       dist(inputData[currIndex][0],
                       inputData[currIndex][1],
                       inputData[nextIndex][0],
                       inputData[nextIndex][1])) {
                  nextIndex = i;
               }
            } else if (o == 2) {
               // point to the left of current hull face
               nextIndex = i;
            }
         }
         currIndex = nextIndex;
      } while (currIndex != maxIndex);

      return output;
   }
   return output;
}

void GrahamScan2D::solveID(const facet_t& input, std::vector<unsigned>& ids) {

   //     std::cout<<"graham start"<<std::endl;

   if (input.size() <= 2) {
      for (unsigned i = 0; i < input.size(); i++) {
         ids.push_back(0);
      }
      return;
   }

   order_.clear();
   polar_.clear();
   ids.clear();
   const data_t& inputData = input;

   for (unsigned i = 0; i < inputData.size(); i++)
      order_.push_back(i);
   pivot_ = findMinY(inputData);
   std::swap(order_.at(0), order_.at(pivot_));

   sortPoints(inputData);

   unsigned * ptStack = new unsigned[inputData.size()];
   unsigned stackSize = scan(inputData, ptStack);
   for (unsigned i = 0; i < stackSize; i++) {
      ids.push_back(ptStack[i]);
   }
   delete[] ptStack;
   //std::cout<<"graham closed"<<std::endl;

}

int GrahamScan2D::findMinY(const data_t& points) {
   int minIndex = 0;

   for (unsigned i = 1; i < points.size(); i++) {
      double delta = points[minIndex][1] - points[i][1];
      if (delta > EPS) {
         minIndex = i;
      } else if (fabs(delta) < EPS) {
         if (points[minIndex][0] < points[i][0]) {
            minIndex = i;
         }
      }
   }

   return minIndex;
}

unsigned GrahamScan2D::scan(const data_t& inputData, unsigned * ptStack) {
   unsigned iPtr = 2, sPtr = 2, iSize = inputData.size();
   ptStack[0] = order_[0];
   ptStack[1] = order_[1];
   while (iPtr < iSize) {
      if (ccw(inputData[ptStack[0]][0],
              inputData[ptStack[0]][1],
              inputData[ptStack[1]][0],
              inputData[ptStack[1]][1],
              inputData[order_[iPtr]][0],
              inputData[order_[iPtr]][1])) {
         break;
      }
      ptStack[1] = order_[iPtr++];
   }

   while (iPtr < iSize) {
      if (ccw(inputData[ptStack[sPtr - 2]][0],
              inputData[ptStack[sPtr - 2]][1],
              inputData[ptStack[sPtr - 1]][0],
              inputData[ptStack[sPtr - 1]][1],
              inputData[order_[iPtr]][0],
              inputData[order_[iPtr]][1])) {
         ptStack[sPtr++] = order_[iPtr++];
      } else {
         sPtr--;
      }
   }

   // handle last point collinear with first
   if (sPtr >= 3) {
      if (ccw(inputData[ptStack[sPtr - 2]][0],
              inputData[ptStack[sPtr - 2]][1],
              inputData[ptStack[sPtr - 1]][0],
              inputData[ptStack[sPtr - 1]][1],
              inputData[order_[0]][0],
              inputData[order_[0]][1]) == 0) {
         sPtr--;
      }
   }

   return sPtr;
}

void GrahamScan2D::sortPoints(const data_t& inputData) {
   computeAngles(inputData);
   std::sort((order_.begin()) + 1, order_.end(),
           AngleCmp(*this, inputData));
}

void GrahamScan2D::computeAngles(const data_t& points) {
   double dx, dy;
   for (unsigned i = 0; i < points.size(); i++) {
      dx = points[i][0] - points[pivot_][0];
      dy = points[i][1] - points[pivot_][1];
      polar_.push_back(-(dx / dy));
   }
}

bool GrahamScan2D::AngleCmp::operator()(const unsigned& a, const unsigned& b) {
   double x = part_.polar_[a] - part_.polar_[b];
   if (fabs(x) < EPS) {
      return dist({data_[part_.pivot_][0], data_[part_.pivot_][1]},
      {
         data_[a][0], data_[a][1]
      })
      < dist({data_[part_.pivot_][0], data_[part_.pivot_][1]},
      {
         data_[b][0], data_[b][1]
      });
   }
   return x > EPS;
}
