#include <vector>

#include "SolverJarvis.h"
#include "functions.h"
#include <climits>
#include <iostream>
#include <set>
#include <cfloat>
#include <ctime>
#include <omp.h>

Polyhedron& SolverJarvis::Solve(data_t& input, Polyhedron& output) {
   // edge cases
   if (input.size() == 0) {
      return output;
   } else if (input.size() < 3) {
      facet_t plane;
      const data_t& inputData = input;
      for (auto& i : inputData) {
         plane.push_back(i);
      }
      output.addFacet(plane);
      return output;
   }

   SolveNaive(input, output);

   return output;
}

Polyhedron& SolverJarvis::SolveNaive(data_t& input, Polyhedron& output) {
   const data_t& idata = input;
   //debug = 1;
   typedef std::pair<unsigned, unsigned> edge_t;
   edge_t init = findInitial(idata, DOWN);
   // discovered and processed edges
   std::set<edge_t> fresh, closed;
   if (debug) {
      std::cout << "init: ";
      printPoint(input[init.first]);
      printPoint(input[init.second]);
   }
   std::set<unsigned> coplanarPointsInside;
   std::map<edge_t, facet_idx_t> edgeFacetMap;
   do {
      // points a, b
      edge_t curr = {init.first, init.second};
      if (fresh.size() > 0) {
         curr = *(fresh.begin());
      }
      if (debug) {
         std::cout << "edge: ";
         printPoint(input[curr.first], 0);
         std::cout << " , ";
         printPoint(input[curr.second], 1);
      }
      point_t vab = {idata[curr.first][0] - idata[curr.second][0],
         idata[curr.first][1] - idata[curr.second][1],
         idata[curr.first][2] - idata[curr.second][2]};

      point_t vcb, vperp;
      unsigned c = UINT_MAX;
      // find some non-collinear c
      facet_idx_t oppFacet = edgeFacetMap[curr]; //if the edge is already on some facet, which one is it?
      for (unsigned i = 0; i < idata.size(); i++) {
         // cannot reuse a, b
         if (i == curr.first || i == curr.second) {
            continue;
         }

         vcb = {idata[i][0] - idata[curr.second][0],
            idata[i][1] - idata[curr.second][1],
            idata[i][2] - idata[curr.second][2]};
         // find vector perpendicular to abc
         vperp = perpendNormal3d(vab, vcb);
         // if c not collinear, use it
         if (!zeroVect(vperp)) {
            c = i;
            break;
         }
      }

      // all input points on one line
      if (c == UINT_MAX) {
         //R("collinear set");
         return output;
      }

      unsigned currC = c;
      std::vector<unsigned> onPlane;
      onPlane.push_back(c);
      for (unsigned i = 0; i < idata.size(); i++) {
         // cannot reuse a, b and initial c
         if (i == curr.first || i == curr.second || i == c) {
            continue;
         }

         // compute direction, if > 0 its on one side of the normal vector... we dont care which?
         double dd = dot(idata[i][0] - idata[curr.second][0],
                 idata[i][1] - idata[curr.second][1],
                 idata[i][2] - idata[curr.second][2],
                 vperp[0], vperp[1], vperp[2]);

         if (debug) {
            std::cout << "Checking point " << i << ": " << input[i][0] << ", " << input[i][1] << ", " << input[i][2] << ", has angleCos: " << dd;
            std::cout << ", Current points is ";
            printPoint(input[currC]);
         }
         //c is on the same side normal vector goes
         if (dd > EPS_LOC) {
            // new c
            currC = i;
            vcb = {idata[currC][0] - idata[curr.second][0],
               idata[currC][1] - idata[curr.second][1],
               idata[currC][2] - idata[curr.second][2]};
            vperp = perpendNormal3d(vab, vcb);
            onPlane.clear();
            onPlane.push_back(i);
         } else if (fabs(dd) < EPS_LOC) {
            // on the same plane as abc
            onPlane.push_back(i);
         }
      }

      c = currC;
      if (fresh.size() == 0) {
         for (unsigned i = 0; i < idata.size(); i++) {
            if (i == curr.first || i == curr.second || i == c) {
               continue;
            }
            double dd = dot(idata[i][0] - idata[curr.second][0],
                    idata[i][1] - idata[curr.second][1],
                    idata[i][2] - idata[curr.second][2],
                    vperp[0], vperp[1], vperp[2]);
            if (fabs(dd) < EPS_LOC) {
               if (std::find(onPlane.begin(), onPlane.end(), i) == onPlane.end())
                  onPlane.push_back(i);
            }
         }
      }
      // PROBLEM
      // this returns all points on faces, but I only need their convex hull
      // we need a way to find convex hull of points on plane, but in 3d

      // POSSIBLE SOLUTION
      // eliminate one non-zero coordinate, otherwise the points would be on line after elimination of a zero coord
      // normal vector has at least one non zero

      onPlane.push_back(curr.first);
      onPlane.push_back(curr.second);

      facet_t planar; // 3d points on plane converted to 2d
      std::pair<int, int> coords; // id of coordinates to use
      if (fabs(vperp[0]) > EPS_LOC) {
         coords = {1, 2};
      } else if (fabs(vperp[1]) > EPS_LOC) {
         coords = {0, 2};
      } else {
         coords = {0, 1};
      }

      for (auto& i : onPlane) {
         planar.push_back({idata[i][coords.first], idata[i][coords.second]});
      }

      // find points on diameter of face in ccw order
      GrahamScan2D solver;
      std::vector<unsigned> faceID;
      solver.solveID(planar, faceID);
      unsigned fs = faceID.size();

      bool flipped = 0;
      for (unsigned i = 0; i < fs; i++) {
         unsigned ex = onPlane[faceID[i]],
                 ey = onPlane[faceID[(i + 1) % fs]];
         if (ex == curr.second && ey == curr.first) {
            flipped = 1;
         }
      }

      // add new found edges to list, remove already found ones
      for (unsigned i = 0; i < fs; i++) {
         unsigned ex = onPlane[faceID[i]],
                 ey = onPlane[faceID[(i + 1) % fs]];
         edge_t oe = {ex, ey};
         if (ex >= ey) {
            oe = {ey, ex};
         }
         if (flipped) {
            std::swap(ex, ey);
         }
         //edge will be in closed on the SECOND time its encountered, first time i put it in
         if (closed.find(oe) != closed.end()) {

            fresh.erase({ey, ex});
            fresh.erase({ex, ey});
         } else {

            fresh.insert({ey, ex});
            closed.insert(oe);
         }
      }

      fresh.erase(curr);

      facet_t face;
      facet_idx_t face_idx; //facet made of indexes, not points
      for (auto i : faceID) {
         face.push_back(idata[onPlane[i]]);
         face_idx.push_back(onPlane[i]);
      }
      output.addFacet(face);
      output.addFacetIdx(face_idx);

      /*
            std::cout << "hull(){\n";
            for (point_t pt : face) {
               std::cout << "  translate([" << pt[0] << ", " << pt[1] << ", " << pt[2] << "])\n";
               std::cout << "     sphere(r=0.01);\n";
            }
            std::cout << "}\n";
            ;
       */
   } while (!fresh.empty());

   // polyhedron with two faces means a plane => remove one of the faces
   if (output.getSize() == 2) {
      output.popFacet();
   }

   return output;
}

Polyhedron& SolverJarvis::SolveParallel(data_t& input, Polyhedron& output, int numThr) {
   if(numThr == 1){
      Solve(input, output);
      return output;
   }
   
   const data_t& idata = input;
   typedef std::pair<unsigned, unsigned> edge_t;
   omp_set_num_threads(numThr);

   edge_t initDown = {2, 2}, initTop, initLeft, initRight, initBack, initFront;
   //this could go parallel
#pragma omp parallel
   {
#pragma omp sections
      {
#pragma omp section
         initDown = findInitial(idata, DOWN);
#pragma omp section
         initTop = findInitial(idata, TOP);
#pragma omp section
         initLeft = findInitial(idata, LEFT);
#pragma omp section
         initRight = findInitial(idata, RIGHT);
#pragma omp section
         initFront = findInitial(idata, FRONT);
#pragma omp section
         initBack = findInitial(idata, BACK);
      }
   }


   // discovered and processed edges
   std::set<edge_t> fresh, closed, opened;
#pragma omp parallel
   {
#pragma omp sections
      {
#pragma omp section
         WorkParallel(input, output, initDown, opened, closed);
#pragma omp section
         WorkParallel(input, output, initTop, opened, closed);
#pragma omp section
         WorkParallel(input, output, initLeft, opened, closed);
#pragma omp section
         WorkParallel(input, output, initRight, opened, closed);
#pragma omp section
         WorkParallel(input, output, initFront, opened, closed);
#pragma omp section
         WorkParallel(input, output, initBack, opened, closed);

      }
   }

   output.removeDuplicateFaces();
   return output;

}

void SolverJarvis::WorkParallel(data_t& input, Polyhedron& output, std::pair<unsigned, unsigned> init, std::set<std::pair<unsigned, unsigned> >& opened,
        std::set<std::pair<unsigned, unsigned> >& closed) {

   std::set<edge_t> fresh;

   const data_t& idata = input;

   do {
      edge_t curr = {init.first, init.second};
      if (fresh.size() > 0) {
         curr = *(fresh.begin());
      }
      bool test;
#pragma omp critical
      {
         test = closed.find(curr) == closed.end();
      }
      if (test) {
         //std::cout << "working with " << "[" << curr.first << ", " << curr.second << "]" << std::endl;

         point_t vab = {idata[curr.first][0] - idata[curr.second][0],
            idata[curr.first][1] - idata[curr.second][1],
            idata[curr.first][2] - idata[curr.second][2]};

         //point_t can be a point or vector
         point_t vcb, vperp;
         unsigned c = UINT_MAX;
         // find some non-collinear c
         for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b
            if (i == curr.first || i == curr.second) {
               continue;
            }
            vcb = {idata[i][0] - idata[curr.second][0],
               idata[i][1] - idata[curr.second][1],
               idata[i][2] - idata[curr.second][2]};
            // find vector perpendicular to abc
            vperp = perpendNormal3d(vab, vcb);
            // if c not collinear, use it
            if (!zeroVect(vperp)) {
               c = i;
               break;
            }
         }

         // all input points on one line
         if (c == UINT_MAX) {
            return; //output;
         }

         unsigned currC = c;
         std::vector<unsigned> onPlane;
         onPlane.push_back(c);
         for (unsigned i = 0; i < idata.size(); i++) {
            // cannot reuse a, b and initial c
            if (i == curr.first || i == curr.second || i == c) {
               continue;
            }

            // compute direction, if > 0 its on one side of the normal vector... we dont care which?
            double dd = dot(idata[i][0] - idata[curr.second][0],
                    idata[i][1] - idata[curr.second][1],
                    idata[i][2] - idata[curr.second][2],
                    vperp[0], vperp[1], vperp[2]);

            //c is on the same side normal vector goes
            if (dd > EPS_LOC) {
               // new c
               currC = i;
               vcb = {idata[currC][0] - idata[curr.second][0],
                  idata[currC][1] - idata[curr.second][1],
                  idata[currC][2] - idata[curr.second][2]};
               vperp = perpendNormal3d(vab, vcb);
               onPlane.clear();
               onPlane.push_back(i);
            } else if (fabs(dd) < EPS_LOC) {
               // on the same plane as abc
               onPlane.push_back(i);
            }
         }

         c = currC;

         if (fresh.size() == 0) {
            for (unsigned i = 0; i < idata.size(); i++) {
               if (i == curr.first || i == curr.second || i == c) {
                  continue;
               }
               double dd = dot(idata[i][0] - idata[curr.second][0],
                       idata[i][1] - idata[curr.second][1],
                       idata[i][2] - idata[curr.second][2],
                       vperp[0], vperp[1], vperp[2]);
               if (fabs(dd) < EPS_LOC)
                  if (std::find(onPlane.begin(), onPlane.end(), i) == onPlane.end())
                     onPlane.push_back(i);
            }
         }

         // PROBLEM
         // this returns all points on faces, but I only need their convex hull
         // we need a way to find convex hull of points on plane, but in 3d

         // POSSIBLE SOLUTION
         // eliminate one non-zero coordinate, otherwise the points would be on line after elimination of a zero coord
         // normal vector has at least one non zero

         onPlane.push_back(curr.first);
         onPlane.push_back(curr.second);

         facet_t planar; // 3d points on plane converted to 2d
         std::pair<int, int> coords; // id of coordinates to use
         if (fabs(vperp[0]) > EPS_LOC) {
            coords = {1, 2};
         } else if (fabs(vperp[1]) > EPS_LOC) {
            coords = {0, 2};
         } else {
            coords = {0, 1};
         }

         for (auto& i : onPlane) {
            planar.push_back({idata[i][coords.first], idata[i][coords.second]});
         }

         // find points on diameter of face in ccw order
         GrahamScan2D solver;
         std::vector<unsigned> faceID;
         solver.solveID(planar, faceID);
         unsigned fs = faceID.size();
         //
         bool flipped = 0;
         for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                    ey = onPlane[faceID[(i + 1) % fs]];
            if (ex == curr.second && ey == curr.first) {

               flipped = 1;
            }
         }


         // add new found edges to list, remove already found ones
         for (unsigned i = 0; i < fs; i++) {
            unsigned ex = onPlane[faceID[i]],
                    ey = onPlane[faceID[(i + 1) % fs]];
            edge_t oe = {ex, ey};
            if (ex >= ey) {
               oe = {ey, ex};
            }
            if (flipped) {
               std::swap(ex, ey);
            }
            //crit
#pragma omp critical
            {
               if (opened.find(oe) != opened.end()) {
                  fresh.erase({ey, ex});
                  fresh.erase({ex, ey});
                  closed.insert(oe);
               } else {
                  fresh.insert({ey, ex});
                  opened.insert(oe);
               }
            }
         }

#pragma omp critical
         {
            fresh.erase(curr);
            closed.insert(curr);
         }


         facet_t face;
         facet_idx_t face_idx; //facet made of indexes, not points
         for (auto i : faceID) {
            face.push_back(idata[onPlane[i]]);
            face_idx.push_back(onPlane[i]);
         }
         //crit
#pragma omp critical 
         {
            output.addFacet(face);
            output.addFacetIdx(face_idx);
         }
         /*       
      std::cout << "hull(){\n";
      for (point_t pt : face) {
         std::cout << "  translate([" << pt[0] << ", " << pt[1] << ", " << pt[2] << "])\n";
         std::cout << "     sphere(r=0.01);\n";
      }
      std::cout << "}\n";
      ;*/

      } else {
#pragma omp critical
         fresh.erase(curr);
      }
   } while (!fresh.empty());

   // polyhedron with two faces means a plane => remove one of the faces
   if (output.getSize() == 2) {
      output.popFacet();
   }
}

std::pair<unsigned, unsigned> SolverJarvis::findInitial(const data_t& input, const Direction direction) {
   srand(time(NULL));
   int rndx = 0, rndy = 0, rndz = 0;
   double maxd;
   unsigned far; //the furthest point    

   // find farthest point in direction
   far = 0;
   switch (direction) {
      case DOWN:
         maxd = input[0][1];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][1] < maxd) {
               maxd = input[i][1];
               far = i;
            }
         };
         break;
      case TOP:
         maxd = input[0][1];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][1] > maxd) {
               maxd = input[i][1];
               far = i;
            }
         };
         break;
      case LEFT:
         maxd = input[0][0];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][0] < maxd) {
               maxd = input[i][0];
               far = i;
            }
         };
         break;
      case RIGHT:
         maxd = input[0][0];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][0] > maxd) {
               maxd = input[i][0];
               far = i;
            }
         };
         break;
      case FRONT:
         maxd = input[0][2];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][2] > maxd) {
               maxd = input[i][2];
               far = i;
            }
         };
         break;
      case BACK:
         maxd = input[0][2];
         for (unsigned i = 0; i < input.size(); i++) {
            if (input[i][2] < maxd) {
               maxd = input[i][2];
               far = i;
            }
         };
         break;

   }
   
   unsigned paired = 0;
   maxd = DBL_MAX;
   double minLen = DBL_MAX;
   // find second point on edge = point with smallest angle to plane
   // perpendicular to rng vector from previous step
   for (unsigned i = 0; i < input.size(); i++) {
      if (i == far) {
         continue;
      }
      
      point_t vFarI = {input[far][0] - input[i][0],
         input[far][1] - input[i][1],
         input[far][2] - input[i][2]};
      double iLen = sqrt(vFarI[0] * vFarI[0] + vFarI[1] * vFarI[1]
              + vFarI[2] * vFarI[2]);
      // directional distance

      switch (direction) {
         case TOP: rndx = 0;
            rndy = 1;
            rndz = 0;
            break;
         case DOWN: rndx = 0;
            rndy = -1;
            rndz = 0;
            break;
         case LEFT: rndx = -1;
            rndy = 0;
            rndz = 0;
            break;
         case RIGHT: rndx = 1;
            rndy = 0;
            rndz = 0;
            break;
         case FRONT: rndx = 0;
            rndy = 0;
            rndz = 1;
            break;
         case BACK: rndx = 0;
            rndy = 0;
            rndz = -1;
            break;

      }
      double d = dot(vFarI[0], vFarI[1], vFarI[2],
              rndx, rndy, rndz);
      d /= iLen;
      

      double dif = d - maxd;
      //cosinus lower = angle bigger
      if (dif < -EPS_LOC) {
         paired = i;
         maxd = d;
         minLen = iLen;        
      } else if (fabs(dif) < EPS_LOC) {
         if (iLen < minLen - EPS_LOC) {
            paired = i;
            minLen = iLen;
         }
      }
   }
   //int threadNum = omp_get_thread_num();
   return
   {
      far, paired
   };
}

void SolverJarvis::printDir(const Direction& dir) {
   switch (dir) {
      case DOWN: std::cout << "DOWN" << std::endl;
         break;
      case TOP: std::cout << "TOP" << std::endl;
         break;
      case LEFT: std::cout << "LEFT" << std::endl;
         break;
      case RIGHT: std::cout << "RIGHT" << std::endl;
         break;
      case FRONT: std::cout << "FRONT" << std::endl;
         break;
      case BACK: std::cout << "BACK" << std::endl;
         break;
   }
}

void printSet(std::set<std::pair<unsigned, unsigned> > &s) {
   //std::cout << "set "<< name << ": ";
   for (auto it = s.begin(); it != s.end(); it++) {
      std::cout << "[" << it->first << ", " << it->second << "]";
   }
   std::cout << std::endl;
}