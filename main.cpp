/* 
 * File:   main.cpp
 * Author: motykvac
 *
 * Created on February 23, 2017, 5:34 PM
 */

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <vector>
#include <stdexcept>
#include <string>
#include "SolverJarvis.h"
#include "structures.h"
#include "functions.h"
#include "geometry.h"
#include "HalfEdge.h"
#include "SolverQuickHull.h"
#include "SolverIncremental.h"
#include "SolverIncrementalOptimized.h"
#include "SolverDivide.h"
#include "Tester.h"

#include <omp.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <time.h>




using namespace std;

/*
 * 
 */

bool debug = 0;
double EPS_LOC = 1e-6;

/*TODO
 * halfedge optimalizovany inkremental - nestihnut
 * quickhull uprava
 * otestovat point vector vs struct
 * makefile
 * epsilon / eps_loc
 * divide pocet bodu
 * paralelni optimized
 */
int main(int argc, char** argv) {

   int numPoints, dummy;
   bool ownInput = 1;
   bool testing = 0;
   
   if (testing) ownInput = 0;
   else ownInput = 1;


   enum Type {
      QUICKHULL, JARVISMARCH, INCREMENTAL, INCREMENTALOPTI, DIVIDE, ALL
   };

   int parameters;
   const char * tvalue = NULL;
   int pvalue = 1;
   while ((parameters = getopt(argc, argv, "hta:p:")) != -1) {
      switch (parameters) {
         case 'a':
            tvalue = optarg;
            break;
         case 'p':
         {
            char * endp = NULL;
            strtol(optarg, &endp, 10);
            if (*endp != '\0') {
               std::cout << "Parameter -p expects a number." << std::endl;
               return -1;
            }

            pvalue = atoi(optarg);
            break;
         }
         case 'h':
            std::cout << "ConvexHull: Computes a convex hull of points in 3D\n";
            std::cout << "Usage:\n";
            std::cout << "-t: test algorithms\n";            
            std::cout << "-a: type of algorithm to use. Options: j (Jarvis March), i (Incremental algorithm), io (Optimized Incremental Algorithm)";
            std::cout << ", q (QuickHull), d (Divide & Conquer)\n";
            std::cout << "-p: number of threads to use\n";
            return 1;
         case 't':
                {
            unsigned algo;
            unsigned minP;
            unsigned maxP;
            unsigned numTests;
            unsigned numThr;
            char type;
            double dist;
            int boundSize;
            std::cout << "Which algorithm to test? (0) All, (1) Jarvis March, (2) Incremental, (3) Optimized incremental, (4) Quickhull, (5) Divide & Conquer\n";
            std::cin >> algo;
            
            std::cout << "Enter minimum number of points to test:\n";
            std::cin >> minP;
            
            std::cout << "Enter maximum number of points to test:\n";
            std::cin >> maxP;
 
            std::cout << "Enter number of tests:\n";
            std::cin >> numTests;
            
            
            std::cout << "Enter input type: (c) cube, (s) sphere \n";
            std::cin >> type;
            
            std::cout << "Enter input distribution (0-1): 0-100% from surface of the cube/sphere \n";
            std::cin >> dist;
            
            std::cout << "Enter size of bounding object:\n";
            std::cin >> boundSize;
            
            std::cout << "Enter number of threads:\n";
            std::cin >> numThr;
            
            std::vector<string> algos;
            switch(algo){
               case 1: algos.push_back("jarvis"); break;
               case 2: algos.push_back("incremental"); break;
               case 3: algos.push_back("incrementalOptimized");break;
               case 4: algos.push_back("quickhull");break;
               case 5: algos.push_back("divide");break;
               case 0: 
                algos.push_back("jarvis"); 
                algos.push_back("incremental"); 
                algos.push_back("incrementalOptimized");
                algos.push_back("quickhull");
                algos.push_back("divide");
                break;
            }
            
            Tester t;
            t.Test(algos, minP, maxP, numTests, type, dist, boundSize, numThr);
            
            return 1;
            }
         case '?':
            if (optopt == 'a') {
               std::cout << "Parameter -a requires an argument" << std::endl;
               return -1;
            }
            if (optopt == 'p') {
               std::cout << "Parameter -p requires an argument" << std::endl;
               return -1;
            } else {
               std::cout << "Unknown parameter. Use -h for help." << optopt << std::endl;
               return -1;
            }
      }
   }

   Type type = ALL;
   if (tvalue) {
      if (strcmp(tvalue, "j") == 0) {
         cout << "Using Jarvis march" << endl;
         type = JARVISMARCH;
      } else if (strcmp(tvalue, "q") == 0) {
         cout << "Using QuickHull" << endl;
         type = QUICKHULL;
      } else if (strcmp(tvalue, "i") == 0) {
         cout << "Using Incremental" << endl;
         type = INCREMENTAL;
      } else if (strcmp(tvalue, "io") == 0) {
         cout << "Using Optimized Incremental" << endl;
         type = INCREMENTALOPTI;
      } else if (strcmp(tvalue, "a") == 0) {
         cout << "Using ALL" << endl;
         type = ALL;
      } else if (strcmp(tvalue, "d") == 0) {
         cout << "Using Divide & Conquer" << endl;
         type = DIVIDE;      
      } else {
         std::cout << "Unknown -t argument" << std::endl;
         return -1;
      }
   }
   int numThreads = pvalue;
   if (numThreads < 1) {
      std::cout << "Invalid parameter -p argument, setting 1 instead.\n";
      numThreads = 1;
   }

   cout << "Enter dimension (works only with 3) number of points and points coordinates:" << endl;
   if (ownInput)
      cin >> dummy;
   if (ownInput)
      cin >> numPoints;
   string line;
   double xcoord = 0, ycoord = 0, zcoord = 0;
   //must be here to ger rid of first line
   if (ownInput)
      getline(cin, line);
   data_t data;


   /*Data input*/

   if (ownInput) {
      for (int i = 0; i < numPoints; i++) {
         getline(cin, line);
         istringstream iss(line);
         iss >> xcoord >> ycoord >> zcoord;
         //cout << xcoord << " " << ycoord << " " << zcoord << endl;
         point_t point;
         point.push_back(xcoord);
         point.push_back(ycoord);
         point.push_back(zcoord);
         
         data.push_back(point);
      }
   } else {
      /***FOR TESTING PURPOSES***/      
   }
   
   /***FOR TESTING PURPOSES***/
   if (testing) {
      Tester t;
      //t.TestGraham();
      //t.Test({"divide"}, 10000, 10000, 1, 's', 0.5, 100, 1);
      //t.TimeTest({"jarvis", "quickhull"}, 50, 200, 150, 'c', 0, 10, 1);
      //t.TimeTest({"quickhull"}, 100000, 400000, 3, 's', 1, 5000, 1);
      //t.ThreadTest({"quickhull"}, 2, 10, 10000, 's', 1, 1000);
     //t.ThreadTest({"incrementalOptimized"}, 1, 2, 2000, 's', 0, 1000);
      //t.Test({"all"},500, 15000, 100, 'c', 0.5, 100,4);
      //t.TimeTest({"divide"},50000, 50000, 1, 's', 0.5, 100,1);
      t.RandomTest({"quickhull"}, 1000, 200);
      return 1;
   }
   
   /*****NORMAL WORK*****/
   if(type == JARVISMARCH || type == ALL){
      Polyhedron solverOutput;
      SolverJarvis solver;
      data_t jdata = data;
      solver.SolveParallel(jdata, solverOutput, numThreads);
      std::cout << "Jarvis:\n";
      solverOutput.print();
   }
   if(type == INCREMENTAL || type == ALL){
      Polyhedron solverOutput;
      SolverIncremental solver;
      data_t idata = data;
      solver.SolveParallel(idata, solverOutput, numThreads);
      std::cout << "Incremental:\n";
      solverOutput.print();
   }
   if(type == INCREMENTALOPTI || type == ALL){
      Polyhedron solverOutput;
      SolverIncrementalOptimized solver;
      data_t idata = data;
      solver.SolveParallel(idata, solverOutput, numThreads);
      std::cout << "Incremental 2:\n";
      solverOutput.print();
   }
   if(type == QUICKHULL || type == ALL){
      Polyhedron solverOutput;
      SolverQuickHull solver;
      data_t idata = data;
      solver.SolveParallel(idata, solverOutput, numThreads);
      std::cout << "QuickHull:\n";
      solverOutput.print();
   }
   if(type == DIVIDE || type == ALL){
      Polyhedron solverOutput;
      SolverDivide solver;
      data_t idata = data;
      solver.SolveParallel(idata, solverOutput, numThreads);
      std::cout << "D&C:\n";
      solverOutput.print();
   }
   return 0;
}

