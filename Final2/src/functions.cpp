
#include "functions.h"
#include "geometry.h"

void removeDuplicatePoints(data_t & data){
   for(unsigned int i = 0; i < data.size(); i++){
      point_t p1 = data[i];
      for(unsigned int j = i+1; j < data.size(); j++){
         point_t p2 = data[j];
         if(p1 == p2){
            std::cout << "Removing duplicata point "; printPoint(p1);
            data.erase(data.begin() + j);
            j--;
         }
      }
   }
}

void removeCoplanarFaces(Polyhedron& p, std::vector<facet_t>& newFaces) {
   //for each new facet find its coplanars
   std::set<int> coplanarFacesIdx;
   std::set<point_t> coplanarPoints;
   int foundCoplanar = 0;
   point_t controlPoint = getAveragePoint(p);
   //find all coplanar faces, make hull of their vertices
   for (unsigned int i = 0; i < newFaces.size(); i++) {
      facet_t f = newFaces[i];

      for (unsigned int j = i + 1; j < newFaces.size(); j++) {//remove coplanars from newFacets, so I dont test them in the furute
         facet_t f2 = newFaces[j];
         if (areCoplanar(f, f2)) {
            newFaces.erase(newFaces.begin() + j);
            j--;
         }
      }

      int foundIdx;
      for (unsigned int j = 0; j < p.m_faces.size(); j++) {
         facet_t f2 = p.m_faces[j];
         if (areCoplanar(f, f2)) {
            if (debug) {
               std::cout << "merging coplanar faces\n";
               printFacet(f);
               std::cout << '\n';
               printFacet(f2);
            }
            foundIdx = j;
            coplanarFacesIdx.insert(j);
            for (point_t pt : f2) //add all facet
               coplanarPoints.insert(pt);
            foundCoplanar++;
         }
      }

      if (foundCoplanar == 1) {
         coplanarFacesIdx.erase(foundIdx);
         coplanarPoints.clear();
      }

      if (foundCoplanar > 1) { //f will be always found on p1, so if no other facet found as coplanar, size will be 1
         //find hull of these points        
         std::vector<point_t> onPlane(coplanarPoints.begin(), coplanarPoints.end());
         GrahamScan2D graham;
         std::vector<unsigned> ids;
         //because graham can only solve on x,y axes, i need to use only 2 coords
         point_t vab = {onPlane[1][0] - onPlane[0][0], onPlane[1][1] - onPlane[0][1], onPlane[1][2] - onPlane[0][2]};
         point_t vac = {onPlane[2][0] - onPlane[0][0], onPlane[2][1] - onPlane[0][1], onPlane[2][2] - onPlane[0][2]};
         point_t facetNormal = perpendNormal3d(vab, vac);

         facet_t planar; // 3d points on plane converted to 2d
         std::pair<int, int> coords; // id of coordinates to use
         if (fabs(facetNormal[0]) > EPS_LOC) {
            coords = {1, 2};
         } else if (fabs(facetNormal[1]) > EPS_LOC) {
            coords = {0, 2};
         } else {
            coords = {0, 1};
         }
         for (auto it : onPlane) {
            planar.push_back({it[coords.first], it[coords.second]});
         }


         graham.solveID(planar, ids);
         facet_t newFacet;
         for (auto i : ids)
            newFacet.push_back(onPlane[i]);
         makeFaceCCW(newFacet, controlPoint);
         p.addFacet(newFacet);
         if (debug) {
            std::cout << std::endl << " into: ";
            printFacet(newFacet);
            std::cout << "coplanar points are: ";
         }

      }
      //coplanarFacesIdx.clear();
      coplanarPoints.clear();
      foundCoplanar = 0;
   }

   for (auto it = coplanarFacesIdx.rbegin(); it != coplanarFacesIdx.rend(); it++)
      p.removeFacet(*it);
}


void discardHiddenFaces(Polyhedron& polyhedron, std::set<point_t>& horizon, std::set<point_t>& myHorizon, bool left) {
   //set of visible faces
   //dont test if already tested
   std::set<unsigned> visibleFacesIdx;
   std::set<unsigned> toDiscard;
   std::map<point_t, std::set<unsigned> > facesMap = polyhedron.getFacesMap();
   std::set<point_t> verts = polyhedron.getVertices();
   std::set<point_t> testedPoints = myHorizon;   
   point_t extremePoint = *myHorizon.begin();
   
   if(left == 1) // find leftmost point
      for(point_t pt : myHorizon){
         if(pt[0] < extremePoint[0])
            extremePoint = pt;
      }
   else //find rightmost point
      for(point_t pt : myHorizon){
         if(pt[0] > extremePoint[0])
            extremePoint = pt;
      }
   
   for(point_t pt : verts){
      if(left){
         if(pt[0] > extremePoint[0])
            testedPoints.insert(pt);
      }else{
         if(pt[0] < extremePoint[0])
            testedPoints.insert(pt);
      }
   }
   
   for (point_t pt : testedPoints) {
      for (unsigned u : facesMap[pt]) //all faces with pt
         visibleFacesIdx.insert(u);
   }

   for (point_t point : horizon) {
      for (int facetIdx : visibleFacesIdx) {
         if (pointCanSeeFacet(point, polyhedron.m_faces[facetIdx]))
            toDiscard.insert(facetIdx);
      }
      for (int i : toDiscard)
         visibleFacesIdx.erase(i);

   }
   
   //std::cout << "will discard " << toDiscard.size() << "\n";
   for (auto it = toDiscard.rbegin(); it != toDiscard.rend(); it++) {
      polyhedron.removeFacet(*it);
   }
}


void discardHiddenFaces(Polyhedron& polyhedron, std::set<point_t>& horizon) { //p is polyhedron and horizon is horizon of the polyhedron I am merging with
   //for each point of horizon look which faces of p are visible, these will be to be deleted
   //set of visible faces
   std::set<unsigned> visibleFacesIdx;
   std::set<unsigned> toDiscard;
   for (unsigned int i = 0; i < polyhedron.m_faces.size(); i++)
      visibleFacesIdx.insert(i);

   for (point_t point : horizon) {
      for (int facetIdx : visibleFacesIdx) {
         if (pointCanSeeFacet(point, polyhedron.m_faces[facetIdx]))
            toDiscard.insert(facetIdx);
      }
      for (int i : toDiscard)
         visibleFacesIdx.erase(i);

   }
   //std::cout << "will discard " << toDiscard.size() << "\n";
   for (auto it = toDiscard.rbegin(); it != toDiscard.rend(); it++) {
      polyhedron.removeFacet(*it);
   }
}


edgePoints_t findLowerTangent(Polyhedron& p1, Polyhedron& p2) {
   //convert p1 and p2 to 2d
   std::set<point_t> p1PointsSet;
   std::set<point_t> p2PointsSet;

   //set - i dont want duplicates
   for (unsigned int i = 0; i < p1.m_faces.size(); i++) {
      for (unsigned int j = 0; j < p1.m_faces[i].size(); j++) {
         point_t p;
         p.push_back(p1.m_faces[i][j][0]);
         p.push_back(p1.m_faces[i][j][1]);
         p1PointsSet.insert(p);
      }
   }
   for (unsigned int i = 0; i < p2.m_faces.size(); i++) {
      for (unsigned int j = 0; j < p2.m_faces[i].size(); j++) {
         point_t p;
         p.push_back(p2.m_faces[i][j][0]);
         p.push_back(p2.m_faces[i][j][1]);
         p2PointsSet.insert(p);
      }
   }

   std::vector<point_t> p1Points(p1PointsSet.begin(), p1PointsSet.end());
   std::vector<point_t> p2Points(p2PointsSet.begin(), p2PointsSet.end());
   //now find CCW convex hull of the 2d points
   std::vector<unsigned> p1PointsHullIdx;
   std::vector<unsigned> p2PointsHullIdx;
   GrahamScan2D graham;
   graham.solveID(p1Points, p1PointsHullIdx);
   graham.solveID(p2Points, p2PointsHullIdx);
   //graham will retirn in CW order
   facet_t p1Hull;
   facet_t p2Hull;
   for (int idx : p1PointsHullIdx)
      p1Hull.push_back(p1Points[idx]);

   for (int idx : p2PointsHullIdx)
      p2Hull.push_back(p2Points[idx]);
   
   if (debug) {
      std::cout << "Computing lower tangent. Hulls projected to 2d are: \n";
      for (unsigned int i = 0; i < p1PointsHullIdx.size(); i++) {
         printPoint(p1Hull[i]);
      }
      std::cout << "\n";
      for (unsigned int i = 0; i < p2PointsHullIdx.size(); i++) {
         printPoint(p2Hull[i]);
      }
   }
   //find rightmost point on left hull and leftmost on the right one
   int rightmostIdx = 0;
   int leftmostIdx = 0;
   for (unsigned int i = 1; i < p1Hull.size(); i++) {
      if (p1Hull[i][0] > p1Hull[rightmostIdx][0])
         rightmostIdx = i;
   }
   for (unsigned int i = 1; i < p2Hull.size(); i++) {
      if (p2Hull[i][0] < p2Hull[leftmostIdx][0])
         leftmostIdx = i;
   }

   edgePoints_t currentEdge = {p1Hull[rightmostIdx], p2Hull[leftmostIdx]};

   int leftIdx = leftmostIdx;
   int rightIdx = rightmostIdx;
   //bool isLeftTangent = 0, isRightTangent = 0;

   //while edge is not lower tangent to both, make it lower tangent to one, then to other and check again
   //computational geometry in C p. 92
   while (isLowerTangent(p1Hull, currentEdge, rightIdx) == 0 || isLowerTangent(p2Hull, currentEdge, leftIdx) == 0) {
      while (!isLowerTangent(p1Hull, currentEdge, rightIdx)) {
         rightIdx = (rightIdx + 1) % p1Hull.size();
         currentEdge = {p1Hull[rightIdx], p2Hull[leftIdx]};
      }
      while (!isLowerTangent(p2Hull, currentEdge, leftIdx)) {
         leftIdx = (leftIdx - 1 + p2Hull.size()) % p2Hull.size();
         currentEdge = {p1Hull[rightIdx], p2Hull[leftIdx]};
      }
   }

   edgePoints_t finalEdge;
   std::set<point_t> vertices = p1.getVertices();
   for (auto point : vertices) {
      if (point[0] == currentEdge.first[0] && point[1] == currentEdge.first[1]) {
         finalEdge.first = point;
         break;
      }
   }
   vertices = p2.getVertices();
   for (auto point : vertices) {
      if (point[0] == currentEdge.second[0] && point[1] == currentEdge.second[1]) {
         finalEdge.second = point;
         break;
      }
   }
   return finalEdge;
}


bool isLowerTangent(facet_t& facet, edgePoints_t& edge, int edgePointIdx) {
   //next and previous point on the facet must lie under the edge line
   //https://math.stackexchange.com/questions/128061/check-if-point-is-on-or-below-line-when-either-x-or-y-0

   point_t prevPoint = facet[(edgePointIdx - 1 + facet.size()) % facet.size()];
   point_t nextPoint = facet[(edgePointIdx + 1) % facet.size()];
   //      ax                ay             bx              by              cx            cy
   int orientationPrev = orientation(edge.first[0], edge.first[1], edge.second[0], edge.second[1], prevPoint[0], prevPoint[1]);
   int orientationNext = orientation(edge.first[0], edge.first[1], edge.second[0], edge.second[1], nextPoint[0], nextPoint[1]);
   //1 means above line, 0 on line, 2 under line - line has to be from left to right
   if (orientationNext != 2 && orientationPrev != 2)
      return 1;
   return 0;
}

void makeFaceCCW(facet_t& facet, point_t & outside) {
   //vector from point 1 to 2
   point_t v12 = {facet[1][0] - facet[0][0], facet[1][1] - facet[0][1], facet[1][2] - facet[0][2]};
   point_t v13 = {facet[2][0] - facet[0][0], facet[2][1] - facet[0][1], facet[2][2] - facet[0][2]};
   point_t vperp = perpendNormal3d(v12, v13);

   point_t v14 = {outside[0] - facet[0][0], outside[1] - facet[0][1], outside[2] - facet[0][2]};
   double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);
   //printPoint(vperp);
   //vperp going from me, angle with vector to outside point is 0-90° means points on the facet are in CW! order
   if (angleCos > EPS_LOC) {
      //change order
      facet_t correctFacet;
      if (facet.size() == 3) {
         correctFacet.push_back(facet[0]);

         point_t p1 = facet[2];
         point_t p2 = facet[1];
         correctFacet.push_back(p1);
         correctFacet.push_back(p2);
      } else {
         //change the order of points
         for (auto it = facet.rbegin(); it != facet.rend(); it++) {
            correctFacet.push_back(*it);
         }
      }
      facet = correctFacet;
   }
   if (fabs(angleCos) < EPS_LOC) {
      //std::cerr << "makeFaceCCW(): Points are coplanar\n";
      // throw ("points are coplanar");
   }
}

bool isCollinear(point_t& p, edgePoints_t & e) {
   point_t ve = {e.second[0] - e.first[0], e.second[1] - e.first[1], e.second[2] - e.first[2]};
   point_t vep = {p[0] - e.first[0], p[1] - e.first[1], p[2] - e.first[2]};
   point_t vperp = perpendNormal3d(ve, vep);
   if (zeroVect(vperp))
      return 1;
   return 0;
}


bool pointCanSeeFacet(point_t& point, facet_t & f) {

   point_t v12 = {f[1][0] - f[0][0], f[1][1] - f[0][1], f[1][2] - f[0][2]};
   point_t v13 = {f[2][0] - f[0][0], f[2][1] - f[0][1], f[2][2] - f[0][2]};
   point_t vperp = perpendNormal3d(v12, v13);

   point_t v14 = {point[0] - f[0][0], point[1] - f[0][1], point[2] - f[0][2]};
   double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);

   if (angleCos > EPS_LOC) {
      return 1;
   }
   return 0;
}


bool areCoplanar(facet_t& f1, facet_t& f2) {
   //vector ab, ac, their normal 
   point_t vab = {f1[1][0] - f1[0][0], f1[1][1] - f1[0][1], f1[1][2] - f1[0][2]};
   point_t vac = {f1[2][0] - f1[0][0], f1[2][1] - f1[0][1], f1[2][2] - f1[0][2]};
   point_t f1Normal = perpendNormal3d(vab, vac);
   vab = {f2[1][0] - f2[0][0], f2[1][1] - f2[0][1], f2[1][2] - f2[0][2]};
   vac = {f2[2][0] - f2[0][0], f2[2][1] - f2[0][1], f2[2][2] - f2[0][2]};
   point_t f2Normal = perpendNormal3d(vab, vac);
   
   if (zeroVect(perpendNormal3d(f1Normal, f2Normal))) {
      //two planes can have the same normal, but not be coplanar (one facet moved in one direction(fwd, bckwd ...))
      if (isCoplanar(f1, f2[0])) //but if they are coplanar, all points will be coplanar too, so its enough to test one
         return 1;
   }
   return 0;
}

bool isCoplanar(facet_t& f, point_t & point) {
   point_t v12 = {f[1][0] - f[0][0], f[1][1] - f[0][1], f[1][2] - f[0][2]};
   point_t v13 = {f[2][0] - f[0][0], f[2][1] - f[0][1], f[2][2] - f[0][2]};
   point_t vperp = perpendNormal3d(v12, v13);

   point_t v14 = {point[0] - f[0][0], point[1] - f[0][1], point[2] - f[0][2]};
   double angleCos = dot(v14[0], v14[1], v14[2], vperp[0], vperp[1], vperp[2]);
   if (fabs(angleCos) < EPS_LOC) {
      return 1;
   }
   return 0;
}

point_t getAveragePoint(Polyhedron& p) {
   std::set<point_t> allVertices = p.getVertices();
   double avgx = 0, avgy = 0, avgz = 0;
   for (auto pt : allVertices) {
      avgx += pt[0];
      avgy += pt[1];
      avgz += pt[2];
   }
   avgx /= allVertices.size();
   avgy /= allVertices.size();
   avgz /= allVertices.size();
   return
   {
      avgx, avgy, avgz
   };
}


point_t getAveragePoint(std::vector<point_t>& allVertices) {
   double avgx = 0, avgy = 0, avgz = 0;
   for (unsigned int i = 0; i < allVertices.size(); i++) {
      avgx += allVertices[i][0];
      avgy += allVertices[i][1];
      avgz += allVertices[i][2];
   }
   avgx /= allVertices.size();
   avgy /= allVertices.size();
   avgz /= allVertices.size();
   return
   {
      avgx, avgy, avgz
   };
}

bool comparePolyhedrons(Polyhedron& p1, Polyhedron& p2, std::string type) {

   if (p1.getNumFaces() != p2.getNumFaces()) {
      std::cerr << "Polyhedrons differ in size " << std::endl;
      return 0;
   }
   for (unsigned int i = 0; i < p1.m_faces.size(); i++) {
      //find the same polyhderon on p2
      facet_t f = p1.m_faces[i];
      bool found = 0;

      for (unsigned int j = 0; j < p2.m_faces.size(); j++) {
         facet_t f2 = p2.m_faces[j];
         if (type == "CCW") {
            if (checkFacetsAreTheSame(f, f2))
               found = 1;
         } else {
            if (checkFacetsAreTheSame(f, f2, "NOTCCW"))
               found = 1;
         }
      }

      if (!found) {
         std::cout << "Polyhedrons differ, facet not found" << std::endl;
         printFacet(f);
         //return 0;
      }

   }
   std::cout << "Polyhedrons OK" << std::endl;
   return 1;
}


bool comparePolyhedronsFast(Polyhedron& p1, Polyhedron& p2){
   if (p1.getNumFaces() != p2.getNumFaces()) {
      std::cout << "Polyhedrons differ in size " << p1.getNumFaces() << " / " << p2.getNumFaces() << std::endl;
      return 0;
   }
   std::set<point_t> p1Verts = p1.getVertices();
   std::set<point_t> p2Verts = p2.getVertices();
   if(p1Verts.size() != p2Verts.size()){
      std::cout << "Polyhedrons have different number of vertices" << std::endl;
      return 0;
   }
   for(point_t pt : p1Verts){
      if(p2Verts.find(pt) == p2Verts.end()){
         std::cout << "Polyhedrons differ: vertex not found" << std::endl;
         return 0;
      }
   }
   return 1;      
}


void printJava(data_t data) {
   std::ofstream file;
   file.open("./in/java");

   for (unsigned int i = 0; i < data.size(); i++) {
      point_t point = data[i];
      file << "new Point3d (" << point[0] << "," << point[1] << "," << point[2] << ")," << std::endl;
   }

   file.close();
}


void printOpenScadEachFacetVect(std::vector<facet_t>& fs) {
   std::cout << "$fn = 10;\n";
   for (facet_t f : fs) {
      std::cout << "hull(){\n";
      for (point_t pt : f) {
         std::cout << "  translate([" << pt[0] << ", " << pt[1] << ", " << pt[2] << "])\n";
         std::cout << "     sphere(r=0.01);\n";
      }
      std::cout << "}\n";
   }
}


void printOpenScadEachFacet(Polyhedron& p) {
   std::cout << "$fn = 10;\n";
   for (facet_t f : p.m_faces) {
      std::cout << "hull(){\n";
      for (point_t pt : f) {
         std::cout << "  translate([" << pt[0] << ", " << pt[1]<< ", " << pt[2]<< "])\n";
         std::cout << "     sphere(r=0.01);\n";
      }
      std::cout << "}\n";
   }
}


void printOpenScad(Polyhedron& p) {
   std::set<point_t> vert = p . getVertices();

   std::cout << "$fn = 10;\n";
   std::cout << "hull(){\n";
   for (point_t pt : vert) {
      std::cout << "  translate([" << pt[0] << ", " << pt[1] << ", " << pt[2] << "])\n";
      std::cout << "     sphere(r=0.01);\n";
   }

   std::cout << "}\n";
}


void printGeomView(data_t data) {
   std::ofstream file;
   file.open("./in/geom");
   file << "LIST" << std::endl;
   for (unsigned int i = 0; i < data.size(); i++) {
      point_t point = data[i];
      file << "SPHERE 0.1 " << point[0] << " " << point[1] << " " << point[2] << std::endl;
   }

   file.close();
}


void printAllPoints(std::vector<point_t>& v) {
   for (unsigned int i = 0; i < v.size(); i++) {
      printPoint(v[i]);
      //std::cout << std::endl;
   }
}

bool checkPointsAreTheSame(point_t& first, point_t& second) {
   if (first[0] == second[0] && first[1] == second[1] && first[2] == second[2]) return 1;
   return 0;
}

bool checkFacetsAreTheSame(facet_t& first, facet_t& second, std::string type) {
   if (type == "CCW") {
      if (first.size() != second.size()) return 0;

      point_t firstPoint = first[0];
      unsigned int i = 0;
      for (unsigned i = 0; i < second.size(); i++) {
         if (checkPointsAreTheSame(firstPoint, second[i])) break;
      }
      //i will be the the position of first point in second facet
      if (i > second.size()) return 0; //first point is not in second facet

      for (unsigned int j = 0; j < first.size(); j++, i++) {
         if (checkPointsAreTheSame(first[j], second[i % second.size()]) == 0) return 0;
      }
      return 1;
   } else {
      //for each point of f1 look if it is in f2
      for (unsigned int i = 0; i < first.size(); i++) {
         point_t p1 = first[i];
         bool found = 0;
         for (unsigned int j = 0; j < second.size(); j++) {
            if (checkPointsAreTheSame(p1, second[j])) {
               found = 1;
               break;
            }
         }
         //some point not found
         if (found == 0) {
            return 0;
         }
      }
      return 1;
   }
}



void mergeTwoHullsOptimized(Polyhedron& p1, Polyhedron& p2, edgePoints_t& lowerTangent) {
   std::set<point_t> p1VerticesSet = p1.getVertices();
   std::set<point_t> p2VerticesSet = p2.getVertices();
   std::vector<point_t> p1Vertices(p1VerticesSet.begin(), p1VerticesSet.end());
   std::vector<point_t> p2Vertices(p2VerticesSet.begin(), p2VerticesSet.end());
   std::vector<point_t> allVertices = p1Vertices;
   for (unsigned int i = 0; i < p2Vertices.size(); i++)
      allVertices.push_back(p2Vertices[i]);
   std::vector<facet_t> newFacets;
   std::vector<facet_t> possiblyCoplanarFacets;
   std::set<point_t> usedPoints;
   std::set<point_t> p1HorizonVertices;
   std::set<point_t> p2HorizonVertices;
   std::map<point_t, std::set<point_t> > p1Neighbours = p1.getNeighboursMap();
   std::map<point_t, std::set<point_t> > p2Neighbours = p2.getNeighboursMap();

   bool notWorkingOnLowerTangentFacet = 0; //At the beginning I will add lowertangent edgepoints to used edges, so I dont have infinite loop at the first 
   //facet. Once I am working on facet not coplanar with lowertangent, i can erase them from used points until I gift wrap it all


   debug = 0;

   point_t controlPoint = getAveragePoint(allVertices);
   point_t currC, direction;
   if (debug) {
      std::cout << "Controlpoint: ";
      printPoint(controlPoint, 0);
   }
   edgePoints_t curr = {lowerTangent.first, lowerTangent.second};
   edgePoints_t prevEdge;
   facet_t newFacet;
   facet_t currFacet;
   usedPoints.insert(lowerTangent.first);
   usedPoints.insert(lowerTangent.second);
   bool first = 1;
   point_t lastPerp;
   //double wrapTimeA = omp_get_wtime();
   bool foundCoplanar = 0;
   edgePoints_t firstAddedEdge;
   do {
      if (debug) {
         std::cout << "Edge: ";
         printPoint(curr.first);
         printPoint(curr.second, 0);
         std::cout << ", Direction: ";
         printPoint(direction, 1);
      }
      point_t vab = {curr.second[0] - curr.first[0],
         curr.second[1] - curr.first[1],
         curr.second[2] - curr.first[2]};

      point_t vcb, vperp, vac;
      point_t c, point;

      foundCoplanar = 0;

      std::vector<point_t> neighbours(p1Neighbours[curr.first].begin(), p1Neighbours[curr.first].end());
      for (point_t pt : p2Neighbours[curr.second])
         neighbours.push_back(pt);
      //std::cout << neighbours.size() << std::endl;
      // find some non-collinear c, coplanar if possible
      for (unsigned i = 0; i < neighbours.size(); i++) {
         // cannot reuse a, b
         point = neighbours[i];
         if (point == curr.first || point == curr.second) {
            continue;
         }

         if (usedPoints.find(point) != usedPoints.end()) //cannot use already used
            continue;

         facet_t tmp = {curr.first, curr.second, point};
         bool foundSameFacet = 0;
         for (facet_t alreadyAdded : newFacets) {
            if (checkFacetsAreTheSame(tmp, alreadyAdded, "NOTCCW")) {
               //std::cout << "same facet found\n";
               foundSameFacet = 1;
            }
         }
         if (foundSameFacet == 1) continue;

         vac = {point[0] - curr.first[0], point[1] - curr.first[1], point[2] - curr.first[2]};
         // find vector perpendicular to abc
         vperp = perpendNormal3d(vab, vac);
         if (first) { //in first iteration just find some noncollinear
            // if c not collinear, use it         
            if (!zeroVect(vperp)) {
               c = point;
               //first = 0;
               break;
            }
         } else { //in later iterations look for coplanars first            
            if (isCoplanar(newFacet, point)) {               
               point_t tmp = {point[0] - curr.first[0], point[1] - curr.first[1], point[2] - curr.first[2]};
               if (dot(direction, tmp) < EPS_LOC) //i need to be goind forwards all the time
                  continue;
               c = point;
               foundCoplanar = 1;
               break;
            }
            if (!zeroVect(vperp))
               c = point;
         }
      }

      //when there are coplanar faces, it is possible that i will not find the exact edge as was lowerTangent,
      //this checks if I am done with wrapping, if so, I can end wrapping
      if (foundCoplanar == 1 && notWorkingOnLowerTangentFacet == 1 && (c == lowerTangent.first || c == lowerTangent.second))
         break;
      if(c.size() == 0)
         break;

      if (debug) {
         std::cout << "First noncoll. C is: ";
         printPoint(c);
      }
      currFacet = {curr.first, curr.second, c};
      if (first)
         makeFaceCCW(currFacet, controlPoint);
      else if (isCoplanar(currFacet, prevEdge.first) && isCoplanar(currFacet, prevEdge.second)) {
         makeFaceCCW(currFacet, controlPoint);
      } else {
         makeFaceCCW(currFacet, prevEdge.first);
         makeFaceCCW(currFacet, prevEdge.second);
      }

      vab = {currFacet[1][0] - currFacet[0][0], currFacet[1][1] - currFacet[0][1], currFacet[1][2] - currFacet[0][2]};
      vac = {currFacet[2][0] - currFacet[0][0], currFacet[2][1] - currFacet[0][1], currFacet[2][2] - currFacet[0][2]};
      vperp = perpendNormal3d(vab, vac);

      currC = c;
      for (unsigned i = 0; i < neighbours.size(); i++) {
         point_t point = neighbours[i];
         if (point == curr.first || point == curr.second || point == c) {
            continue;
         }
  
         // compute direction, if > 0 its on one side of the normal vector goes to
         double dd = dot(point[0] - curr.second[0],
                 point[1] - curr.second[1],
                 point[2] - curr.second[2],
                 vperp[0], vperp[1], vperp[2]);

         if (debug) {
            std::cout << "Point ";
            printPoint(point);
            std::cout << "has angle cos: " << dd << ", current c is ";
            printPoint(currC, 0);
            std::cout << "vector: ";
            point_t pppp = {point[0] - curr.second[0],
               point[1] - curr.second[1],
               point[2] - curr.second[2]};
            printPoint(pppp);
            std::cout << ", vperp: ";
            printPoint(vperp);
            std::cout << '\n';
         }
         //c is on the same side normal vector goes
         if (dd > -EPS_LOC) {
            // new c

            if (fabs(dd) < EPS_LOC) { //if coplanar, change only if the facet is smaller than actual
               if (isCollinear(point, curr))
                  continue;
               if (!first) {
                  //point_t tmp = {point[0] - prevEdge.first[0], point[1] - prevEdge.first[1], point[2] - prevEdge.first[2]};
                  point_t tmp = {point[0] - curr.first[0], point[1] - curr.first[1], point[2] - curr.first[2]};
                  normalize(tmp);
                  if (dot(direction, tmp) < EPS_LOC) //i need to be goind forwards all the time
                     continue;
               }

               if (usedPoints.find(point) != usedPoints.end())
                  continue;
               //if I found another coplanar, I want the one closest to the curr edge

               if (notWorkingOnLowerTangentFacet && (point == lowerTangent.first || point == lowerTangent.second)) { //if i am on first side again, prefer lt points
               ;;
               } else {
                  double actLen = vectSqr3d({curr.first[0] - currC[0], curr.first[1] - currC[1], curr.first[2] - currC[2]});
                  actLen += vectSqr3d({curr.second[0] - currC[0], curr.second[1] - currC[1], curr.second[2] - currC[2]});

                  double newLen = vectSqr3d({curr.first[0] - point[0], curr.first[1] - point[1], curr.first[2] - point[2]});
                  newLen += vectSqr3d({curr.second[0] - point[0], curr.second[1] - point[1], curr.second[2] - point[2]});

                  if (newLen >= actLen) //if its further dont use it
                     continue;
               }
               foundCoplanar = 1;
            }

            facet_t tmp = {curr.first, curr.second, point};
            bool foundSameFacet = 0;
            for (facet_t alreadyAdded : newFacets) {
               if (checkFacetsAreTheSame(tmp, alreadyAdded, "NOTCCW")) {
                  //std::cout << "same facet found\n";
                  foundSameFacet = 1;
               }
            }
            if (foundSameFacet == 1) continue;

            currC = point;
            currFacet = {curr.first, curr.second, point};

            if (first)
               makeFaceCCW(currFacet, controlPoint);
            else if (isCoplanar(currFacet, prevEdge.first) && isCoplanar(currFacet, prevEdge.second)) {
               makeFaceCCW(currFacet, controlPoint);
            } else {
               makeFaceCCW(currFacet, prevEdge.first);
               makeFaceCCW(currFacet, prevEdge.second);

            }
            vab = {currFacet[1][0] - currFacet[0][0], currFacet[1][1] - currFacet[0][1], currFacet[1][2] - currFacet[0][2]};
            vac = {currFacet[2][0] - currFacet[0][0], currFacet[2][1] - currFacet[0][1], currFacet[2][2] - currFacet[0][2]};
            vperp = perpendNormal3d(vab, vac);
            if (debug) {
               std::cout << "new facet: \n";
               printFacet(currFacet);
            }
            if (foundCoplanar == 1 && notWorkingOnLowerTangentFacet == 1 && (point == lowerTangent.first || point == lowerTangent.second))
               break;
         }
      }
      first = 0;
      c = currC;
      usedPoints.insert(c);

      if (debug) {
         std::cout << "\n Using point ";
         printPoint(c);
         std::cout << std::endl;
      }

      newFacet = {curr.first, curr.second, c};
      makeFaceCCW(newFacet, controlPoint);
      newFacets.push_back(newFacet);
      if (foundCoplanar == 1)
         possiblyCoplanarFacets.push_back(newFacet);

      prevEdge = curr;
      lastPerp = vperp;
      //curr edge will be dfrom p1 to p2
      if (p1VerticesSet.find(c) != p1VerticesSet.end()) {
         curr = {c, curr.second};
         p1HorizonVertices.insert(c);
      } else {
         curr = {curr.first, c};
         p2HorizonVertices.insert(c);
      }
      
      point_t tmp = {curr.second[0] - curr.first[0], curr.second[1] - curr.first[1], curr.second[2] - curr.first[2]};
      direction = perpendNormal3d(vperp, tmp);
      //how to count direction depends on the direction of wrapping
      point_t prevEdgeVector = {prevEdge.second[0] - prevEdge.first[0], prevEdge.second[1] - prevEdge.first[1], prevEdge.second[2] - prevEdge.first[2]};
      if (p1VerticesSet.find(c) != p1VerticesSet.end()) {
         if (dot(prevEdgeVector, direction) < EPS_LOC)
            direction = perpendNormal3d(tmp, vperp);
      } else {
         if (dot(prevEdgeVector, direction) > -EPS_LOC)
            direction = perpendNormal3d(tmp, vperp);
      }

      if (notWorkingOnLowerTangentFacet == 0) {
         if (!isCoplanar(newFacet, lowerTangent.first) && !isCoplanar(newFacet, lowerTangent.second)) {
            usedPoints.erase(lowerTangent.first);
            usedPoints.erase(lowerTangent.second);
            notWorkingOnLowerTangentFacet = 1; //once I deleted lowerTangent vertices I dont need to test that again
         }
      }

      /*
      std::cout << "hull(){\n";
      for (point_t pt : newFacet) {
         std::cout << "  translate([" << pt[0]<< ", " << pt[1]<< ", " << pt[2]<< "])\n";
         std::cout << "     sphere(r=0.01);\n";
      }
      std::cout << "}\n";
      ;*/
   } while ((curr != lowerTangent));
   //double wrapTimeB = omp_get_wtime();
   //std::cout << "wrappping took " << wrapTimeB - wrapTimeA << "\n";
   if (debug) {
      std::cout << "Newly added faces: " << newFacets.size() << " \n";
      for (unsigned int i = 0; i < newFacets.size(); i++) {
         printFacet(newFacets[i]);
         std::cout << '\n';
      }
   }

   //double discardTimeA = omp_get_wtime();
   discardHiddenFaces(p1, p2HorizonVertices, p1HorizonVertices, 1);
   discardHiddenFaces(p2, p1HorizonVertices, p2HorizonVertices, 0);
   //double discardTimeB = omp_get_wtime();
   //std::cout << "discarding took " << discardTimeB - discardTimeA << "\n";
   
   for (facet_t f : newFacets)
      p1.addFacet(f);
   for (facet_t f : p2.m_faces)
      p1.addFacet(f);

   /*
   std::cout << "$fn = 10;\n";
   for (facet_t f : newFacets) {
      std::cout << "hull(){\n";
      for (point_t pt : f) {
         std::cout << "  translate([" << pt[0] << ", " << pt[1] << ", " << pt[2] << "])\n";
         std::cout << "     sphere(r=0.01);\n";
      }
      std::cout << "}\n";
   }
    */

   removeCoplanarFaces(p1, possiblyCoplanarFacets);
}

Polyhedron& recursiveMerge(std::vector<Polyhedron>& partialResults, int start, int end){
      if(end-start == 1){
      edgePoints_t lt = findLowerTangent(partialResults[start], partialResults[end]);
      mergeTwoHullsOptimized(partialResults[start], partialResults[end], lt);
   }else if(end - start == 2){
      edgePoints_t lt = findLowerTangent(partialResults[start], partialResults[end-1]);
      mergeTwoHullsOptimized(partialResults[start], partialResults[end-1], lt);
      lt = findLowerTangent(partialResults[start], partialResults[end]);
      mergeTwoHullsOptimized(partialResults[start], partialResults[end], lt);
   }else{
      int half = (end + start) / 2;
#pragma omp task shared(partialResults)
      recursiveMerge(partialResults, start, half);
#pragma omp task shared(partialResults)
      recursiveMerge(partialResults, half+1, end);
#pragma omp taskwait
      edgePoints_t lt = findLowerTangent(partialResults[start], partialResults[half+1]);
      mergeTwoHullsOptimized(partialResults[start], partialResults[half+1], lt);
   }
}
