/* 
 * File:   SolverJarvis.h
 * Author: motykvac
 *
 * Created on February 23, 2017, 6:10 PM
 */

#ifndef SOLVERJARVIS_H
#define	SOLVERJARVIS_H

#include "structures.h"

class SolverJarvis : Solver3D {
public:

   SolverJarvis(int thr = 1) {
      numThreads = thr;
      m_name = "Jarvis";
      EPS_LOC = 1e-6;
   };
   Polyhedron& Solve(data_t& input, Polyhedron& output);
   Polyhedron& SolveParallel(data_t& input, Polyhedron& output, int numThr);

private:
   int numThreads;
   Polyhedron& SolveNaive(data_t& input, Polyhedron& output);
   std::pair<unsigned, unsigned> findInitial(const data_t& input);
   double EPS_LOC;
};


#endif	/* SOLVERJARVIS_H */

